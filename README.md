# Nation Instruments Library #

The NI PCI cards for data acquisition and control (e.g. the NI PCI 6229) were supported for Simulink Real-Time (formerly known as XPC).

However, since MATLAB 2018a the support for Simulink Real-Time has largely diminished, putting the focus on their own hardware: Speedgoat. Because of this there is no native support for the NI cards in later versions of MATLAB.

However, the NI Simulink blocks from MATLAB 2016b are just S-functions on top of the NI C-library. So these blocks could be packed up and used also in later versions. That is exactly what this repository is.

## How to use ##

 1. Clone this repository
 2. Add the `nilib/` directory to your MATLAB path
 3. Create your own Simulink model (*outside* this library directory, it can be anywhere on your computer)
 4. With your model and the library `slrt_nilib_*.slx` model open, copy the necessary blocks from the library into your own model
     * The library model can be closed again afterwards

Test your model by trying to build it. The blocks you copied are masks. Because you added the library model to your path, MATLAB is able to find the necessary sources. 

## Versions ##

The library file is directly from MATLAB 2016b. For convenience, the file as re-saved with MATLAB 2018b, while running the upgrade advisor. Use this version for later versions.  
It could be worthwhile to create another upgrades version for later editions.
