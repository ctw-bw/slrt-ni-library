/* dinipcim.c - xPC Target, non-inlined S-function driver for Digital Input
 * section of NI PCI-M series boards  */
/* Copyright 1996-2009 The MathWorks, Inc.
*/

#define         S_FUNCTION_LEVEL        2
#undef          S_FUNCTION_NAME
#define         S_FUNCTION_NAME         dinipcim

#include        <stddef.h>
#include        <stdlib.h>

#include        "simstruc.h"

#ifdef          MATLAB_MEX_FILE
#include        "mex.h"
#endif

#ifndef         MATLAB_MEX_FILE
#include        <windows.h>
#include        "xpctarget.h"
#include        "xpcionim.h"
#endif

/* Input Arguments */
#define NUMBER_OF_ARGS          (5)
#define CHANNEL_ARG             ssGetSFcnParam(S,0)
#define SAMP_TIME_ARG           ssGetSFcnParam(S,1)
#define SLOT_ARG                ssGetSFcnParam(S,2)
#define DIR_CONTROL_ARG         ssGetSFcnParam(S,3)
#define DEV_ARG                 ssGetSFcnParam(S,4)

#define NO_I_WORKS              (1)
#define BASE_ADDR_I_IND         (0)

#define NO_R_WORKS              (0)

static char_T msg[256];

static void mdlInitializeSizes(SimStruct *S)
{
    size_t i;

    ssSetNumSFcnParams(S, NUMBER_OF_ARGS);
    if (ssGetNumSFcnParams(S) != ssGetSFcnParamsCount(S))
    {
        sprintf(msg,"Wrong number of input arguments passed.\n%d arguments are expected\n",NUMBER_OF_ARGS);
        ssSetErrorStatus(S,msg);
        return;
    }

    ssSetNumContStates(S, 0);
    ssSetNumDiscStates(S, 0);

    if( !ssSetNumOutputPorts(S, (int_T)mxGetN(CHANNEL_ARG))) return;
    for (i=0;i<mxGetN(CHANNEL_ARG);i++) {
        ssSetOutputPortWidth(S, i, 1);
    }

    if( !ssSetNumInputPorts(S, 0)) return;

    ssSetNumSampleTimes(S, 1);

    ssSetSimStateCompliance( S, HAS_NO_SIM_STATE );

    ssSetNumRWork(S, NO_R_WORKS);
    ssSetNumIWork(S, NO_I_WORKS);
    ssSetNumPWork(S, 0);

    ssSetNumModes(S, 0);
    ssSetNumNonsampledZCs(S, 0);

    for( i = 0 ; i < NUMBER_OF_ARGS ; i++ )
        ssSetSFcnParamTunable(S, i, 0);

    ssSetOptions(S, SS_OPTION_DISALLOW_CONSTANT_SAMPLE_TIME | SS_OPTION_EXCEPTION_FREE_CODE );
}

static void mdlInitializeSampleTimes(SimStruct *S)
{
    if (mxGetPr(SAMP_TIME_ARG)[0] == -1.0)
    {
    	ssSetSampleTime(S, 0, INHERITED_SAMPLE_TIME);
    	ssSetOffsetTime(S, 0, FIXED_IN_MINOR_STEP_OFFSET);
        ssSetModelReferenceSampleTimeInheritanceRule(S, USE_DEFAULT_FOR_DISCRETE_INHERITANCE);
    }
    else
    {
        ssSetSampleTime(S, 0, mxGetPr(SAMP_TIME_ARG)[0]);
    	ssSetOffsetTime(S, 0, 0.0);
    }
}

#define MDL_START
static void mdlStart(SimStruct *S)
{
#ifndef MATLAB_MEX_FILE

    int_T Control_Register;
    int_T bus, slot;
    xpcPCIDevice pciinfo;
    volatile uint32_T *lreg;
    MseriesBoard *bd;
    int  devtype;

    if (mxGetN(SLOT_ARG) == 1)
    {
        bus = 0;
        slot = (int_T)mxGetPr(SLOT_ARG)[0];
    }
    else
    {
        bus = (int_T)mxGetPr(SLOT_ARG)[0];
        slot = (int_T)mxGetPr(SLOT_ARG)[1];
    }

    devtype = (int_T)mxGetPr(DEV_ARG)[0];

    bd = niMIdentify( S, devtype, bus, slot, &pciinfo );
    if( bd == 0 )
        return;  // We didn't find the board as described.

    lreg  = (volatile uint32_T *)pciinfo.VirtAddress[1];
    Control_Register = (uint32_T)mxGetPr( DIR_CONTROL_ARG )[0];
    lreg[ L_DIO_Direction ] = Control_Register;

    ssSetIWorkValue(S, BASE_ADDR_I_IND, (uint_T)lreg);
#endif

}

static void mdlOutputs(SimStruct *S, int_T tid)
{
#ifndef MATLAB_MEX_FILE
    volatile uint32_T *lreg = (volatile uint32_T *)ssGetIWorkValue(S, BASE_ADDR_I_IND);
    size_t i;
    uint32_T input;
    int32_T channel;
    real_T  *y;

    input = lreg[ L_Static_Digital_Input ];

    for (i = 0 ; i < mxGetN(CHANNEL_ARG) ; i++ )
    {
        channel = (int32_T)mxGetPr(CHANNEL_ARG)[i];
        y = ssGetOutputPortSignal(S,i);
        y[0] = (input >>(channel-1)) & 1;
    }
#endif
}

static void mdlTerminate(SimStruct *S)
{
}

#ifdef MATLAB_MEX_FILE  /* Is this file being compiled as a MEX-file? */
#include "simulink.c"   /* Mex glue */
#else
#include "cg_sfun.h"    /* Code generation glue */
#endif
