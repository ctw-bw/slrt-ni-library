/* pwmoutnipcim.c - xPC Target, non-inlined S-function driver for
 *               Pulse Train generation using the General Purpose Counters
 *               of the National Instruments M series boards.
 */
/* Copyright 1996-2014 The MathWorks, Inc.
 */

#define         S_FUNCTION_LEVEL        2
#undef          S_FUNCTION_NAME
#define         S_FUNCTION_NAME         pwmoutnipcim

#include        <stddef.h>
#include        <stdlib.h>

#include        "simstruc.h"

#ifdef          MATLAB_MEX_FILE
#include        "mex.h"
#endif

#ifndef         MATLAB_MEX_FILE
#include        <windows.h>
#include        "xpctarget.h"
#include        "xpcionim.h"
#endif

/* Input Arguments */
#define NUMBER_OF_ARGS          (10)
#define CHANNEL_ARG             ssGetSFcnParam(S, 0)
#define SAMP_TIME_ARG           ssGetSFcnParam(S, 1)
#define SLOT_ARG                ssGetSFcnParam(S, 2)
#define INIT_HIGH_ARG           ssGetSFcnParam(S, 3)
#define INIT_LOW_ARG            ssGetSFcnParam(S, 4)
#define PFIDIR_ARG              ssGetSFcnParam(S, 5)
#define ARM_ARG                 ssGetSFcnParam(S, 6)
#define DISARM_ARG              ssGetSFcnParam(S, 7)
#define STOP_ARG                ssGetSFcnParam(S, 8)
#define DEV_ARG                 ssGetSFcnParam(S, 9)

#define SAMP_TIME_IND           (0)

#define NO_R_WORKS              (0)
#define NO_I_WORKS              (5)
#define NO_P_WORKS              (1)

/* Index into PWork */
#define BASE_ADDR_P_IND         (0)

/*Index into IWork */
#define LOW_I_IND               (0)
#define HIGH_I_IND              (1)
#define BANK_I_IND              (2)
#define BNUM_I_IND              (3)
#define ARM_I_IND               (4)

static char_T msg[256];

static void mdlInitializeSizes(SimStruct *S)
{
    int j;
    int arm = (int)mxGetPr(ARM_ARG)[0];

    ssSetNumSFcnParams(S, NUMBER_OF_ARGS);
    if (ssGetNumSFcnParams(S) != ssGetSFcnParamsCount(S))
    {
        sprintf(msg,"Wrong number of input arguments passed.\n"
                "%d arguments are expected\n", NUMBER_OF_ARGS);
        ssSetErrorStatus(S,msg);
        return;
    }

    ssSetNumContStates(S, 0);
    ssSetNumDiscStates(S, 0);

    //printf("arm = %d\n", arm );
    if( arm == 0 )
    {
        if( !ssSetNumInputPorts(S, 2)) return;
    }
    else
    {
        if( !ssSetNumInputPorts(S, 3)) return;
        ssSetInputPortWidth(             S, 2, 1);  // ARM input
        ssSetInputPortDirectFeedThrough( S, 2, 1);
        ssSetInputPortRequiredContiguous(S, 2, 1);
    }
    ssSetInputPortWidth(             S, 0, 1);  // High count
    ssSetInputPortDirectFeedThrough( S, 0, 1);
    ssSetInputPortRequiredContiguous(S, 0, 1);
    ssSetInputPortWidth(             S, 1, 1);  // Low count
    ssSetInputPortDirectFeedThrough( S, 1, 1);
    ssSetInputPortRequiredContiguous(S, 1, 1);

    if( !ssSetNumOutputPorts(S, 0)) return;

    ssSetNumSampleTimes(S, 1);

    ssSetSimStateCompliance( S, HAS_NO_SIM_STATE );

    ssSetNumRWork(S, NO_R_WORKS);
    ssSetNumIWork(S, NO_I_WORKS);
    ssSetNumPWork(S, NO_P_WORKS);

    ssSetNumModes(S, 0);
    ssSetNumNonsampledZCs(S, 0);

    for (j = 0; j < NUMBER_OF_ARGS; j++)
        ssSetSFcnParamNotTunable(S, j);

    ssSetOptions(S, SS_OPTION_DISALLOW_CONSTANT_SAMPLE_TIME | SS_OPTION_EXCEPTION_FREE_CODE );
    return;
}

static void mdlInitializeSampleTimes(SimStruct *S)
{
    if (mxGetPr(SAMP_TIME_ARG)[0] == -1.0)
    {
    	ssSetSampleTime(S, 0, INHERITED_SAMPLE_TIME);
    	ssSetOffsetTime(S, 0, FIXED_IN_MINOR_STEP_OFFSET);
        ssSetModelReferenceSampleTimeInheritanceRule(S, USE_DEFAULT_FOR_DISCRETE_INHERITANCE);
    }
    else
    {
        ssSetSampleTime(S, 0, mxGetPr(SAMP_TIME_ARG)[0]);
    	ssSetOffsetTime(S, 0, 0.0);
    }
}

#define MDL_START
static void mdlStart(SimStruct *S)
{
#ifndef MATLAB_MEX_FILE
    int_T bus, slot;
    xpcPCIDevice pciinfo;
    volatile uint32_T *lreg;
    volatile uint16_T *sreg;
    uint32_T *iwork = (uint32_T *)ssGetIWork(S);
    const char *devName;
    MseriesBoard *bd;
    int32_T bnum;
    int  devtype;
    int32_T chan = (int32_T)mxGetPr( CHANNEL_ARG )[0];  // 0 or 1
    uint32_T high, low;
    double dhigh, dlow;
    uint32_T pfidir = (uint32_T)mxGetPr(PFIDIR_ARG)[0];
    uint32_T running = 0;  // -1 for fixed low, +1 fixed high, 0 running
    int32_T pfi;
    
    if (mxGetN(SLOT_ARG) == 1)
    {
        bus = 0;
        slot = (int_T)mxGetPr(SLOT_ARG)[0];
    }
    else
    {
        bus = (int_T)mxGetPr(SLOT_ARG)[0];
        slot = (int_T)mxGetPr(SLOT_ARG)[1];
    }

    devtype = (int_T)mxGetPr(DEV_ARG)[0];

    bd = niMIdentify( S, devtype, bus, slot, &pciinfo );
    if( bd == 0 )
        return;  // We didn't find the board as described.
    devName = bd->name;

    lreg  = (volatile uint32_T *)pciinfo.VirtAddress[1];
    sreg  = (volatile uint16_T *)pciinfo.VirtAddress[1];

    bnum = niM_Private_Data( (void *)sreg );
    if( bnum < 0 )
    {
        sprintf(msg, "%s: Too many M series boards", devName );
        ssSetErrorStatus(S, msg);
        return;
    }
    ssSetPWorkValue(S, BASE_ADDR_P_IND, (void *)lreg);
    iwork[BNUM_I_IND] = bnum;

    niM_configureTimebase( (void *)sreg );
    niM_Counter_Reset_All( (void *)sreg, chan );
    //niM_pllReset( (void *)sreg );
    //niM_analogTriggerReset( (void *)sreg );

    dhigh = mxGetPr(INIT_HIGH_ARG)[0] - 1;
    if( dhigh < 0 ) dhigh = 0;
    dlow  = mxGetPr(INIT_LOW_ARG)[0] - 1;
    if( dlow < 0 ) dlow = 0;
    
    high = (uint32_T)dhigh;
    low = (uint32_T)dlow;

    // If high < 1 or low < 1 (not both)
    // Output should saturate with the counter disarmed
    running = 0;  // Assume it should be running unless changed
    if( (high < 1) || (low < 1) )
    {
        if( ( high < 1 ) && (low > 1) )
            running = -1;  // Set to LOW, stopped
        if( ( high > 1 ) && (low < 1) )
            running = 1;   // Set to HIGH, stopped
        // If both < 1, we have a problem!  Set to LOW, stopped
        if( ( high < 1 ) && (low < 1) )
            running = -1;  // Set to LOW, stopped
    }
    if( high < 1 ) high = 1;
    if( low < 1 ) low = 1;

    iwork[LOW_I_IND] = low;
    iwork[HIGH_I_IND] = high;
    iwork[BANK_I_IND] = 1;  // initial bank y

//printf("Start: l = %d, h = %d\n", low, high );
    niM_Cont_Pulse_Train_Generation( (void *)sreg, chan, low, high );

    // Set a bit for OUTPUT on that PFI.
    // The mask init file looked at all blocks that use PFI bits.
    sreg[ S_IO_Bidirection_Pin ] = pfidir;

    // Need to arm or disarm based on the value of the arm input
    // to the block.  Do we need to disarm to a chosen level?
    
    // We need to set the PFI mux to the correct output on the board.
    // Choices for this driver are either PFI_SELECT_G0OUT and PFI_SELECT_G1OUT.
    // We'll use the simple fact that PFI_SELECT_G1OUT = PFI_SELECT_G0OUT + 1
    // On the 6221/37,      channel 0 out => PFI6,  channel 1 out => PFI7
    // On all other boards, channel 0 out => PFI12, channel 1 out => PFI13
    // Since chan = 0,1, we can just use the channel 0 PFI number and add chan.

    if( devtype == 27 ) // If 6221/37 board
    {
        pfi = 6+chan;
        // Reset the default counter out PFI to be a DO
        niM_SetPFI_Select( bnum, 12+chan, PFI_SELECT_DO );
    }
    else
        pfi = 12+chan;

//printf("devtype = %d, pfi = %d, chan = %d\n", devtype, pfi, chan );
    
    if( running == 0 )
    {
        // Repeat the select call twice.  There's some hardware issue that
        // this doesn't always work if only done once.
        niM_SetPFI_Select( bnum, pfi, PFI_SELECT_G0OUT+chan );
        niM_SetPFI_Select( bnum, pfi, PFI_SELECT_G0OUT+chan );
        niM_CtrArm( (void *)sreg, chan );
        iwork[ARM_I_IND] = 1;
    }
    else
    {
        uint16_T doval;
        niM_SetPFI_Select( bnum, pfi, PFI_SELECT_DO );
        if( running == 1 ) // Set high
            doval = niM_SetPFI_DO( bnum, pfi, 1 );
        else  // Set low
            doval = niM_SetPFI_DO( bnum, pfi, 0 );
        sreg[ S_PFI_DO ] = doval;
        // Leave the counter running, but invisibly
        niM_CtrArm( (void *)sreg, chan );
        iwork[ARM_I_IND] = 0;
    }
#endif
}

static void mdlOutputs(SimStruct *S, int_T tid)
{
#ifndef MATLAB_MEX_FILE
    uint32_T chan = (uint32_T)mxGetPr( CHANNEL_ARG )[0];
    uint32_T low, high, oldlow, oldhigh;
    real_T dlow, dhigh;
    uint16_T bank;
    uint32_T *iwork = (uint32_T *)ssGetIWork(S);
    uint16_T stat;
    volatile uint32_T *lreg = ssGetPWorkValue(S, BASE_ADDR_P_IND );
    volatile uint16_T *sreg = ssGetPWorkValue(S, BASE_ADDR_P_IND );
    uint32_T arm = (uint32_T)mxGetPr(ARM_ARG)[0];
    real_T   armval = 1.0;
    uint32_T bnum = ssGetIWork(S)[BNUM_I_IND];
    uint16_T doval;
    uint32_T maxcount = 0xffffffff;
    real_T dmaxcount = (real_T)maxcount;
    int32_T  pfi;
    int32_T  devtype;

    devtype = (int32_T)mxGetPr(DEV_ARG)[0];
    if( devtype == 27 ) // If 6221/37 board
        pfi = 6+chan;
    else
        pfi = 12+chan;

    // Need to arm or disarm based on the value of the arm input
    // to the block.  We need to disarm to a chosen level.
    if( arm == 1 )
    {
        armval = ssGetInputPortRealSignal(S, 2)[0];
        if( iwork[ARM_I_IND] == 0 ) // if disarmed
        {
            if( armval > 0.5 )
            {
                niM_SetPFI_Select( bnum, pfi, PFI_SELECT_G0OUT+chan );
                iwork[ARM_I_IND] = 1;
            }
            else
            {
                return;
            }
        }
        else // currently armed, check if we should disarm
        {
            if( armval <= 0.5 )
            {
                if( (int)mxGetPr(DISARM_ARG)[0] == 1 )
                    doval = niM_SetPFI_DO( bnum, pfi, 0 );
                else // DISARM == 2, HIGH
                    doval = niM_SetPFI_DO( bnum, pfi, 1 );
                sreg[ S_PFI_DO ] = doval;
                niM_SetPFI_Select( bnum, pfi, PFI_SELECT_DO );
                iwork[ARM_I_IND] = 0;
                return;
            }
        }
    }
    
    // Read the signal as a double, check for negative
    // and limit it before casting to int.
    // That allows me to use a uint32 and get the full
    // range of pulse widths.
    
    dhigh = ssGetInputPortRealSignal(S, 0)[0] - 1;
    dlow = ssGetInputPortRealSignal(S, 1)[0] - 1;
    if( dhigh < 0 ) dhigh = 0;
    if( dlow < 0 ) dlow = 0;
    if( dhigh > dmaxcount ) dhigh = dmaxcount;
    if( dlow > dmaxcount ) dlow = dmaxcount;
    high = (uint32_T)dhigh;
    low  = (uint32_T)dlow;
    oldlow  = iwork[ LOW_I_IND];
    oldhigh = iwork[HIGH_I_IND];
    bank    = (ushort_T)iwork[BANK_I_IND];
    
    // if high < 1, set DO=0, switch
    // else if low < 1, set DO=1, switch
    // else switch back to counter
    // If both are < 1, then we get a low level output
    
    if( high < 1 )
    {
        doval = niM_SetPFI_DO( bnum, pfi, 0 );
        sreg[ S_PFI_DO ] = doval;
        niM_SetPFI_Select( bnum, pfi, PFI_SELECT_DO );
    }
    else if( low < 1 )
    {
        doval = niM_SetPFI_DO( bnum, pfi, 1 );
        sreg[ S_PFI_DO ] = doval;
        niM_SetPFI_Select( bnum, pfi, PFI_SELECT_DO );
    }
    else
    {
        niM_SetPFI_Select( bnum, pfi, PFI_SELECT_G0OUT+chan );
    }
    
    if( high < 1 ) high = 1;
    if( low < 1 ) low = 1;

    /* do nothing if nothing needs to be done */
    if (oldlow == low && oldhigh == high)
        return;
    
    //iwork[BANK_I_IND] = 1 - bank;
    //iwork[ LOW_I_IND] = low;
    //iwork[HIGH_I_IND] = high;

    /* Banks need to have switched since last update; can't do it if not */
    // chan is 0 or 1, bank is 0 or 1
    // chan 0 bank bit is 0x0001, chan 1 bank bit is 0x0002
    stat = sreg[S_G01_Joint_Status_1];
    if( (stat & (1 << chan)) == (bank << chan) )
        return;

    iwork[BANK_I_IND] = 1 - bank;
    iwork[ LOW_I_IND] = low;
    iwork[HIGH_I_IND] = high;

    /* Load low, high values into appropriate registers */
    /* Gi_Load_A <= high - 1 */
    lreg[L_G0_Load_A+2*chan] = high;

    /* Gi_Load_B <= low - 1 */
    lreg[L_G0_Load_B+2*chan] = low;

    /* Gi_Bank_Switch <= 1 */
    sreg[S_G0_Command + chan] = 0x1d05;

    return;
#endif
}

static void mdlTerminate(SimStruct *S)
{
#ifndef MATLAB_MEX_FILE
    int32_T chan = (int32_T)mxGetPr( CHANNEL_ARG )[0];
    void *base = (void *)ssGetPWork(S)[BASE_ADDR_P_IND];
    volatile uint16_T *sreg = (volatile uint16_T *)base;
    uint16_T doval;
    uint32_T bnum = ssGetIWork(S)[BNUM_I_IND];
    int32_T  pfi;
    int32_T  devtype;

    devtype = (int32_T)mxGetPr(DEV_ARG)[0];
    if( devtype == 27 ) // If 6221/37 board
        pfi = 6+chan;
    else
        pfi = 12+chan;
    
    // Stop to the stop state, either high or low.
    // Switch the PFI to DO after setting the state.
    if( (int)mxGetPr(STOP_ARG)[0] == 1 )
        doval = niM_SetPFI_DO( bnum, pfi, 0 );
    else
        doval = niM_SetPFI_DO( bnum, pfi, 1 );
    sreg[ S_PFI_DO ] = doval;
    niM_SetPFI_Select( bnum, pfi, PFI_SELECT_DO );

    niM_CtrDisarm( base, chan );

#endif
    return;
}

#ifdef MATLAB_MEX_FILE  /* Is this file being compiled as a MEX-file? */
#include "simulink.c"   /* Mex glue */
#else
#include "cg_sfun.h"    /* Code generation glue */
#endif
