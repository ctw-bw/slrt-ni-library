function mnipci660x(phase, ctr, slot, boardType) %#ok

% Copyright 2003-2011 The MathWorks, Inc.
  
  if phase == 1  % InitFcn call, only phase is defined
    ctr = get_param(gcb, 'channel');
    masktype = get_param( gcb, 'MaskType' );
    ctrtype = ['ctr', masktype(4:end)];
    pwmtype = ['pwm', masktype(4:end)];
    enctype = ['enc', masktype(4:end)];
    evttype = ['evt', masktype(4:end)];
    ctrs = find_system(bdroot, ...
                       'FollowLinks', 'on', ...
                       'LookUnderMasks', 'all', ...
                       'MaskType', ctrtype );
    pwms = find_system(bdroot, ...
                       'FollowLinks', 'on', ...
                       'LookUnderMasks', 'all', ...
                       'MaskType', pwmtype );
    encs = find_system(bdroot, ...
                       'FollowLinks', 'on', ...
                       'LookUnderMasks', 'all', ...
                       'MaskType', enctype );
    evts = find_system(bdroot, ...
                       'FollowLinks', 'on', ...
                       'LookUnderMasks', 'all', ...
                       'MaskType', evttype );

    slot = evalin( 'base', get_param(gcb, 'slot') );

    matching = {};                          % will contain all blocks for the
                                            % same board.
    for i = 1 : length(ctrs)
      tmp = evalin( 'base', get_param(ctrs{i}, 'slot'));
      if isequal(tmp, slot)
        tmp = get_param(ctrs{i}, 'MaskValues');
        matching{end + 1} = tmp{1}; %#ok
      end
    end
    for i = 1 : length(pwms)
      tmp = evalin( 'base', get_param(pwms{i}, 'slot'));
      if isequal(tmp, slot)
        tmp = get_param(pwms{i}, 'MaskValues');
        matching{end + 1} = tmp{1}; %#ok
      end
    end
    for i = 1 : length(encs)
      tmp = evalin( 'base', get_param(encs{i}, 'slot'));
      if isequal(tmp, slot)
        tmp = get_param(encs{i}, 'MaskValues');
        matching{end + 1} = tmp{1}; %#ok
      end
    end
    for i = 1 : length(evts)
        tmp = evalin( 'base', get_param(evts{i}, 'slot'));
        if isequal(tmp, slot)
            tmp = get_param(evts{i}, 'channel');
            matching{end + 1} = tmp; %#ok
        end
    end

    if sum(strcmp(ctr, matching)) ~= 1
      error(message('xPCTarget:PCI660xPWM:DupBlock', ctr));
    end
  end
  
  if phase == 2
    if prod(length(slot)) > 2
      error(message('xPCTarget:PCI660xPWM:Slot'));
    end
    % sprintf('phase2: ctr = %d\n', ctr );
  end
