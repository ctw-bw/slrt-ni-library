function [maskdisplay, maskdescription] = ...
    mdanipci670x(phase, boardType, channel, pciSlot) %#ok

%   Copyright 2001-2011 The MathWorks, Inc.

  if phase == 1
    mType = get_param( gcb, 'MaskType' );
    blocks = find_system(bdroot, ...
                         'FollowLinks','on', ...
                         'LookUnderMasks','all', ...
                         'MaskType', mType);
    if length(blocks)>1
      % loop over all blocks and collect information for cross-block checking
      for block=1:length(blocks)
        % PCI-slot (physical board reference
        tmp=evalin('base', get_param(blocks{block},'pcislot'));
        if length(tmp)==2
          info.pci(block)=tmp(1)*256+tmp(2);
        else
          info.pci(block)=tmp(1);
        end
      end
      % do cross-block checking by using the collected information
      % check for multiple instances using the same physical board
      if length(info.pci)~=length(unique(info.pci))
        error(message('xPCTarget:PCI670x:DupBlock'));
      end
    end
  end
  
  if phase == 2
    switch boardType
     case 1
      maskdisplay = 'disp(''PCI-6703\nNational Instr.\n';
      description = 'PCI-6703';
      maxChannel  = 16;
     case 2
      maskdisplay = 'disp(''PCI-6704\nNational Instr.\n';
      description = 'PCI-6704';
      maxChannel  = 32;
     case 3
      maskdisplay = 'disp(''PXI-6704\nNational Instr.\n';
      description = 'PXI-6704';
      maxChannel  = 32;
    end
    maskdisplay=[maskdisplay,'Analog Output'');'];
    for i=1:length(channel)
      maskdisplay = [maskdisplay,'port_label(''input'',',num2str(i),',''',num2str(channel(i)),''');']; %#ok
    end
    maskdescription=[description,10,'National Instr.',10,'Analog Output'];

    if size(channel,1)~=1
      error(message('xPCTarget:PCI670x:ChannelCol'));
    end

    chUsed=zeros(1,32);
    for i=1:length(channel)
      chan=round(channel(i));
      if chan < 1 || chan > maxChannel
        error(message('xPCTarget:PCI670x:ChannelValue', maxChannel));
      end
      if chUsed(chan)
        error(message('xPCTarget:PCI670x:DupChannel', chan));
      else
        chUsed(chan)=1;
      end
    end
  end
  
