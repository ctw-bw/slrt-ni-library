/* danipcim.c - xPC Target, non-inlined S-function driver for D/A 
 * section of NI PCI-M series boards
 */
/* Copyright 1996-2009 The MathWorks, Inc.
 */

#define         S_FUNCTION_LEVEL        2
#undef          S_FUNCTION_NAME
#define         S_FUNCTION_NAME         danipcim

#include        <stddef.h>
#include        <stdlib.h>

#include        "simstruc.h"

#ifdef          MATLAB_MEX_FILE
#include        "mex.h"
#endif

#ifndef         MATLAB_MEX_FILE
#include        <windows.h>
#include        "xpctarget.h"
#include        "xpcionim.h"
#endif

/* Input Arguments */
#define NUMBER_OF_ARGS          (7)
#define CHANNEL_ARG             ssGetSFcnParam(S,0)
#define RANGE_ARG               ssGetSFcnParam(S,1)
#define RESET_ARG               ssGetSFcnParam(S,2)
#define INIT_VAL_ARG            ssGetSFcnParam(S,3)
#define SAMP_TIME_ARG           ssGetSFcnParam(S,4)
#define SLOT_ARG                ssGetSFcnParam(S,5)
#define DEV_ARG                 ssGetSFcnParam(S,6)

#define SAMP_TIME_IND           (0)
#define BASE_ADDR_IND           (0)

#define NO_I_WORKS              (41)
#define BASE_ADDR_I_IND         (0)
#define SCALE_I_IND             (1)  /* array of tScaleCoefficients*8 */

#define NO_R_WORKS              (6)
#define GAIN_R_IND              (0)
#define MINVAL_R_IND            (2)
#define MAXVAL_R_IND            (4)

static char_T msg[256];

// List xpcionim.c as additional file to
// include with the block.

static void mdlInitializeSizes(SimStruct *S)
{
    size_t i;

    ssSetNumSFcnParams(S, NUMBER_OF_ARGS);
    if (ssGetNumSFcnParams(S) != ssGetSFcnParamsCount(S)) {
        sprintf(msg,"Wrong number of input arguments passed.\n%d arguments are expected\n",NUMBER_OF_ARGS);
        ssSetErrorStatus(S,msg);
        return;
    }

    ssSetNumContStates(S, 0);
    ssSetNumDiscStates(S, 0);

    if( !ssSetNumInputPorts(S, (int_T)mxGetN(CHANNEL_ARG))) return;
    for (i = 0; i < mxGetN(CHANNEL_ARG); i++)
    {
        ssSetInputPortWidth(S, i, 1);
        ssSetInputPortDirectFeedThrough(S, i, 1);
    }

    if( !ssSetNumOutputPorts(S, 0)) return;

    ssSetNumSampleTimes(S, 1);

    ssSetSimStateCompliance( S, HAS_NO_SIM_STATE );

    ssSetNumRWork(S, NO_R_WORKS);
    ssSetNumIWork(S, NO_I_WORKS);
    ssSetNumPWork(S, 0);

    ssSetNumModes(S, 0);
    ssSetNumNonsampledZCs(S, 0);

    for (i = 0; i < NUMBER_OF_ARGS; i++)
        ssSetSFcnParamTunable(S, i, 0);

    ssSetOptions(S, SS_OPTION_DISALLOW_CONSTANT_SAMPLE_TIME | SS_OPTION_EXCEPTION_FREE_CODE );
}

static void mdlInitializeSampleTimes(SimStruct *S)
{
    ssSetSampleTime(S, 0, mxGetPr(SAMP_TIME_ARG)[SAMP_TIME_IND]);
    ssSetOffsetTime(S, 0, 0.0);
}

#define MDL_START
static void mdlStart(SimStruct *S)
{

#ifndef MATLAB_MEX_FILE
    int_T nChannels;
    int_T i;

    xpcPCIDevice pciinfo;
    volatile uint32_T *lreg;
    volatile uint8_T  *breg;
    volatile uint8_T  *breg0;  // bar0
    MseriesBoard *bd;
    int32_T  devtype;
    int_T bus, slot;
    uint8_T eeprom[1024];
    tScalingCoefficients *iscale;
    uint32_T maxchan;

    nChannels = mxGetN(CHANNEL_ARG);

    if (mxGetN(SLOT_ARG) == 1)
    {
        bus = 0;
        slot = (int_T)mxGetPr(SLOT_ARG)[0];
    }
    else
    {
        bus = (int_T)mxGetPr(SLOT_ARG)[0];
        slot = (int_T)mxGetPr(SLOT_ARG)[1];
    }

    devtype = (int_T)mxGetPr(DEV_ARG)[0];

    bd = niMIdentify( S, devtype, bus, slot, &pciinfo );
    if( bd == 0 )
        return;  // We didn't find the board as described.

    maxchan = (uint32_T)bd->maxDAChan;

    lreg  = (volatile uint32_T *)pciinfo.VirtAddress[1];
    breg  = (volatile uint8_T *)lreg;
    breg0 = (volatile uint8_T *)pciinfo.VirtAddress[0];

    ssSetIWorkValue(S, BASE_ADDR_I_IND, (uint_T)lreg);

    niM_eepromReadMSeries( (void *)breg0, (void *)breg, eeprom, 1024 );

    iscale = (tScalingCoefficients *)(((uint8_T *)ssGetIWork( S )) + sizeof(uint32_T)*SCALE_I_IND);

#if 0
    // DEBUGGING: print out the eeprom contents
    for( i = 0 ; i < 1024 ; i++ )
    {
        if( (i % 16) == 0 )
            printf("%04d:", i );
        printf("%02x%c", eeprom[i], (i%16 == 15)?'\n':' ');
    }
#endif

    for( i = 0 ; i < nChannels ; i++ )
    {
        uint32_T range = (uint32_T)mxGetPr(RANGE_ARG)[i];
        uint32_T chan = (uint32_T)mxGetPr(CHANNEL_ARG)[i] - 1;

        if( range == 0 || range == 1 )
        {
            // Get scaling coefficients from EEProm data, only get the data
            // for the range this channel is using.
            niM_aoGetScalingCoefficients( eeprom, range, 0, chan, &iscale[chan] );
        }
        else
        {
            // For external reference, the converter takes 32767 to Vref
            // Hard clip to +-Vref, no extra!
            iscale[chan].order = 1;
            iscale[chan].c[0] = 0.0;
            iscale[chan].c[1] = 32767.0;
            iscale[chan].c[2] = 0.0;
            iscale[chan].c[3] = 0.0;
        }

        //printf("%d, %d %d, %f %f %f %f\n", range, chan,
        //       iscale[chan].order, iscale[chan].c[0],
        //       iscale[chan].c[1],iscale[chan].c[2],iscale[chan].c[3] );
    }

    niM_AO_Setup( (void *)breg );

    for( i = 0 ; i < nChannels ; i++ )
    {
        // Mask uses 1 based channel numbers, hardware is 0 based.
        uint32_T channel = (uint32_T)mxGetPr(CHANNEL_ARG)[i] - 1;
        uint32_T range = (uint32_T)mxGetPr(RANGE_ARG)[i];
        int32_T outval;
        float output;

        if( channel >= maxchan )
        {
            sprintf( msg, "Channel %d exceeds the max DA channel for the %s", channel+1, bd->name );
            ssSetErrorStatus( S, msg );
            return;
        }
        if( range > bd->maxDARange )
        {
            // The mask init file should have caught this.
            sprintf( msg, "Output range %d is not supported on the %s", range, bd->name );
            ssSetErrorStatus( S, msg );
            return;
        }

        output = (float)mxGetPr(INIT_VAL_ARG)[i];

        outval = niM_aoLinearScaler( output, iscale + channel );
        // All M series have 16 bit DA, limit the value.
        if( outval > 32767 )
            outval = 32767;
        if( outval < -32768 )
            outval = -32768;

        niM_aoConfigureDAC( (void *)breg, channel, 0xF, range );

        lreg[ L_DAC_Direct_Data0 + channel ] = outval;
    }

#endif

}

static void mdlOutputs(SimStruct *S, int_T tid)
{

#ifndef MATLAB_MEX_FILE

    volatile uint32_T *lreg = (volatile uint32_T *)ssGetIWorkValue(S, BASE_ADDR_I_IND);
    size_t   nChannels = mxGetN(CHANNEL_ARG);
    size_t   i;
    float     output;
    int32_T   channel;
    int32_T   outval;
    InputRealPtrsType uPtrs;
    tScalingCoefficients *iscale;

    iscale = (tScalingCoefficients *)(((uint8_T *)ssGetIWork( S )) + sizeof(uint32_T)*SCALE_I_IND);

    for( i = 0 ; i < nChannels ; i++ )
    {
        // convert channel number to 0 based
        channel = (int32_T)mxGetPr(CHANNEL_ARG)[i] - 1;
        uPtrs = ssGetInputPortRealSignalPtrs(S,i);
        output = (float)*uPtrs[0];

        outval = niM_aoLinearScaler( output, iscale + channel );
//printf("%d %f %d\n", channel, (double)output, outval );
        // All M series have 16 bit DA, limit the value.
        if( outval > 32767 )
            outval = 32767;
        if( outval < -32768 )
            outval = -32768;

        lreg[ L_DAC_Direct_Data0 + channel ] = outval;
    }
#endif

}


static void mdlTerminate(SimStruct *S)
{

#ifndef MATLAB_MEX_FILE

    uint32_T  *lreg = (uint32_T *)ssGetIWorkValue(S, BASE_ADDR_I_IND);
    size_t i, nChannels = mxGetN( CHANNEL_ARG );
    int32_T  channel;
    int32_T outval;
    float  output;
    tScalingCoefficients *iscale;

    iscale = (tScalingCoefficients *)(((uint8_T *)ssGetIWork( S )) + sizeof(uint32_T)*SCALE_I_IND);

    // At load time, set channels to their initial values.
    // At model termination, reset resettable channels to their initial values.
    for (i = 0 ; i < nChannels ; i++ )
    {
        if (xpcIsModelInit() || (uint_T)mxGetPr(RESET_ARG)[i])
        {
            // convert channel number to 0 based
            channel = (int32_T)mxGetPr(CHANNEL_ARG)[i] - 1;

            output = (float)mxGetPr(INIT_VAL_ARG)[i];

            outval = niM_aoLinearScaler( output, iscale + channel );
            // All M series have 16 bit DA, limit the value.
            if( outval > 32767 )
                outval = 32767;
            if( outval < -32768 )
                outval = -32768;

            lreg[ L_DAC_Direct_Data0 + channel ] = outval;
        }
    }

#endif

}

#ifdef MATLAB_MEX_FILE  /* Is this file being compiled as a MEX-file? */
#include "simulink.c"   /* Mex glue */
#else
#include "cg_sfun.h"    /* Code generation glue */
#endif
