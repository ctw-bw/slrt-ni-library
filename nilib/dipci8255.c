/* dipci8255.c - xPC Target, non-inlined S-function driver for digital input section for PCI boards using the 8255 chip */
/* Copyright 1996-2009 The MathWorks, Inc.
*/

// Note: when adding a new DEV_ARG, be sure to add a case to all three DEV_ARG switches

#define         S_FUNCTION_LEVEL        2
#undef          S_FUNCTION_NAME
#define         S_FUNCTION_NAME         dipci8255

#include        <stddef.h>
#include        <stdlib.h>

#include        "simstruc.h"

#ifdef          MATLAB_MEX_FILE
#include        "mex.h"
#endif

#ifndef         MATLAB_MEX_FILE
#include        <windows.h>
#include        "deviceinfo.h"
#include        "xpcimports.h"
#endif

/* Input Arguments */
#define NUMBER_OF_ARGS          (8)
#define CHANNEL_ARG             ssGetSFcnParam(S,0)
#define PORT_ARG                ssGetSFcnParam(S,1)
#define INI_ARG                 ssGetSFcnParam(S,2)
#define CHIP_ARG                ssGetSFcnParam(S,3)
#define SAMP_TIME_ARG           ssGetSFcnParam(S,4)
#define SLOT_ARG                ssGetSFcnParam(S,5)
#define CONTROL_ARG             ssGetSFcnParam(S,6)
#define DEV_ARG                 ssGetSFcnParam(S,7)

#define SAMP_TIME_IND           (0)
#define BASE_ADDR_IND           (0)

#define NO_I_WORKS              (2)
#define BASE_I_IND              (0)
#define OFFSET_IND              (1)

#define NO_R_WORKS              (0)

static char_T msg[256];

static void mdlInitializeSizes(SimStruct *S)
{
    size_t i;

    ssSetNumSFcnParams(S, NUMBER_OF_ARGS);
    if (ssGetNumSFcnParams(S) != ssGetSFcnParamsCount(S)) {
        sprintf(msg,"Wrong number of input arguments passed.\n%d arguments are expected\n",NUMBER_OF_ARGS);
        ssSetErrorStatus(S,msg);
        return;
    }

    ssSetNumContStates(S, 0);
    ssSetNumDiscStates(S, 0);

    if( !ssSetNumOutputPorts(S, (int_T)mxGetN(CHANNEL_ARG)) )return;
    for (i = 0; i < mxGetN(CHANNEL_ARG); i++) {
        ssSetOutputPortWidth(S, i, 1);
    }

    if( !ssSetNumInputPorts(S, 0) )return;

    ssSetNumSampleTimes(S, 1);

    ssSetSimStateCompliance( S, HAS_NO_SIM_STATE );

    ssSetNumRWork(S, NO_R_WORKS);
    ssSetNumIWork(S, NO_I_WORKS);
    ssSetNumPWork(S, 0);

    ssSetNumModes(S, 0);
    ssSetNumNonsampledZCs(S, 0);

    for( i = 0 ; i < NUMBER_OF_ARGS ; i++ )
        ssSetSFcnParamTunable(S,i,0);

    ssSetOptions(S, SS_OPTION_DISALLOW_CONSTANT_SAMPLE_TIME | SS_OPTION_EXCEPTION_FREE_CODE );
}

static void mdlInitializeSampleTimes(SimStruct *S)
{
    ssSetSampleTime(S, 0, mxGetPr(SAMP_TIME_ARG)[SAMP_TIME_IND]);
    ssSetOffsetTime(S, 0, 0.0);
}

#define MDL_START
static void mdlStart(SimStruct *S)
{

#ifndef MATLAB_MEX_FILE

    PciDevice    pDev;
    int32_T      pciBus, pciSlot;
    int32_T      num_channels, base, control;
    int_T        ival, prt;
    uint16_T     vendorId, deviceId;
    int32_T      sectionAddr, offset8255;
    const uint8_T *devName;
    void *Physical1, *Physical0;
    void *Virtual1, *Virtual0;
    volatile uint32_T *ioaddress0;
    volatile uint16_T *ioaddress1;
    volatile uint8_T *ioaddress;

    switch ((int_T)mxGetPr(DEV_ARG)[0]) {
      case 1:
        devName = "MC PCI-DAS1602/16";
        vendorId=0x1307;
        deviceId=0x1;
        sectionAddr=0x3;
        offset8255=0x4;
        break;
      case 2:
        devName = "MC PCI-DAS1200";
        vendorId=0x1307;
        deviceId=0xf;
        sectionAddr=0x3;
        offset8255=0x4;
        break;
      case 3:
        devName = "MC PCI-DAS1200/JR";
        vendorId=0x1307;
        deviceId=0x19;
        sectionAddr=0x3;
        offset8255=0x4;
        break;
      case 4:
        devName = "MC PCI-DIO48H";
        vendorId=0x1307;
        deviceId=0xb;
        sectionAddr=0x2;
        switch ((int_T)mxGetPr(CHIP_ARG)[0]) {
          case 1:
            offset8255=0x0;
            break;
          case 2:
            offset8255=0x4;
            break;
        }
        break;
      case 5:
        devName = "MC PCI-DDA02/12";
        vendorId=0x1307;
        deviceId=0x20;
        sectionAddr=0x2;
        switch ((int_T)mxGetPr(CHIP_ARG)[0]) {
          case 1:
            offset8255=0x0;
            break;
          case 2:
            offset8255=0x4;
            break;
        }
        break;
      case 6:
        devName = "MC PCI-DDA04/12";
        vendorId=0x1307;
        deviceId=0x21;
        sectionAddr=0x2;
        switch ((int_T)mxGetPr(CHIP_ARG)[0]) {
          case 1:
            offset8255=0x0;
            break;
          case 2:
            offset8255=0x4;
            break;
        }
        break;
      case 7:
        devName = "MC PCI-DDA08/12";
        vendorId=0x1307;
        deviceId=0x22;
        sectionAddr=0x2;
        switch ((int_T)mxGetPr(CHIP_ARG)[0]) {
          case 1:
            offset8255=0x0;
            break;
          case 2:
            offset8255=0x4;
            break;
        }
        break;
      case 8:
        devName = "MC PCI-DIO24H";
        vendorId=0x1307;
        deviceId=0x14;
        sectionAddr=0x2;
        offset8255=0x0;
        break;
      case 9:
        devName = "MC PCI-DIO96H";
        vendorId=0x1307;
        deviceId=0x17;
        sectionAddr=0x2;
        switch ((int_T)mxGetPr(CHIP_ARG)[0]) {
          case 1:
            offset8255=0x0;
            break;
          case 2:
            offset8255=0x4;
            break;
          case 3:
            offset8255=0x8;
            break;
          case 4:
            offset8255=0xc;
            break;
        }
        break;
      case 10:
        devName = "MC PCI-DIO24";
        vendorId=0x1307;
        deviceId=0x28;
        sectionAddr=0x2;
        offset8255=0x0;
        break;
      case 11:
        devName = "MC PCI-DAS1602/12";
        vendorId=0x1307;
        deviceId=0x10;
        sectionAddr=0x3;
        offset8255=0x4;
        break;
      case 12:
        devName = "NI PCI-6503";
        vendorId=0x1093;
        deviceId=0x17d0;
        sectionAddr=0x1;
        offset8255=0x0;
        break;
      case 13:
        devName = "NI PCI-DIO-96";
        vendorId=0x1093;
        deviceId=0x0160;
        sectionAddr=0x1;
        switch ((int_T)mxGetPr(CHIP_ARG)[0]) {
          case 1:
            offset8255=0x0;
            break;
          case 2:
            offset8255=0x4;
            break;
          case 3:
            offset8255=0x8;
            break;
          case 4:
            offset8255=0xc;
            break;
        }
        break;
      case 14:
        devName = "NI PXI-6508";
        vendorId=0x1093;
        deviceId=0x13c0;
        sectionAddr=0x1;
        switch ((int_T)mxGetPr(CHIP_ARG)[0]) {
          case 1:
            offset8255=0x0;
            break;
          case 2:
            offset8255=0x4;
            break;
          case 3:
            offset8255=0x8;
            break;
          case 4:
            offset8255=0xc;
            break;
        }
        break;
      case 15:
        devName = "NI PCI-6025E";
        vendorId=0x1093;
        deviceId=0x2a80;
        sectionAddr=0x1;
        offset8255=0x19;
        break;
      case 16:
        devName = "MC PCI-DUAL-AC5";
        vendorId=0x1307;
        deviceId=0x33;
        sectionAddr=0x2;
        switch ((int_T)mxGetPr(CHIP_ARG)[0]) {
          case 1:
            offset8255=0x0;
            break;
          case 2:
            offset8255=0x4;
            break;
        }
        break;
      case 17:
        devName = "MC PCI-DDA02/16";
        vendorId=0x1307;
        deviceId=0x23;
        sectionAddr=0x2;
        switch ((int_T)mxGetPr(CHIP_ARG)[0]) {
          case 1:
            offset8255=0x0;
            break;
          case 2:
            offset8255=0x4;
            break;
        }
        break;
      case 18:
        devName = "MC PCI-DDA04/16";
        vendorId=0x1307;
        deviceId=0x24;
        sectionAddr=0x2;
        switch ((int_T)mxGetPr(CHIP_ARG)[0]) {
          case 1:
            offset8255=0x0;
            break;
          case 2:
            offset8255=0x4;
            break;
        }
        break;
      case 19:
        devName = "MC PCI-DDA08/16";
        vendorId=0x1307;
        deviceId=0x25;
        sectionAddr=0x2;
        switch ((int_T)mxGetPr(CHIP_ARG)[0]) {
          case 1:
            offset8255=0x0;
            break;
          case 2:
            offset8255=0x4;
            break;
        }
        break;
      case 20:
        devName = "MC PCIM-DDA06/16";
        vendorId=0x1307;
        deviceId=0x53;
        sectionAddr=0x3;
        offset8255=0x0c;
        break;
      case 21:
        devName = "MC PCIM-DAS1602/16";
        vendorId=0x1307;
        deviceId=0x56;
        sectionAddr=0x4;
        offset8255=0x00;
        break;
      case 22:
        devName = "MC PCI-DIO96";
        vendorId=0x1307;
        deviceId=0x54;
        sectionAddr=0x3;
        switch ((int_T)mxGetPr(CHIP_ARG)[0]) {
          case 1:
            offset8255=0x0;
            break;
          case 2:
            offset8255=0x4;
            break;
          case 3:
            offset8255=0x8;
            break;
          case 4:
            offset8255=0xc;
            break;
        }
        break;
      case 23:
        devName = "MC PCI-DAS1001";
        vendorId=0x1307;
        deviceId=0x1A;
        sectionAddr=0x3;
        offset8255=0x4;
        break;
      case 24:
        devName = "MC PCI-DAS1002";
        vendorId=0x1307;
        deviceId=0x1B;
        sectionAddr=0x3;
        offset8255=0x4;
        break;
    }

    if( mxGetN(SLOT_ARG) == 1 )
    {
        pciBus = 0;
        pciSlot = (int_T) mxGetPr(SLOT_ARG)[0];
    } else
    {
        pciBus = (int_T) mxGetPr(SLOT_ARG)[0];
        pciSlot = (int_T) mxGetPr(SLOT_ARG)[1];
    }

    if( GetPCIDeviceFull( vendorId, deviceId, 0xffff, 0xffff, pciBus, pciSlot, &pDev ) )
    {
        sprintf(msg, "No %s at bus %d slot %d", devName, pciBus, pciSlot);
        ssSetErrorStatus(S, msg);
        return;
    }

    // show Device Information
    //ShowPCIDevice( &pDev );

    switch ((int_T)mxGetPr(DEV_ARG)[0]) {
      case 1:
      case 2:
      case 3:
      case 4:
      case 5:
      case 6:
      case 7:
      case 8:
      case 9:
      case 10:
      case 11:
      case 16:
      case 17:
      case 18:
      case 19:
      case 20:
      case 21:
      case 22:
      case 23:
      case 24:
        base = pDev.BaseAddress[sectionAddr] + offset8255;
        ssSetIWorkValue(S, BASE_I_IND, base);
        num_channels = mxGetN(CHANNEL_ARG);

        if( xpceIsModelInit() )
        {
            // On PCI, we need to set the direction first, before writing
            // the initial values.  This leaves a short glitch on any channel
            // with initial value 1.  The glitch is on the order of a microsecond.
            // Setting control after presetting the output value doesn't work.
            control = (int_T)mxGetPr(CONTROL_ARG)[0];
            rl32eOutpB((unsigned short)(base + 3), (unsigned short)control);
            
            for( prt = 0 ; prt < 3 ; prt++ )
            {
                ival = (int_T)mxGetPr(INI_ARG)[prt];
                rl32eOutpB((unsigned short)(base+prt), ival );
            }
        }
        break;
      case 12:
      case 13:
      case 14:
      case 15:
        Physical1 = (void *)pDev.BaseAddress[1];
        Virtual1 = rl32eGetDevicePtr(Physical1, 4096, RT_PG_USERREADWRITE);
        ioaddress1 = (uint16_T *)Virtual1;

        Physical0 = (void *)pDev.BaseAddress[0];
        Virtual0 = rl32eGetDevicePtr(Physical0, 4096, RT_PG_USERREADWRITE);
        ioaddress0 = (uint32_T *)Virtual0;

        ioaddress0[48] = ((unsigned int)Physical1 & ~0xff) | 0x00000080;
        ssSetIWorkValue(S, BASE_I_IND, (uint_T)ioaddress1);
        ioaddress = (uint8_T *)ioaddress1;

        if( xpceIsModelInit() )
        {
            control = (int_T)mxGetPr(CONTROL_ARG)[0];
            if ((int_T)mxGetPr(DEV_ARG)[0]==15)
            {
                ioaddress[0x3*2+offset8255] = control;
            } else {
                ioaddress[0x3+offset8255] = control;
            }

            // Set the initial value here, after setting the direction
            // We will get the same behavior as in the IO case above.
            for( prt = 0 ; prt < 3 ; prt++ )
            {
                ival = (int_T)mxGetPr(INI_ARG)[prt];
                if ((int_T)mxGetPr(DEV_ARG)[0]==15) {
                    ioaddress[prt*2+offset8255] = ival;
                } else {
                    ioaddress[prt+offset8255] = ival;
                }
            }
        }
        ssSetIWorkValue(S, OFFSET_IND, offset8255);
        break;
    }
#endif

}

static void mdlOutputs(SimStruct *S, int_T tid)
{

#ifndef MATLAB_MEX_FILE

    int_T base = ssGetIWorkValue(S, BASE_I_IND);
    int_T port, input, channel;
    size_t  i;
    real_T  *y;
    int_T offset8255 = ssGetIWorkValue(S, OFFSET_IND);
    volatile unsigned char *ioaddress;

    port=(int_T)mxGetPr(PORT_ARG)[0]-1;

    switch ((int_T)mxGetPr(DEV_ARG)[0]) {
      case 1:
      case 2:
      case 3:
      case 4:
      case 5:
      case 6:
      case 7:
      case 8:
      case 9:
      case 10:
      case 11:
      case 16:
      case 17:
      case 18:
      case 19:
      case 20:
      case 21:
      case 22:
      case 23:
      case 24:
        input=rl32eInpB((unsigned short)(base+port));
        break;
      case 12:
      case 13:
      case 14:
        ioaddress = (void *)base;
        input=ioaddress[port+offset8255];
        break;
      case 15:
        ioaddress = (void *)base;
        input=ioaddress[port*2+offset8255];
        break;
    }

    for (i=0;i<mxGetN(CHANNEL_ARG);i++) {
        channel=(int_T)mxGetPr(CHANNEL_ARG)[i]-1;
        y=ssGetOutputPortSignal(S,i);
        y[0]=( input & (1 << channel) ) >> channel;
    }


#endif

}

static void mdlTerminate(SimStruct *S)
{
}

#ifdef MATLAB_MEX_FILE  /* Is this file being compiled as a MEX-file? */
#include "simulink.c"   /* Mex glue */
#else
#include "cg_sfun.h"    /* Code generation glue */
#endif
