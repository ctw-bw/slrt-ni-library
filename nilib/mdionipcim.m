function [maskdisplay, control, reset, initValue] = ...
        mdionipcim(flag, channel, boardType, reset, initValue)

% MDIONIPCIM - InitFcn and Mask Initialization for NI PCI-M series
% digital I/O section.
% The final two parameters (reset and initValue) are used only when
% direct = 2 (Output)  

% Copyright 1996-2011 The MathWorks, Inc.

  switch flag
   case 1
    masktype = get_param( gcb, 'MaskType' );
    dimask = ['di', masktype(3:end)];
    domask = ['do', masktype(3:end)];
    
    diblocks = find_system(bdroot, ...
                           'FollowLinks', 'on', ...
                           'LookUnderMasks', 'all', ...
                           'MaskType', dimask );
    doblocks = find_system(bdroot, ...
                           'FollowLinks', 'on', ...
                           'LookUnderMasks', 'all', ...
                           'MaskType', domask );
   
    % I need to accumulate all bits that are output for the current
    % board.
    % Two di blocks can access the same board, it's not optimum, but
    % it will work.
    % The same with multiple do blocks.  Since the do driver reads back
    % the current value and modifies it, we can have multiple blocks using
    % the same board.
   
    slot = evalin( 'base', get_param( gcb, 'slot' ));
    control = 0;  % preset to all input
                  %inputs = zeros(1,32);
    outputs = zeros(1,32);
    
    % accumulate input and output bit assignments for this board
    for i = 1 : length(diblocks)
      tmpslot = evalin( 'base', get_param( diblocks{i}, 'slot' ));
      if isequal( tmpslot, slot )
        channel = eval( get_param( diblocks{i}, 'channel' ));
        % Check to make sure all channels are in range
        if ~all( channel >= 1 )
            error(message('xPCTarget:NIMSeries:ChannelMin'));
        end
      end
    end
    for i = 1 : length(doblocks)
      tmpslot = evalin( 'base', get_param( doblocks{i}, 'slot' ));
      if isequal( tmpslot, slot )
        channel = eval( get_param( doblocks{i}, 'channel' ));
        % Check to make sure all channels are in range
        if ~all( channel >= 1 )
            error(message('xPCTarget:NIMSeries:ChannelMin'));
        end
        
        for j = 1 : length(channel)
          outputs(channel(j)) = 1;
        end
      end
    end

    % Allow an input on the same bit as an output.  This is just a readback
    % of the last value sent to the output.
    
    % create the DIO config register value, here a float.  The driver
    % converts the value to the integer register value.
    for i = 1:32
      if outputs(i) >= 1
        control = control + bitshift(1, i-1);
      end
    end
    
    set_param( gcb, 'UserData', control );
   
   case 2
    
    masktype = get_param( gcb, 'MaskType' );
    direct = masktype(1:2);  % di or do
    control = get_param( gcb, 'UserData' );
    
    family = floor(mod( boardType/10, 10 ));
    type = mod( boardType, 10 );
    name = '';
      
    if family == 2  % 622x boards
                    % 6220, 6221, 6224, 6225, 6229, 6221/37
        switch type
          case 0
            name = 'PCI-6220';
            maxChannel = 8;
          case 1
            name = 'PCI-6221';
            maxChannel = 8;
          case 4
            name = 'PCI-6224';
            maxChannel = 32;
          case 5
            name = 'PCI-6225';
            maxChannel = 8;
          case 7
            name = 'PCI-6221/37 pin';
            maxChannel = 2;
          case 9
            name = 'PCI-6229';
            maxChannel = 32;
          otherwise
            error(message('xPCTarget:NIMSeries:Unknown', boardType));
        end
    elseif family == 5  %625x boards
                        % 6250, 6251, 6254, 6255, 6259
        switch type
          case 0
            name = 'PCI-6250';
            maxChannel = 8;
          case 1
            name = 'PCI-6251';
            maxChannel = 8;
          case 4
            name = 'PCI-6254';
            maxChannel = 32;
          case 5
            name = 'PCI-6255';
            maxChannel = 8;
          case 9
            name = 'PCI-6259';
            maxChannel = 32;
          otherwise
            error(message('xPCTarget:NIMSeries:Unknown', boardType));
        end
        
    elseif family == 8  % 628x boards
                        % 6280, 6281, 6284, 6289
        switch type
          case 0
            name = 'PCI-6280';
            maxChannel = 8;
          case 1
            name = 'PCI-6281';
            maxChannel = 8;
          case 4
            name = 'PCI-6284';
            maxChannel = 32;
          case 9
            name = 'PCI-6289';
            maxChannel = 32;
          otherwise
            error(message('xPCTarget:NIMSeries:Unknown', boardType));
        end
    end
    inout = 'Input';
    if direct(2) == 'o'
        inout = 'Output';
    end
    maskdisplay = sprintf( 'disp(''%s\\nNational Instr.\\nDigital %s'');', name, inout );

    % Check to make sure all channels are in range
    if ~all( ismember( channel, 1:maxChannel ) )
        error(message('xPCTarget:NIMSeries:ChannelValue', maxChannel));
    end
    
    if direct(2) == 'i'
        for i = 1:length(channel)
            maskdisplay = sprintf('%s port_label(''output'', %d, ''%d'' );', maskdisplay, i, channel(i) );
        end
    elseif direct(2) == 'o'
        for i = 1:length(channel)
            maskdisplay = sprintf('%s port_label(''input'', %d, ''%d'' );', maskdisplay, i, channel(i) );
        end

        %  check reset vector parameter
        if all( size(reset) == 1 )
            reset = reset * ones(size(channel));
        elseif ~all(size(reset) == size(channel))
            error(message('xPCTarget:NIPCIM:ResetLength'));
        end
        if ~all(ismember(reset, [0 1]))
            error(message('xPCTarget:NIPCIM:ResetValue'));
        end

        % check initValue vector parameter
        if all( size(initValue) == 1 )
            initValue = initValue * ones(size(channel));
        elseif ~all(size(initValue) == size(channel))
            error(message('xPCTarget:NIPCIM:InitLength'));
        end
        if ~all(ismember(initValue, [0 1]))
            error(message('xPCTarget:NIPCIM:InitValue'));
        end
    end
  end
