/*
 * dinipci6528.c - xPC Target, non-inlined S-function driver for the 
 *                  digital input section of the National Instruments 6528 boards.
 *               
 *   The 6528 povides 24 input channels, and 24 output channels.  The 24 input channels are located
 *      in 3 separate ports: 0, 1, and 2, with each port containing 8 bits.  The 24 output channels are
 *      in port 3, 4, 5.   To make things consistent with the 6527 board driver, we create this mode, 
 *      which only handles the input for one port. 
 *      
 * Copyright 2006-2014 The MathWorks, Inc.
*/


#define S_FUNCTION_LEVEL   2
#undef  S_FUNCTION_NAME
#define S_FUNCTION_NAME    dinipci6528    // The name of the S-function must match the
                                          // "S-function name" found in the "Look Under Mask" menu entry


#include <stddef.h>
#include <stdlib.h>
#include "simstruc.h"

#ifndef MATLAB_MEX_FILE
#include <windows.h>
#include "xpctarget.h"
#include "dionipci6528.h"
#endif

#ifdef MATLAB_MEX_FILE
#include "mex.h"
#endif

/* List of parameters. The order must match the order in "S-function parameters" found in the "Look Under Mask" menu entry */
#define NUMBER_OF_ARGS          (7)
#define CHANNEL_ARG             ssGetSFcnParam(S,0)
#define PORT_ARG                ssGetSFcnParam(S,1)
#define SAMP_TIME_ARG           ssGetSFcnParam(S,2)
#define SLOT_ARG                ssGetSFcnParam(S,3)
#define FILTER_ARG              ssGetSFcnParam(S,4)
#define FILTER_TIME_ARG         ssGetSFcnParam(S,5)
#define DEVTYPE_ARG             ssGetSFcnParam(S,6)

/* List of I Works storage. Used to share local information between mdlStart, Outputs and Terminate */
typedef enum {
  BASE_I_IND = 0,
  NUMBER_OF_I_WORKS
} Iworks;

#define THRESHOLD (0.5)

static char_T msg[256];


static void mdlInitializeSizes(SimStruct *S)
{
    size_t i;

    /* Verify the the number of parameters passed from the mask match what is expected */
    ssSetNumSFcnParams(S, NUMBER_OF_ARGS);
    if ( ssGetNumSFcnParams(S) != ssGetSFcnParamsCount(S) ) {
        sprintf(msg,
            "Wrong number of input arguments passed.\n%d arguments are expected\n",
            NUMBER_OF_ARGS);
        ssSetErrorStatus(S, msg);
        return;
    }

    ssSetNumContStates(S, 0);
    ssSetNumDiscStates(S, 0);

    /* Create the output ports and specify the port width */
    if( !ssSetNumOutputPorts(S, (int_T)(mxGetN(CHANNEL_ARG))) )return;
    for ( i = 0 ; i < mxGetN(CHANNEL_ARG); i++ ) {
        ssSetOutputPortDataType(S, i, SS_DOUBLE);
        ssSetOutputPortWidth(S, i, 1);
    }

    if( !ssSetNumInputPorts(S, 0) )return;

    ssSetNumSampleTimes(S, 1);

    ssSetSimStateCompliance( S, HAS_NO_SIM_STATE );

    /* Reserve space for the work variables */
    ssSetNumPWork(S, 0);
    ssSetNumRWork(S, 0);
    ssSetNumIWork(S, NUMBER_OF_I_WORKS);

    ssSetNumModes(S, 0);
    ssSetNumNonsampledZCs(S, 0);

    for ( i = 0 ; i < NUMBER_OF_ARGS ; i++ ) {
        ssSetSFcnParamTunable(S, i, SS_PRM_NOT_TUNABLE);
    }

    /* Set options */
    ssSetOptions(S, SS_OPTION_DISALLOW_CONSTANT_SAMPLE_TIME | SS_OPTION_RUNTIME_EXCEPTION_FREE_CODE );
}


/* Common practice is to get the sample time from the mask and pass it into the driver.
 * This function uses the sample time value (-1 for inherit or a specified value) and passes
 * the appropriate values to the Simulink functions.
*/
static void mdlInitializeSampleTimes(SimStruct *S)
{
     if( mxGetN(SAMP_TIME_ARG) == 1 )
    {
        if( mxGetPr(SAMP_TIME_ARG)[0] == -1 )
        {
            ssSetSampleTime( S, 0, INHERITED_SAMPLE_TIME );
            ssSetOffsetTime( S, 0, FIXED_IN_MINOR_STEP_OFFSET );
            ssSetModelReferenceSampleTimeInheritanceRule(S, USE_DEFAULT_FOR_DISCRETE_INHERITANCE);
        }
        else
        {
            ssSetSampleTime( S, 0, mxGetPr(SAMP_TIME_ARG)[0] );
            ssSetOffsetTime( S, 0, 0.0 );
        }
    }
    else
    {
        ssSetSampleTime( S, 0, mxGetPr(SAMP_TIME_ARG)[0] );
        ssSetOffsetTime( S, 0, mxGetPr(SAMP_TIME_ARG)[1] );
    }
}


#define MDL_START
static void mdlStart(SimStruct *S)
{
#ifndef MATLAB_MEX_FILE
    int_T             port; //num_channels, 
    char_T            *devName;
    uint16_T           deviceId;
    int32_T           bus, slot;
    xpcPCIDevice      pciinfo;
    volatile uint32_T *Physical0, *Virtual0;
    volatile uint32_T *Physical1, *Virtual1;
    size_t            i;
    real_T filtTime;
    int_T filtCount;
    unsigned char fmask = 0;
    int_T channel, maskbit;
    
  
    switch( (int_T)mxGetPr(DEVTYPE_ARG)[0] )
    {
      case 1:
        devName = "NI PCI-6528";
        deviceId = NI_PCI_6528_ID;    /* PCI-6528 */
        break;
      case 2:
        devName = "NI PXI-6528";
        deviceId =  NI_PXI_6528_ID;    /* PXI-6528 */
        break;
    }
    
    if (mxGetN(SLOT_ARG) == 1) {
        bus = 0;
        slot = (int32_T)mxGetPr(SLOT_ARG)[0];
    } else {
        bus = (int32_T)mxGetPr(SLOT_ARG)[0];
        slot = (int32_T)mxGetPr(SLOT_ARG)[1];
    }
    // look for the PCI-Device
    if (xpcGetPCIDeviceInfo((uint16_T)NI_VENDOR_ID,deviceId,XPC_NO_SUB, XPC_NO_SUB, bus, slot, &pciinfo)) {
            sprintf(msg,"%s (bus %d,slot %d): board not present",devName, bus, slot );
            ssSetErrorStatus(S,msg);
            return;
    }
   
    // Map MITE space and mark it read/write
    Physical0 = (volatile uint32_T *)pciinfo.BaseAddress[0];
    Virtual0 = (volatile uint32_T *)xpcReserveMemoryRegion((void*)Physical0, 4096, 
        XPC_RT_PG_USERREADWRITE);

    // Map IO space and mark it read/write
    Physical1 = (volatile uint32_T *)pciinfo.BaseAddress[1];
    Virtual1 = (volatile uint32_T *)xpcReserveMemoryRegion((void*)Physical1, 8192, 
        XPC_RT_PG_USERREADWRITE);

    /* Only do the initialization once */
    if ( xpcIsModelInit() && ((volatile uint8_T *)Virtual1)[NI_ID_REG] !=  NI_LAST_TWO_DIGITS_OF_MODEL ){
       // Initialize the board
       Virtual0[NI_BAR0_INIT_REG>>2] = NI_BAR0_INIT_CODE;
       Virtual0[N1_BAR0_WINDOW_DATA_REG>>2] = ((((uint32_T)Physical1) & ~0xFF) | 0x80);// Set address window size.
    }

    /* Check the initialization result */
    if (((volatile uint8_T *)Virtual1)[NI_ID_REG] !=  NI_LAST_TWO_DIGITS_OF_MODEL ){
        sprintf(msg,"%s isn't initialized. ID register %x",devName,
            ((volatile uint8_T *)Virtual1)[NI_ID_REG] );
            ssSetErrorStatus(S,msg);
            return;
    }

    // Compute the filtering enable mask based on the FILTER_ARG vector.
    for( i = 0; i < mxGetN(CHANNEL_ARG); i++ ){
        maskbit = (int_T)mxGetPr(FILTER_ARG)[i];
        channel = (int_T)mxGetPr(CHANNEL_ARG)[i] - 1;
        fmask |= maskbit << channel;
    }

   if( fmask != 0 ){
      // Set the board filter interval from FILTER_TIME_ARG
      filtTime = (real_T)mxGetPr( FILTER_TIME_ARG )[0];
      filtCount = (int_T)( filtTime / 0.0000002 );   // Number of 200ns counts in filtTime seconds, truncated.
      filtCount &= 0x000FFFFF; //31-20 bits are reserved;
      Virtual1[NI_FILTER_INTERVAL_32BIT_REG>>2] = (uint32_T)filtCount;
   }

    // Write the filter enable mask to the hardware.
    port=(int_T)mxGetPr(PORT_ARG)[0]-1;
    ((volatile uint8_T*)Virtual1)[ NI_FILTER_ENABLE_REG + (port << 4 )] = fmask;

    // Save IO space base address for later.
    ssSetIWorkValue( S, BASE_I_IND, (uint_T)Virtual1 );

#endif
}


static void mdlOutputs(SimStruct *S, int_T tid)
{
#ifndef MATLAB_MEX_FILE
    uint8_T           input, channel;
    volatile uint8_T *base = (volatile uint8_T *)ssGetIWorkValue(S, BASE_I_IND);
    real_T            *y;
    int_T             port;
    size_t            i;

    port=(int_T)mxGetPr(PORT_ARG)[0]-1;    
    
    input = base[NI_IN_PORT_DATA_REG+(port<<4)];

    for ( i = 0 ; i < mxGetN(CHANNEL_ARG); i++ ) {
        channel = (int_T)(mxGetPr(CHANNEL_ARG)[i]) - 1;
        y = ssGetOutputPortSignal(S, i);
        y[0] = (input >> channel ) & 1;
    }
#endif
}


static void mdlTerminate(SimStruct *S)
{
}


#ifdef MATLAB_MEX_FILE  /* Is this file being compiled as a MEX-file? */
#include "simulink.c"   /* Mex glue */
#else
#include "cg_sfun.h"    /* Code generation glue */
#endif
