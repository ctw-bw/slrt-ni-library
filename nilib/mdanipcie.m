function [maskdisplay, range, reset, initValue] = ...
        mdanipcie(flag, channel, range, reset, initValue, ref, boardType) %#ok

% MADNIPCIE - InitFcn and Mask Initialization for NI PCI-E series D/A section

% Copyright 1996-2011 The MathWorks, Inc.

  switch flag
   case 1
    ck = mxpccrosscheckers;
    boards = ck.pciuniqa();
    if length(boards) > 1
      myslot = evalin( 'base', get_param( gcb, 'slot' ) );
      for bd = 1 : length(boards)
        slots{bd} = evalin('base', get_param( boards{bd}, 'slot' )); %#ok
      end
      ck.pciuniqb( boards, myslot, slots );
    end
    
   case 2
    maxChannel=2;

    switch boardType
     case 1
      maskdisplay='disp(''PCI-6023E\nNational Instr.\n';
      supRange=[];
      supRangeStr='';
      range=-10*ones(1,length(channel));
     case 2
      maskdisplay='disp(''PCI-6024E\nNational Instr.\n';
      supRange=-10;
      supRangeStr='-10';
      range=-10*ones(1,length(channel));
     case 3
      maskdisplay='disp(''PCI-6025E\nNational Instr.\n';
      supRange=-10;
      supRangeStr='-10';
      range=-10*ones(1,length(channel));
     case 4
      maskdisplay='disp(''PCI-6070E\nNational Instr.\n';
      supRange=[-10, 10];
      supRangeStr='-10, 10';
     case 5
      maskdisplay='disp(''PCI-6040E\nNational Instr.\n';
      supRange=[-10, 10];
      supRangeStr='-10, 10';
     case 6
      maskdisplay='disp(''PXI-6070E\nNational Instr.\n';
      supRange=[-10, 10];
      supRangeStr='-10, 10';
     case 7
      maskdisplay='disp(''PXI-6040E\nNational Instr.\n';
      supRange=[-10, 10];
      supRangeStr='-10, 10';
     case 8
      maskdisplay='disp(''PCI-6071E\nNational Instr.\n';
      supRange=[-10, 10];
      supRangeStr='-10, 10';
     case 9
      maskdisplay='disp(''PCI-6052E\nNational Instr.\n';
      supRange=[-10, 10];
      supRangeStr='-10, 10';
     case 10
      maskdisplay='disp(''PCI-6030E\nNational Instr.\n';
      supRange=[-10, 10];
      supRangeStr='-10, 10';
     case 11
      maskdisplay='disp(''PCI-6031E\nNational Instr.\n';
      supRange=[-10, 10];
      supRangeStr='-10, 10';
     case 12
      maskdisplay='disp(''PXI-6071E\nNational Instr.\n';
      supRange=[-10, 10];
      supRangeStr='-10, 10';
     case 13  % reserved
     case 14  % reserved
     case 15  % reserved
     case 16
      maskdisplay='disp(''PXI-6052E\nNational Instr.\n';
      supRange=[-10, 10];
      supRangeStr='-10, 10';
     case 17
     case 18
     case 19
     case 20
     case 21
      error(message('xPCTarget:NIPCIEDA:UnSupported'));  % no A/D section on these.
     case 22
      maskdisplay='disp(''PCI-6011E\nNational Instr.\n';
      supRange= -10;
      supRangeStr='-10';
    end

    maskdisplay=[maskdisplay,'Analog Output'');'];
    for i=1:length(channel)
      maskdisplay = [maskdisplay,'port_label(''input'',',num2str(i),',''',num2str(channel(i)),''');']; %#ok
    end

    if size(channel,1)~=1
      error(message('xPCTarget:NIPCIEDA:ChannelCol'));
    end
    if size(range,1)~=1
      error(message('xPCTarget:NIPCIEDA:RangeCol'));
    end
  
    % do scaler to vector expansion
    lc = length(channel);
    if length(range) ~= lc
      if length(range) == 1
        range = range * ones( 1, lc );
      else
        error(message('xPCTarget:NIPCIEDA:RangeLength'));
      end
    end
    
    chUsed = zeros( 1, maxChannel );
    for i=1:length(channel)
      chan=round(channel(i));
      rng=range(i);
      if chan < 1 || chan > maxChannel
        error(message('xPCTarget:NIPCIEDA:ChannelValue', maxChannel));
      end
      if chUsed(chan)
        error(message('xPCTarget:NIPCIEDA:DupChannel', chan));
      else
        chUsed(chan)=1;
      end
      if ~ismember(rng,supRange)
        error(message('xPCTarget:NIPCIEDA:RangeValue', supRangeStr));
      end
    end

    %%% check reset vector parameter
    if ~isa(reset, 'double')
      error(message('xPCTarget:NIPCIEDA:ResetType'));
    end
    if all(size(reset) == 1)
      reset = reset * ones(size(channel));
    elseif ~all(size(reset) == size(channel))
      error(message('xPCTarget:NIPCIEDA:ResetLength'));
    end
    reset = round(reset);
    if ~all(ismember(reset, [0 1]))
      error(message('xPCTarget:NIPCIEDA:ResetValue'));
    end

    %%% check initValue vector parameter
    if ~isa(initValue, 'double')
      error(message('xPCTarget:NIPCIEDA:InitType'));
    end
    if all(size(initValue) == 1)
      initValue = initValue * ones(size(channel));
    elseif ~all(size(initValue) == size(channel))
      error(message('xPCTarget:NIPCIEDA:InitLength'));
    end
  end
