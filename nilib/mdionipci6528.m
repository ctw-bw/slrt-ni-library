function [maskdisplay, filter, reset, initValue] = mdionipci6528( phase, channel, direct, filter, reset, initValue )

% MPCI6528 - InitFcn and Mask Initialization for the National Instruments
% PCI-6528 and PXI-6528 digital I/O-boards
% The reset and initValue parameters are used only for digital output

% Copyright 2006-2011 The MathWorks, Inc.

type = get_param(gcb, 'MaskType' );

pcipxi = type(5:7);

if phase == 1
    switch type(2)
        case 'i' % input
            direct = 1;
        case 'o' % output
            direct = 2;
    end

    if direct == 1
        block = find_system(bdroot, ...
            'FollowLinks', 'on', ...
            'LookUnderMasks', 'all', ...
            'MaskType', type );

        if length(block) > 1 % multiple blocks
            %get information for current block
            slot = str2num(get_param( gcb, 'slot' )); %#ok<ST2NM>
            filter = str2num(get_param( gcb, 'filter' )); %#ok<ST2NM>
            filterinterval = str2double(get_param( gcb, 'filtertime' ));

            for i = 1:length(block)
                % Check the filter interval for input blocks.  Any port with filtering
                % enabled on any channel must specify the same filter interval or there is
                % an error.
                % filter = vector, filterinterval = duration, seconds.

                %get slot for another block
                other_slot = str2num(get_param(block{i}, 'slot' )); %#ok<ST2NM>
                if isequal(slot, other_slot) && ~strcmp( gcb, block{i} ) && any(filter)
                    other_filter = str2num( get_param(block{i}, 'filter' ) ); %#ok<ST2NM>
                    other_filterinterval = str2double(get_param(block{i}, 'filtertime' ));
                    if any( other_filter ) > 0 && filterinterval ~= other_filterinterval
                        error(message('xPCTarget:NI6528:Filter'));
                    end
                end
            end
        end
    end
end


if phase == 2
    switch( pcipxi )
        case 'pci'
            maskdisplay='disp(''PCI-6528\nNational Instr\n';
        case 'pxi'
            maskdisplay='disp(''PXI-6528\nNational Instr\n';
    end

    if direct == 1
        maskdisplay=[maskdisplay,'Digital Input'');'];
        for i=1:length(channel)
            maskdisplay=[maskdisplay,'port_label(''output'',',num2str(i),',''',num2str(channel(i)),''');']; %#ok
        end
    elseif direct == 2
        maskdisplay=[maskdisplay,'Digital Output'');'];
        for i=1:length(channel)
            maskdisplay=[maskdisplay,'port_label(''input'',',num2str(i),',''',num2str(channel(i)),''');']; %#ok
        end
    end

    % we just have to check the channel vector for this block for dups.
    if any( channel < 1 | channel > 8 )
        error(message('xPCTarget:NI6528:ChannelValue'));
    end
    if length(channel) ~= length( unique(channel))
        error(message('xPCTarget:NI6528:DupChannel'));
    end

    if direct == 1
        % Perform scaler expansion on filter if there is a single element in the vector.
        if length(filter) == 1
            filter = filter * ones( 1, length(channel) );
        end

        % If filter wasn't length 1 and also not the same length as channel, it is an error.
        if length(filter) ~= length(channel)
            error(message('xPCTarget:NI6528:FilterLength'));
        end
    end

    if direct == 2 % if output, check reset and initValue parameters
        if ~isa(reset, 'double')
            error(message('xPCTarget:NI6528:ResetType'));
        end
        if length(reset) == 1
            reset = reset * ones(1, length(channel) );
        elseif length(reset) ~= length(channel)
            error(message('xPCTarget:NI6528:ResetLength'));
        end
        if ~all(ismember(reset, [0 1]))
            error(message('xPCTarget:NI6528:ResetValue'));
        end

        if ~isa(initValue, 'double')
            error(message('xPCTarget:NI6528:InitType'));
        end
        if length(initValue) == 1
            initValue = initValue * ones(length(channel));
        elseif length(initValue) ~= length(channel)
            error(message('xPCTarget:NI6528:InitLength'));
        end
        if ~all(ismember(initValue, [0 1]))
            error(message('xPCTarget:NI6528:InitValue'));
        end
    end
end
