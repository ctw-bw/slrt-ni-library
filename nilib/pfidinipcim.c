/* pfidionipcim.c - xPC Target, non-inlined S-function driver for
 *                  digital IO on the PFI lines
 *                  for the National Instruments M series boards.
 */
/* Copyright 1996-2014 The MathWorks, Inc.
 */

#define         S_FUNCTION_LEVEL        2
#undef          S_FUNCTION_NAME
#define         S_FUNCTION_NAME         pfidinipcim

#include        <stddef.h>
#include        <stdlib.h>

#include        "simstruc.h"

#ifdef          MATLAB_MEX_FILE
#include        "mex.h"
#endif

#ifndef         MATLAB_MEX_FILE
#include        <windows.h>
#include        "xpctarget.h"
#include        "xpcionim.h"
#endif

/* Input Arguments */
#define NUMBER_OF_ARGS          (5)
#define DEV_ARG                 ssGetSFcnParam(S, 0)
#define CHANNEL_ARG             ssGetSFcnParam(S, 1)
#define PFIDIR_ARG              ssGetSFcnParam(S, 2)
#define SAMP_TIME_ARG           ssGetSFcnParam(S, 3)
#define SLOT_ARG                ssGetSFcnParam(S, 4)

#define SAMP_TIME_IND           (0)

#define NO_R_WORKS              (0)
#define NO_I_WORKS              (0)
#define NO_P_WORKS              (1)

/* Index into PWork */
#define BASE_ADDR_P_IND         (0)

static char_T msg[256];

static void mdlInitializeSizes(SimStruct *S)
{
    size_t i, j, numInputs, numOutputs;

    ssSetNumSFcnParams(S, NUMBER_OF_ARGS);
    if (ssGetNumSFcnParams(S) != ssGetSFcnParamsCount(S))
    {
        sprintf(msg,"Wrong number of input arguments passed.\n"
                "%d arguments are expected\n", NUMBER_OF_ARGS);
        ssSetErrorStatus(S,msg);
        return;
    }

    ssSetNumContStates(S, 0);
    ssSetNumDiscStates(S, 0);

    numInputs = 0;
    if( !ssSetNumInputPorts(S, (int32_T)numInputs ) )return;
    for( i = 0 ; i < numInputs ; i++ )
    {
        ssSetInputPortWidth(             S, i, 1);
        ssSetInputPortDirectFeedThrough( S, i, 1);
        ssSetInputPortRequiredContiguous(S, i, 1);
    }

    numOutputs = mxGetN( CHANNEL_ARG );
    if( !ssSetNumOutputPorts(S, (int32_T)numOutputs ) )return;
    for( i = 0 ; i < numOutputs ; i++ )
    {
        ssSetOutputPortWidth( S, i, 1 );
    }

    ssSetNumSampleTimes(S, 1);

    ssSetSimStateCompliance( S, HAS_NO_SIM_STATE );

    ssSetNumRWork(S, NO_R_WORKS);
    ssSetNumIWork(S, NO_I_WORKS);
    ssSetNumPWork(S, NO_P_WORKS);

    ssSetNumModes(S, 0);
    ssSetNumNonsampledZCs(S, 0);

    for (j = 0; j < NUMBER_OF_ARGS; j++)
        ssSetSFcnParamNotTunable(S, j);

    ssSetOptions(S, SS_OPTION_DISALLOW_CONSTANT_SAMPLE_TIME | SS_OPTION_EXCEPTION_FREE_CODE );
    return;
}

static void mdlInitializeSampleTimes(SimStruct *S)
{
    ssSetModelReferenceSampleTimeInheritanceRule(S, USE_DEFAULT_FOR_DISCRETE_INHERITANCE);
    if (mxGetPr(SAMP_TIME_ARG)[0] == -1.0)
    {
    	ssSetSampleTime(S, 0, INHERITED_SAMPLE_TIME);
    	ssSetOffsetTime(S, 0, FIXED_IN_MINOR_STEP_OFFSET);
    }
    else
    {
        ssSetSampleTime(S, 0, mxGetPr(SAMP_TIME_ARG)[0]);
    	ssSetOffsetTime(S, 0, 0.0);
    }
}

#define MDL_START
static void mdlStart(SimStruct *S)
{
#ifndef MATLAB_MEX_FILE
    int_T bus, slot;
    xpcPCIDevice pciinfo;
    volatile uint32_T *lreg;
    volatile uint16_T *sreg;
    MseriesBoard *bd;
    int  devtype;
    uint32_T pfidir = (uint32_T)mxGetPr(PFIDIR_ARG)[0];

    if (mxGetN(SLOT_ARG) == 1)
    {
        bus = 0;
        slot = (int_T)mxGetPr(SLOT_ARG)[0];
    }
    else
    {
        bus = (int_T)mxGetPr(SLOT_ARG)[0];
        slot = (int_T)mxGetPr(SLOT_ARG)[1];
    }

    devtype = (int_T)mxGetPr(DEV_ARG)[0];

    bd = niMIdentify( S, devtype, bus, slot, &pciinfo );
    if( bd == 0 )
        return;  // We didn't find the board as described.

    lreg  = (volatile uint32_T *)pciinfo.VirtAddress[1];
    sreg  = (volatile uint16_T *)pciinfo.VirtAddress[1];

    ssSetPWorkValue(S, BASE_ADDR_P_IND, (void *)lreg);

    // Set PFI direction register
    // The mask init file looked at all blocks that use PFI bits.
    sreg[ S_IO_Bidirection_Pin ] = pfidir;
    
#endif
}

static void mdlOutputs(SimStruct *S, int_T tid)
{
#ifndef MATLAB_MEX_FILE
    uint32_T input;
    uint32_T channel;
    volatile uint16_T *sreg = ssGetPWorkValue(S, BASE_ADDR_P_IND );
    size_t i;
    real_T *y;

    input = sreg[S_PFI_DI];
    for( i = 0 ; i < mxGetN(CHANNEL_ARG) ; i++ )
    {
        channel = (int32_T)mxGetPr(CHANNEL_ARG)[i];
        y = ssGetOutputPortSignal(S,i);
        y[0] = (input >>(channel)) & 1;
    }

    return;
#endif
}

static void mdlTerminate(SimStruct *S)
{
#ifndef MATLAB_MEX_FILE
#endif
    return;
}

#ifdef MATLAB_MEX_FILE  /* Is this file being compiled as a MEX-file? */
#include "simulink.c"   /* Mex glue */
#else
#include "cg_sfun.h"    /* Code generation glue */
#endif
