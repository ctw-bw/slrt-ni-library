/* dopci6528.h - xPC Target, non-inlined S-function driver for digital output */
/* section for the National Instruments 6528 board */
/* Copyright 2006 The MathWorks, Inc.
*/

/* ID value */
#define NI_VENDOR_ID          (0x1093)
#define NI_PCI_6528_ID        (0x70A9)
#define NI_PXI_6528_ID        (0x7086)

/* Initialization registers */
#define NI_BAR0_INIT_REG           (0x340)
#define NI_BAR0_INIT_CODE          (0x0000AEAE)
#define N1_BAR0_WINDOW_DATA_REG    (0xC0)

/* non-recurring registers and expecting reading */
#define NI_FILTER_INTERVAL_32BIT_REG      (0x08)
#define NI_ID_REG                         (0)
#define NI_LAST_TWO_DIGITS_OF_MODEL       (0x28)

/* recurring registers */
#define NI_FILTER_ENABLE_REG         (0x44)
#define NI_IN_PORT_DATA_REG          (0x40)
#define NI_OUT_PORT_DATA_REG         (0x70)
