/* dopci8255.c - xPC Target, non-inlined S-function driver for
 * digital output section for PCI boards using the 8255 chip
 * Copyright 1996-2009 The MathWorks, Inc.
*/

// Note: when adding a new DEV_ARG, be sure to add a case to all four DEV_ARG switches

#define         S_FUNCTION_LEVEL        2
#undef          S_FUNCTION_NAME
#define         S_FUNCTION_NAME         dopci8255

#include        <stddef.h>
#include        <stdlib.h>

#include        "simstruc.h"

#ifdef          MATLAB_MEX_FILE
#include        "mex.h"
#endif

#ifndef         MATLAB_MEX_FILE
#include        <windows.h>
#include        "deviceinfo.h"
#include        "xpcimports.h"
#endif



/* Input Arguments */
#define NUMBER_OF_ARGS          (10)
#define CHANNEL_ARG             ssGetSFcnParam(S,0)
#define PORT_ARG                ssGetSFcnParam(S,1)
#define RESET_ARG               ssGetSFcnParam(S,2)
#define INIT_VAL_ARG            ssGetSFcnParam(S,3)
#define INI_ARG                 ssGetSFcnParam(S,4)
#define CHIP_ARG                ssGetSFcnParam(S,5)
#define SAMP_TIME_ARG           ssGetSFcnParam(S,6)
#define SLOT_ARG                ssGetSFcnParam(S,7)
#define CONTROL_ARG             ssGetSFcnParam(S,8)
#define DEV_ARG                 ssGetSFcnParam(S,9)


#define SAMP_TIME_IND           (0)
#define BASE_ADDR_IND           (0)

#define NO_I_WORKS              (2)
#define BASE_I_IND              (0)
#define OFFSET_IND              (1)

#define NO_R_WORKS              (0)

#define THRESHOLD               0.5

static char_T msg[256];

static void mdlInitializeSizes(SimStruct *S)
{
    size_t i;

    ssSetNumSFcnParams(S, NUMBER_OF_ARGS);
    if (ssGetNumSFcnParams(S) != ssGetSFcnParamsCount(S)) {
        sprintf(msg,"Wrong number of input arguments passed.\n%d arguments are expected\n",NUMBER_OF_ARGS);
        ssSetErrorStatus(S,msg);
        return;
    }

    ssSetNumContStates(S, 0);
    ssSetNumDiscStates(S, 0);

    if( !ssSetNumInputPorts(S, (int_T)mxGetN(CHANNEL_ARG)) )return;
    for (i = 0; i < mxGetN(CHANNEL_ARG); i++) {
        ssSetInputPortWidth(S, i, 1);
        ssSetInputPortDirectFeedThrough(S, i, 1);
    }

    if( !ssSetNumOutputPorts(S, 0) )return;

    ssSetNumSampleTimes(S, 1);

    ssSetSimStateCompliance( S, HAS_NO_SIM_STATE );

    ssSetNumRWork(S, NO_R_WORKS);
    ssSetNumIWork(S, NO_I_WORKS);
    ssSetNumPWork(S, 0);

    ssSetNumModes(S, 0);
    ssSetNumNonsampledZCs(S, 0);

    for (i = 0; i < NUMBER_OF_ARGS; i++)
        ssSetSFcnParamTunable(S, i, 0);

    ssSetOptions(S, SS_OPTION_DISALLOW_CONSTANT_SAMPLE_TIME | SS_OPTION_EXCEPTION_FREE_CODE );
}

static void mdlInitializeSampleTimes(SimStruct *S)
{
    ssSetSampleTime(S, 0, mxGetPr(SAMP_TIME_ARG)[SAMP_TIME_IND]);
    ssSetOffsetTime(S, 0, 0.0);
}

#define MDL_START
static void mdlStart(SimStruct *S)
{

#ifndef MATLAB_MEX_FILE

    PciDevice     pDev;
    int32_T  pciBus, pciSlot;
    int32_T  base, control;
    uint16_T  vendorId, deviceId;
    int32_T  sectionAddr, offset8255;
    int_T ival, prt;
    void *Physical1, *Physical0;
    void *Virtual1, *Virtual0;
    volatile uint32_T *ioaddress0;
    volatile uint16_T *ioaddress1;
    volatile uint8_T *ioaddress;

    const uint8_T *devName;

    switch ((int_T)mxGetPr(DEV_ARG)[0]) {
      case 1:
        devName = "CB PCI-DAS1602/16";
        vendorId=0x1307;
        deviceId=0x1;
        sectionAddr=0x3;
        offset8255=0x4;
        break;
      case 2:
        devName = "CB PCI-DAS1200";
        vendorId=0x1307;
        deviceId=0xf;
        sectionAddr=0x3;
        offset8255=0x4;
        break;
      case 3:
        devName = "CB PCI-DAS1200/JR";
        vendorId=0x1307;
        deviceId=0x19;
        sectionAddr=0x3;
        offset8255=0x4;
        break;
      case 4:
        devName = "CB PCI-DIO48H";
        vendorId=0x1307;
        deviceId=0xb;
        sectionAddr=0x2;
        switch ((int_T)mxGetPr(CHIP_ARG)[0]) {
          case 1:
            offset8255=0x0;
            break;
          case 2:
            offset8255=0x4;
            break;
        }
        break;
      case 5:
        devName = "CB PCI-DDA02/12";
        vendorId=0x1307;
        deviceId=0x20;
        sectionAddr=0x2;
        switch ((int_T)mxGetPr(CHIP_ARG)[0]) {
          case 1:
            offset8255=0x0;
            break;
          case 2:
            offset8255=0x4;
            break;
        }
        break;
      case 6:
        devName = "CB PCI-DDA04/12";
        vendorId=0x1307;
        deviceId=0x21;
        sectionAddr=0x2;
        switch ((int_T)mxGetPr(CHIP_ARG)[0]) {
          case 1:
            offset8255=0x0;
            break;
          case 2:
            offset8255=0x4;
            break;
        }
        break;
      case 7:
        devName = "CB PCI-DDA08/12";
        vendorId=0x1307;
        deviceId=0x22;
        sectionAddr=0x2;
        switch ((int_T)mxGetPr(CHIP_ARG)[0]) {
          case 1:
            offset8255=0x0;
            break;
          case 2:
            offset8255=0x4;
            break;
        }
        break;
      case 8:
        devName = "CB PCI-DIO24H";
        vendorId=0x1307;
        deviceId=0x14;
        sectionAddr=0x2;
        offset8255=0x0;
        break;
      case 9:
        devName = "CB PCI-DIO96H";
        vendorId=0x1307;
        deviceId=0x17;
        sectionAddr=0x2;
        switch ((int_T)mxGetPr(CHIP_ARG)[0]) {
          case 1:
            offset8255=0x0;
            break;
          case 2:
            offset8255=0x4;
            break;
          case 3:
            offset8255=0x8;
            break;
          case 4:
            offset8255=0xc;
            break;
        }
        break;
      case 10:
        devName = "CB PCI-DIO24";
        vendorId=0x1307;
        deviceId=0x28;
        sectionAddr=0x2;
        offset8255=0x0;
        break;
      case 11:
        devName = "CB PCI-DAS1602/12";
        vendorId=0x1307;
        deviceId=0x10;
        sectionAddr=0x3;
        offset8255=0x4;
        break;
      case 12:
        devName = "NI PCI-6503";
        vendorId=0x1093;
        deviceId=0x17d0;
        sectionAddr=0x1;
        offset8255=0x0;
        break;
      case 13:
        devName = "NI PCI-DIO-96";
        vendorId=0x1093;
        deviceId=0x0160;
        sectionAddr=0x1;
        switch ((int_T)mxGetPr(CHIP_ARG)[0]) {
          case 1:
            offset8255=0x0;
            break;
          case 2:
            offset8255=0x4;
            break;
          case 3:
            offset8255=0x8;
            break;
          case 4:
            offset8255=0xc;
            break;
        }
        break;
      case 14:
        devName = "NI PXI-6508";
        vendorId=0x1093;
        deviceId=0x13c0;
        sectionAddr=0x1;
        switch ((int_T)mxGetPr(CHIP_ARG)[0]) {
          case 1:
            offset8255=0x0;
            break;
          case 2:
            offset8255=0x4;
            break;
          case 3:
            offset8255=0x8;
            break;
          case 4:
            offset8255=0xc;
            break;
        }
        break;
      case 15:
        devName = "NI PCI-6025E";
        vendorId=0x1093;
        deviceId=0x2a80;
        sectionAddr=0x1;
        offset8255=0x19;
        break;
      case 16:
        devName = "CB PCI-DUAL-AC5";
        vendorId=0x1307;
        deviceId=0x33;
        sectionAddr=0x2;
        switch ((int_T)mxGetPr(CHIP_ARG)[0]) {
          case 1:
            offset8255=0x0;
            break;
          case 2:
            offset8255=0x4;
            break;
        }
        break;
      case 17:
        devName = "CB PCI-DDA02/16";
        vendorId=0x1307;
        deviceId=0x23;
        sectionAddr=0x2;
        switch ((int_T)mxGetPr(CHIP_ARG)[0]) {
          case 1:
            offset8255=0x0;
            break;
          case 2:
            offset8255=0x4;
            break;
        }
        break;
      case 18:
        devName = "CB PCI-DDA04/16";
        vendorId=0x1307;
        deviceId=0x24;
        sectionAddr=0x2;
        switch ((int_T)mxGetPr(CHIP_ARG)[0]) {
          case 1:
            offset8255=0x0;
            break;
          case 2:
            offset8255=0x4;
            break;
        }
        break;
      case 19:
        devName = "CB PCI-DDA08/16";
        vendorId=0x1307;
        deviceId=0x25;
        sectionAddr=0x2;
        switch ((int_T)mxGetPr(CHIP_ARG)[0]) {
          case 1:
            offset8255=0x0;
            break;
          case 2:
            offset8255=0x4;
            break;
        }
        break;
      case 20:
        devName = "CB PCIM-DDA06/16";
        vendorId=0x1307;
        deviceId=0x53;
        sectionAddr=0x3;
        offset8255=0x0c;
        break;
      case 21:
        devName = "CB PCIM-DAS1602/16";
        vendorId=0x1307;
        deviceId=0x56;
        sectionAddr=0x4;
        offset8255=0x00;
        break;
      case 22:
        devName = "CB PCI-DIO96";
        vendorId=0x1307;
        deviceId=0x54;
        sectionAddr=0x3;
        switch ((int_T)mxGetPr(CHIP_ARG)[0]) {
          case 1:
            offset8255=0x0;
            break;
          case 2:
            offset8255=0x4;
            break;
          case 3:
            offset8255=0x8;
            break;
          case 4:
            offset8255=0xc;
            break;
        }
        break;
      case 23:
        devName = "CB (MC) PCI-DAS1001";
        vendorId=0x1307;
        deviceId=0x1A;
        sectionAddr=0x3;
        offset8255=0x4;
        break;
      case 24:
        devName = "CB (MC) PCI-DAS1002";
        vendorId=0x1307;
        deviceId=0x1B;
        sectionAddr=0x3;
        offset8255=0x4;
        break;
    }

    if( mxGetN(SLOT_ARG) == 1 )
    {
        pciBus = 0;
        pciSlot = (int_T) mxGetPr(SLOT_ARG)[0];
    } else
    {
        pciBus = (int_T) mxGetPr(SLOT_ARG)[0];
        pciSlot = (int_T) mxGetPr(SLOT_ARG)[1];
    }

    if( GetPCIDeviceFull( vendorId, deviceId, 0xffff, 0xffff, pciBus, pciSlot, &pDev ) )
    {
        sprintf(msg, "No %s at bus %d slot %d", devName, pciBus, pciSlot);
        ssSetErrorStatus(S, msg);
        return;
    }

    // show Device Information
    //ShowPCIDevice( &pDev );

    switch ((int_T)mxGetPr(DEV_ARG)[0]) {
      case 1:
      case 2:
      case 3:
      case 4:
      case 5:
      case 6:
      case 7:
      case 8:
      case 9:
      case 10:
      case 11:
      case 16:
      case 17:
      case 18:
      case 19:
      case 20:
      case 21:
      case 22:
      case 23:
      case 24:
        base = pDev.BaseAddress[sectionAddr]+offset8255;
        ssSetIWorkValue(S, BASE_I_IND, base);

        if( xpceIsModelInit() )
        {
            control = (int32_T)mxGetPr(CONTROL_ARG)[0];

            // On PCI, we need to set the direction first, before writing
            // the initial values.  This leaves a short glitch on any channel
            // with initial value 1.  The glitch is on the order of a microsecond.
            // Setting control after presetting the output value doesn't work.
            rl32eOutpB((uint16_T)(base+3), (uint16_T)control);
            
            for( prt = 0 ; prt < 3 ; prt++ )
            {
                ival = (int_T)mxGetPr(INI_ARG)[prt];
                rl32eOutpB((unsigned short)(base+prt), ival );
            }
        }
        break;
      case 12:
      case 13:
      case 14:
      case 15:
        if( pDev.BaseAddress[1] == 0 )
        {
            sprintf(msg,"Board base address came up 0, can't run." );
            ssSetErrorStatus(S,msg);
            return;
        }
        ssSetIWorkValue(S, OFFSET_IND, offset8255);

        Physical1  = (void *)pDev.BaseAddress[1];
        Virtual1   = rl32eGetDevicePtr(Physical1, 4096, RT_PG_USERREADWRITE);
        ioaddress1 = (uint16_T *)Virtual1;

        Physical0  = (void *)pDev.BaseAddress[0];
        Virtual0   = rl32eGetDevicePtr(Physical0, 4096, RT_PG_USERREADWRITE);
        ioaddress0 = (volatile unsigned int *)Virtual0;

        ioaddress0[48] = ((unsigned int)Physical1 & ~0xff) | 0x80;
        ssSetIWorkValue(S, BASE_I_IND, (uint_T)ioaddress1);

        ioaddress = (volatile unsigned char *)ioaddress1;

        if( xpceIsModelInit() )
        {
            control=(int_T)mxGetPr(CONTROL_ARG)[0];
            if ((int_T)mxGetPr(DEV_ARG)[0]==15)
            {
                ioaddress[0x3*2+offset8255] = control;
            } else {
                ioaddress[0x3+offset8255] = control;
            }
            // Set the initial value here, after setting the direction.
            // We will get the same behavior as in the IO case above.
            for( prt = 0 ; prt < 3 ; prt++ )
            {
                ival = (int_T)mxGetPr(INI_ARG)[prt];
                if ((int_T)mxGetPr(DEV_ARG)[0]==15)
                {
                    ioaddress[prt*2+offset8255] = ival;
                } else {
                    ioaddress[prt+offset8255] = ival;
                }
            }
        }
        break;
    }


#endif

}

static void mdlOutputs(SimStruct *S, int_T tid)
{

#ifndef MATLAB_MEX_FILE

    int_T base = ssGetIWorkValue(S, BASE_I_IND);
    int_T port;
    size_t  i;
    uchar_T output, channel;
    InputRealPtrsType uPtrs;
    int_T offset8255 = ssGetIWorkValue(S, OFFSET_IND);
    volatile unsigned char *ioaddress;

    port=(int_T)mxGetPr(PORT_ARG)[0]-1;

    switch ((int_T)mxGetPr(DEV_ARG)[0]) {
      case 1:
      case 2:
      case 3:
      case 4:
      case 5:
      case 6:
      case 7:
      case 8:
      case 9:
      case 10:
      case 11:
      case 16:
      case 17:
      case 18:
      case 19:
      case 20:
      case 21:
      case 22:
      case 23:
      case 24:
        output = (uchar_T)rl32eInpB((unsigned short)(base+port));
        for (i=0;i<mxGetN(CHANNEL_ARG);i++) {
            channel = (uchar_T)mxGetPr(CHANNEL_ARG)[i];
            uPtrs = ssGetInputPortRealSignalPtrs(S,i);
            if (*uPtrs[0] >= THRESHOLD) {
                output |= 1 << (channel-1);
            } else {
                output &= ~(1 << (channel-1));
            }
        }
        rl32eOutpB((unsigned short)(base+port), (unsigned short)(output));
        break;
      case 12:
      case 13:
      case 14:
        ioaddress = (volatile unsigned char *) base;
        output = ioaddress[port+offset8255];
        for (i=0;i<mxGetN(CHANNEL_ARG);i++) {
            uPtrs=ssGetInputPortRealSignalPtrs(S,i);
            channel = (uchar_T)mxGetPr(CHANNEL_ARG)[i];
            if (*uPtrs[0] >= THRESHOLD) {
                output |= 1 << (channel-1);
            } else {
                output &= ~(1 << (channel-1));
            }
        }
        ioaddress[port+offset8255] = output;
        break;
      case 15:
        ioaddress=(volatile unsigned char *) base;
        output = ioaddress[port*2+offset8255];
        for (i=0;i<mxGetN(CHANNEL_ARG);i++) {
            uPtrs=ssGetInputPortRealSignalPtrs(S,i);
            channel = (uchar_T)mxGetPr(CHANNEL_ARG)[i];
            if (*uPtrs[0] >= THRESHOLD) {
                output |= 1 << (channel-1);
            } else {
                output &= ~(1 << (channel-1));
            }
        }
        ioaddress[port*2+offset8255] = output;
        break;
    }
#endif
}

static void mdlTerminate(SimStruct *S)
{

#ifndef MATLAB_MEX_FILE

    int_T base = ssGetIWorkValue(S, BASE_I_IND);
    int_T port = (int_T)mxGetPr(PORT_ARG)[0] - 1;
    int_T offset8255 = ssGetIWorkValue(S, OFFSET_IND);
    uint_T channel;
    size_t i;
    volatile unsigned char *ioaddress;
    unsigned char bit, init, reset, mask, output;

    // At load time, don't do anything, we set initial value in mdlStart
    if( xpceIsModelInit() )
        return;
    
    // At model termination, reset resettable channels to their initial values.

    init = reset = mask = 0;

    for (i = 0; i < mxGetN(CHANNEL_ARG); i++) {
        channel = (uint_T)(mxGetPr(CHANNEL_ARG)[i] - 1);
        bit = 1 << channel;
        mask |= bit;
        if ((uint_T)mxGetPr(INIT_VAL_ARG)[i] > 0)
            init |= bit;
        if ((uint_T)mxGetPr(RESET_ARG)[i] > 0)
            reset |= bit;
    }

    if (!xpceIsModelInit())
    {
        mask &= reset;
        init &= reset;
    }
    
    switch ((int_T)mxGetPr(DEV_ARG)[0]) {
      case 1:
      case 2:
      case 3:
      case 4:
      case 5:
      case 6:
      case 7:
      case 8:
      case 9:
      case 10:
      case 11:
      case 16:
      case 17:
      case 18:
      case 19:
      case 20:
      case 21:
      case 22:
      case 23:
      case 24:
        output = (unsigned char)rl32eInpB((unsigned short)(base+port));
        output &= ~mask;
        output |= init;
        rl32eOutpB((unsigned short)(base+port), output);
        break;
      case 12:
      case 13:
      case 14:
        ioaddress=(volatile unsigned char *) base;
        output = ioaddress[port+offset8255];
        output &= ~mask;
        output |= init;
        ioaddress[port+offset8255] = output;
        break;
      case 15:
        ioaddress=(volatile unsigned char *) base;
        output = ioaddress[port*2+offset8255];
        output &= ~mask;
        output |= init;
        ioaddress[port*2+offset8255] = output;
        break;
    }
#endif

}

#ifdef MATLAB_MEX_FILE  /* Is this file being compiled as a MEX-file? */
#include "simulink.c"   /* Mex glue */
#else
#include "cg_sfun.h"    /* Code generation glue */
#endif
