function [maskdisplay, orange, ocoupling, oscantime] ...
        = madnipcim(flag, channel, range, coupling, scantime, boardType)

% MADNIPCIM - InitFcn and Mask Initialization for NI PCI-M series A/D section

% Copyright 1996-2011 The MathWorks, Inc.

  switch flag
    case 1  % Initfcn time, once per update per block
      ck = mxpccrosscheckers;
      boards = ck.pciuniqa();
      if length(boards) > 1
          myslot = evalin( 'base', get_param( gcb, 'slot' ) );
          for bd = 1 : length(boards)
              slots{bd} = evalin('base', get_param( boards{bd}, 'slot' )); %#ok
          end
          ck.pciuniqb( boards, myslot, slots );
      end
   
    case 2  % mask init time, can be called multiple times at update
      
      family = floor(mod( boardType/10, 10 ));
      type = floor(mod( boardType, 10 ));
      name = 'Unknown'; %#ok<NASGU>
      
      if family == 2  % 622x boards
                      % 6220, 6221, 6224, 6225, 6229
                      % The index into supRange is 1 more than the code
                      % that the driver needs to put into the channel ram
                      % for the gain.  For the 622x boards, the +-10 volt
                      % range is gain code 0.  +-5 is gain code 1, etc.
          supRange = [10, 5, 0, 0, 1, 0.2 ];
          supRangeStr = '10, 5, 1, 0.2';
          minscantime = [80/2e7, 80/2e7];
          
          switch type
            case 0
              name = 'PCI-6220';
              maxChannel = 16;
            case 1
              name = 'PCI-6221';
              maxChannel = 16;
            case 4
              name = 'PCI-6224';
              maxChannel = 32;
            case 5
              name = 'PCI-6225';
              maxChannel = 80;
            case 7
              name = 'PCI-6221/37 pin';
              maxChannel = 16;
            case 9
              name = 'PCI-6229';
              maxChannel = 32;
            otherwise
              error(message('xPCTarget:NIMSeries:UnknownType', boardType));
          end
          % We will add family == 3 for the 623x boards at some point
      elseif family == 5  %625x boards
                          % 6250, 6251, 6254, 6255, 6259
          supRange = [0, 10, 5, 2, 1, 0.5, 0.2, 0.1];
          supRangeStr = '10, 5, 2, 1, 0.5, 0.2, 0.1';
          minscantime = [16/2e7, 20/2e7];

          switch type
            case 0
              name = 'PCI-6250';
              maxChannel = 16;
            case 1
              name = 'PCI-6251';
              maxChannel = 16;
            case 4
              name = 'PCI-6254';
              maxChannel = 32;
            case 5
              name = 'PCI-6255';
              maxChannel = 80;
            case 9
              name = 'PCI-6259';
              maxChannel = 32;
            otherwise
              error(message('xPCTarget:NIMSeries:UnknownType', boardType));
          end
          
      elseif family == 8  % 628x boards
                          % 6280, 6281, 6284, 6289
          supRange = [0, 10, 5, 2, 1, 0.5, 0.2, 0.1];
          supRangeStr = '10, 5, 2, 1, 0.5, 0.2, 0.1';
          minscantime = [30/2e7, 40/2e7];

          switch type
            case 0
              name = 'PCI-6280';
              maxChannel = 16;
            case 1
              name = 'PCI-6281';
              maxChannel = 16;
            case 4
              name = 'PCI-6284';
              maxChannel = 32;
            case 9
              name = 'PCI-6289';
              maxChannel = 32;
            otherwise
              error(message('xPCTarget:NIMSeries:UnknownType', boardType));
          end
      else
          error(message('xPCTarget:NIMSeries:UnknownFamily', family));
      end
      maskdisplay = sprintf( 'disp(''%s\\nNational Instr.\\nAnalog Input'');', name );
      
      for i=1:length(channel)
          maskdisplay = sprintf('%s port_label(''output'', %d, ''%d'');', maskdisplay, i, channel(i) );
      end

      % do scaler to vector expansion
      lc = length(channel);
      if length(range) ~= lc
          if length(range) == 1
              range = range * ones( 1, lc );
          else
              error(message('xPCTarget:NIPCIM:RangeLength'));
          end
      end

      if length(coupling) ~= lc
          if length(coupling) == 1
              coupling = coupling * ones( 1, lc );
          else
              error(message('xPCTarget:NIPCIM:CouplingLength'));
          end
      end

      orange = zeros( 1, length(channel) );
      ocoupling = zeros( 1, length(channel) );
    
      for i=1:length(channel)

          cpl = coupling(i);
          % change the order, cpl = 0 is RSE, driver needs 3
          %                   cpl = 1 is NRSE, driver needs 2
          %                   cpl = 2 is DIFF, driver needs 1
          %                   cpl = -1 is calibration, driver needs 0.  Puts all chans in cal mode.
          ocpl = find( cpl == [2, 1, 0, -1] );
          if isempty( ocpl )
              error(message('xPCTarget:NIPCIM:CouplingValue'));
          end
          if ocpl == 4
              ocpl = 0;  % type calibration
          end
          ocoupling(i) = ocpl;
      
          rng = abs( range(i) );  % 10 and -10 both choose the +-10 range, ignore the sign.
          orng = find( rng == supRange );
          if isempty( orng )
              error(message('xPCTarget:NIPCIM:RangeValue', rng, supRangeStr));
          end
          orange(i) = orng - 1;
      
          chan = channel(i);
          if (chan < 1) || (chan > maxChannel)
              error(message('xPCTarget:NIPCIM:ChannelValue', maxChannel));
          end
      end
      % just silently limit the minimum to either the single channel or
      % multi channel minimum for the board.
      if length(channel) == 1
          oscantime = max( scantime, minscantime(1) );
      else
          oscantime = max( scantime, minscantime(2) );
      end
  end
