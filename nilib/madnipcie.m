function [maskdisplay, range, coupling] = madnipcie(flag, channel, range, coupling, ref, boardType) %#ok

% MADNIPCIE - InitFcn and Mask Initialization for NI PCI-E series A/D section

% Copyright 1996-2007 The MathWorks, Inc.

  switch flag
   case 1
    ck = mxpccrosscheckers;
    boards = ck.pciuniqa();
    if length(boards) > 1
      myslot = evalin( 'base', get_param( gcb, 'slot' ) );
      for bd = 1 : length(boards)
        slots{bd} = evalin('base', get_param( boards{bd}, 'slot' )); %#ok
      end
      ck.pciuniqb( boards, myslot, slots );
    end
   
   case 2
    maxChannel=16;

    switch boardType
     case 1
      maskdisplay='disp(''PCI-6023E\nNational Instr.\n';
      supRange=[-10, -5, -0.5, -0.05];
      supRangeStr='-10, -5, -0.5, -0.05';
     case 2
      maskdisplay='disp(''PCI-6024E\nNational Instr.\n';
      supRange=[-10, -5, -0.5, -0.05];
      supRangeStr='-10, -5, -0.5, -0.05';
     case 3
      maskdisplay='disp(''PCI-6025E\nNational Instr.\n';
      supRange=[-10, -5, -0.5, -0.05];
      supRangeStr='-10, -5, -0.5, -0.05';
     case 4
      maskdisplay='disp(''PCI-6070E\nNational Instr.\n';
      supRange=[-10,-5,-2.5,-1,-0.5,-0.25,-0.1,-0.05,10,5,2,1,0.5,0.2,0.1];
      supRangeStr='-10, -5, -2.5, -1, -0.5, -0.25, -0.1, -0.05, 10, 5, 2, 1, 0.5, 0.2, 0.1';
     case 5
      maskdisplay='disp(''PCI-6040E\nNational Instr.\n';
      supRange=[-10,-5,-2.5,-1,-0.5,-0.25,-0.1,-0.05,10,5,2,1,0.5,0.2,0.1];
      supRangeStr='-10, -5, -2.5, -1, -0.5, -0.25, -0.1, -0.05, 10, 5, 2, 1, 0.5, 0.2, 0.1';
     case 6
      maskdisplay='disp(''PXI-6070E\nNational Instr.\n';
      supRange=[-10,-5,-2.5,-1,-0.5,-0.25,-0.1,-0.05,10,5,2,1,0.5,0.2,0.1];
      supRangeStr='-10, -5, -2.5, -1, -0.5, -0.25, -0.1, -0.05, 10, 5, 2, 1, 0.5, 0.2, 0.1';
     case 7
      maskdisplay='disp(''PXI-6040E\nNational Instr.\n';
      supRange=[-10,-5,-2.5,-1,-0.5,-0.25,-0.1,-0.05,10,5,2,1,0.5,0.2,0.1];
      supRangeStr='-10, -5, -2.5, -1, -0.5, -0.25, -0.1, -0.05, 10, 5, 2, 1, 0.5, 0.2, 0.1';
     case 8
      maskdisplay='disp(''PCI-6071E\nNational Instr.\n';
      supRange=[-10,-5,-2.5,-1,-0.5,-0.25,-0.1,-0.05,10,5,2,1,0.5,0.2,0.1];
      supRangeStr='-10, -5, -2.5, -1, -0.5, -0.25, -0.1, -0.05, 10, 5, 2, 1, 0.5, 0.2, 0.1';
      maxChannel=64;
     case 9
      maskdisplay='disp(''PCI-6052E\nNational Instr.\n';
      supRange=[-10,-5,-2.5,-1,-0.5,-0.25,-0.1,-0.05,10,5,2,1,0.5,0.2,0.1];
      supRangeStr='-10, -5, -2.5, -1, -0.5, -0.25, -0.1, -0.05, 10, 5, 2, 1, 0.5, 0.2, 0.1';
     case 10
      maskdisplay='disp(''PCI-6030E\nNational Instr.\n';
      supRange=[-10,-5,-2,-1,-0.5,-0.2,-0.1,-0.05,10,5,2,1,0.5,0.2,0.1];
      supRangeStr='-10, -5, -2, -1, -0.5, -0.2, -0.1, 10, 5, 2, 1, 0.5, 0.2, 0.1';
     case 11
      maskdisplay='disp(''PCI-6031E\nNational Instr.\n';
      supRange=[-10,-5,-2,-1,-0.5,-0.2,-0.1,-0.05,10,5,2,1,0.5,0.2,0.1];
      supRangeStr='-10, -5, -2, -1, -0.5, -0.2, -0.1, 10, 5, 2, 1, 0.5, 0.2, 0.1';
      maxChannel=64;
     case 12
      maskdisplay='disp(''PXI-6071E\nNational Instr.\n';
      supRange=[-10,-5,-2.5,-1,-0.5,-0.25,-0.1,-0.05,10,5,2,1,0.5,0.2,0.1];
      supRangeStr='-10, -5, -2.5, -1, -0.5, -0.25, -0.1, -0.05, 10, 5, 2, 1, 0.5, 0.2, 0.1';
      maxChannel=64;
     case 13
     case 14
     case 15
      error(message('xPCTarget:NIPCIE:NoAD'));
     case 16
      maskdisplay='disp(''PXI-6052E\nNational Instr.\n';
      supRange=[-10,-5,-2.5,-1,-0.5,-0.25,-0.1,-0.05,10,5,2,1,0.5,0.2,0.1];
      supRangeStr='-10, -5, -2.5, -1, -0.5, -0.25, -0.1, -0.05, 10, 5, 2, 1, 0.5, 0.2, 0.1';
     case 17
     case 18
     case 19
     case 20
     case 21
      error(message('xPCTarget:NIPCIE:NoAD'));  % no A/D section on these.
     case 22
      maskdisplay='disp(''PCI-6011E\nNational Instr.\n';
      supRange=[-10,-5,-1,-0.1,10,5,1,0.1];
      supRangeStr='-10, -5, -1, -0.1, 10, 5, 1, 0.1';
    end

    maskdisplay=[maskdisplay,'Analog Input'');'];
    for i=1:length(channel)
      maskdisplay = [maskdisplay,'port_label(''output'',',num2str(i),',''',num2str(channel(i)),''');']; %#ok
    end

    if size(channel,1)~=1
      error(message('xPCTarget:NIPCIE:ChannelRow'));
    end
    if size(range,1)~=1
      error(message('xPCTarget:NIPCIE:RangeRow'));
    end
    if size(coupling,1)~=1
      error(message('xPCTarget:NIPCIE:CouplingRow'));
    end

    % do scaler to vector expansion
    lc = length(channel);
    if length(range) ~= lc
      if length(range) == 1
        range = range * ones( 1, lc );
      else
        error(message('xPCTarget:NIPCIE:RangeLength'));
      end
    end

    if length(coupling) ~= lc
      if length(coupling) == 1
        coupling = coupling * ones( 1, lc );
      else
        error(message('xPCTarget:NIPCIE:CouplingLength'));
      end
    end

    chUsed = zeros(1, maxChannel);
    for i=1:length(channel)
      chan=round(channel(i));
      rng=range(i);
      cpl=round(coupling(i));
      if ~ismember(cpl,[0,1,2])
        error(message('xPCTarget:NIPCIE:CouplingValue'));
      end
      if ~ismember(rng,supRange)
        error(message('xPCTarget:NIPCIE:RangeValue', supRangeStr));
      end
      if cpl<2
        if chan < 1 || chan > maxChannel
          error(message('xPCTarget:NIPCIE:ChannelValue', maxChannel));
        end
        if chUsed(chan)
          error(message('xPCTarget:NIPCIE:ChannelInUse', chan));
        else
          chUsed(chan)=1;
        end
      else  
        if chan < 1 || mod(chan,16)>8
          if maxChannel == 16
            tmp='1..8';
          elseif maxChannel == 64
            tmp='1..8,17..24,33..40,49..56';
          end
          error(message('xPCTarget:NIPCIE:DiffChannels', tmp));
        end
        if chUsed(chan)
          error(message('xPCTarget:NIPCIE:ChanUsed', chan));
        else
          chUsed(chan)=1;
          if chUsed(chan+8)
            error(message('xPCTarget:NIPCIE:DiffChanUsed', chan + 8));
          else
            chUsed(chan+8)=1;
          end
        end
      end
    end
  end
