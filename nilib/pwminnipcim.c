/* pwminnipcim.c - xPC Target, non-inlined S-function driver for
 *               Pulse width measurement using the General Purpose Counters
 *               of the National Instruments M series boards.
 */
/* Copyright 1996-2014 The MathWorks, Inc.
 */

#define         S_FUNCTION_LEVEL        2
#undef          S_FUNCTION_NAME
#define         S_FUNCTION_NAME         pwminnipcim

#include        <stddef.h>
#include        <stdlib.h>

#include        "simstruc.h"

#ifdef          MATLAB_MEX_FILE
#include        "mex.h"
#endif

#ifndef         MATLAB_MEX_FILE
#include        <windows.h>
#include        "xpctarget.h"
#include        "xpcionim.h"
#endif

/* Input Arguments */
#define NUMBER_OF_ARGS          (7)
#define CHANNEL_ARG             ssGetSFcnParam(S, 0)
#define TRIGMD_ARG              ssGetSFcnParam(S, 1)
#define FILTER_ARG              ssGetSFcnParam(S, 2)
#define PFIDIR_ARG              ssGetSFcnParam(S, 3)
#define SAMP_TIME_ARG           ssGetSFcnParam(S, 4)
#define SLOT_ARG                ssGetSFcnParam(S, 5)
#define DEV_ARG                 ssGetSFcnParam(S, 6)

#define SAMP_TIME_IND           (0)

#define NO_R_WORKS              (0)
#define NO_I_WORKS              (0)
#define NO_P_WORKS              (1)

/* Index into PWork */
#define BASE_ADDR_P_IND         (0)

static char_T msg[256];

static void mdlInitializeSizes(SimStruct *S)
{
    int j;

    ssSetNumSFcnParams(S, NUMBER_OF_ARGS);
    if (ssGetNumSFcnParams(S) != ssGetSFcnParamsCount(S))
    {
        sprintf(msg,"Wrong number of input arguments passed.\n"
                "%d arguments are expected", NUMBER_OF_ARGS);
        ssSetErrorStatus(S,msg);
        return;
    }

    ssSetNumContStates(S, 0);
    ssSetNumDiscStates(S, 0);

    if( !ssSetNumInputPorts(S, 0)) return;

    if( !ssSetNumOutputPorts(S, 1)) return;
    ssSetOutputPortWidth(S, 0, 1);

    ssSetNumSampleTimes(S, 1);

    ssSetSimStateCompliance( S, HAS_NO_SIM_STATE );

    ssSetNumRWork(S, NO_R_WORKS);
    ssSetNumIWork(S, NO_I_WORKS);
    ssSetNumPWork(S, NO_P_WORKS);

    ssSetNumModes(S, 0);
    ssSetNumNonsampledZCs(S, 0);

    for (j = 0; j < NUMBER_OF_ARGS; j++)
        ssSetSFcnParamNotTunable(S, j);

    ssSetOptions(S, SS_OPTION_DISALLOW_CONSTANT_SAMPLE_TIME | SS_OPTION_EXCEPTION_FREE_CODE );
    return;
}

static void mdlInitializeSampleTimes(SimStruct *S)
{
    ssSetModelReferenceSampleTimeInheritanceRule(S, USE_DEFAULT_FOR_DISCRETE_INHERITANCE);
    if (mxGetPr(SAMP_TIME_ARG)[0] == -1.0)
    {
    	ssSetSampleTime(S, 0, INHERITED_SAMPLE_TIME);
    	ssSetOffsetTime(S, 0, FIXED_IN_MINOR_STEP_OFFSET);
    }
    else
    {
        ssSetSampleTime(S, 0, mxGetPr(SAMP_TIME_ARG)[0]);
    	ssSetOffsetTime(S, 0, 0.0);
    }
}

#define MDL_START
static void mdlStart(SimStruct *S)
{
#ifndef MATLAB_MEX_FILE
    int_T bus, slot;
    xpcPCIDevice pciinfo;
    volatile uint32_T *lreg;
    volatile uint16_T *sreg;
    const char *devName;
    MseriesBoard *bd;
    int  devtype;
    int32_T chan = (int32_T)mxGetPr( CHANNEL_ARG )[0];  // 0 or 1
    uint32_T trigmode = (uint32_T)mxGetPr( TRIGMD_ARG )[0];
    uint32_T filter = (uint32_T)mxGetPr( FILTER_ARG )[0] - 1;
    uint32_T pfidir = (uint32_T)mxGetPr(PFIDIR_ARG)[0];
    int32_T bnum;
    int32_T success;
    int32_T pfichan;
    
    if (mxGetN(SLOT_ARG) == 1)
    {
        bus = 0;
        slot = (int_T)mxGetPr(SLOT_ARG)[0];
    }
    else
    {
        bus = (int_T)mxGetPr(SLOT_ARG)[0];
        slot = (int_T)mxGetPr(SLOT_ARG)[1];
    }

    devtype = (int_T)mxGetPr(DEV_ARG)[0];

    bd = niMIdentify( S, devtype, bus, slot, &pciinfo );
    if( bd == 0 )
        return;  // We didn't find the board as described.
    devName = bd->name;
    
    lreg  = (volatile uint32_T *)pciinfo.VirtAddress[1];
    sreg  = (volatile uint16_T *)pciinfo.VirtAddress[1];

    bnum = niM_Private_Data( (void *)sreg );
    if( bnum < 0 )
    {
        sprintf(msg, "%s: Too many M series boards", devName );
        ssSetErrorStatus(S, msg);
        return;
    }
    ssSetPWorkValue(S, BASE_ADDR_P_IND, (void *)lreg);

    niM_configureTimebase( (void *)sreg );
    niM_Counter_Reset_All( (void *)sreg, chan );

    // Set a bit for OUTPUT on that PFI.
    // The mask init file looked at all blocks that use PFI bits.
    sreg[ S_IO_Bidirection_Pin ] = pfidir;

    niM_PeriodConfigure( (void *)sreg, chan, trigmode, devtype );

    if( devtype == 27 ) // special for 6221/37
    {
        if( chan == 0 )
            pfichan = 1;  // G0 uses PFI1 on 6221/37
        else
            pfichan = 4;  // G1 uses PFI4
    } else
    {   // All the other boards.
        if( chan == 0 )
            pfichan = 9; // G0 gate is PFI9
        else
            pfichan = 4; // G1 gate is PFI4
    }
    success = niM_SetPFIFilter( bnum, pfichan, filter );
    if( success < 0 )
    {
        char *err = " ";
        switch( success )
        {
            case -1:  // bad base pointer
                err = "Bad address lookup";
                break;
            case -2:  // filter value is out of range
                err = "Filter value out of range";
                break;
            case -3:  // PFI channel number out of range
                err = "PFI channel out of range";
                break;
        }
        sprintf( msg, "%s: %s", devName, err );
        ssSetErrorStatus( S, msg );
        return;
    }
    niM_CtrReadArm( (void *)sreg, chan );
#endif
}

static void mdlOutputs(SimStruct *S, int_T tid) {
#ifndef MATLAB_MEX_FILE
    uint32_T chan = (uint32_T)mxGetPr( CHANNEL_ARG )[0];
    volatile uint32_T *lreg = ssGetPWorkValue(S, BASE_ADDR_P_IND );
    uint32_T count;
    
    count = lreg[ L_G0_HW_Save + chan ];
    ssGetOutputPortRealSignal(S, 0)[0] = (double)count;
    return;
#endif
}

static void mdlTerminate(SimStruct *S) {
#ifndef MATLAB_MEX_FILE
    int32_T chan = (int32_T)mxGetPr( CHANNEL_ARG )[0];
    void *base = (void *)ssGetPWork(S)[BASE_ADDR_P_IND];

    niM_CtrDisarm( base, chan );

#endif
    return;
}

#ifdef MATLAB_MEX_FILE  /* Is this file being compiled as a MEX-file? */
#include "simulink.c"   /* Mex glue */
#else
#include "cg_sfun.h"    /* Code generation glue */
#endif
