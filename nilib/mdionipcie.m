function [control, reset, initValue] = mdionipcie(flag, channel, ref, direct, boardType, reset, initValue) %#ok

% MDIONIPCIE - InitFcn and Mask Initialization for NI PCI-E series digital I/O section
% The final two parameters (reset and initValue) are used only when direct = 2 (Output)  

% Copyright 1996-2011 The MathWorks, Inc.

  switch flag
   case 1
    masktype = get_param( gcb, 'MaskType' );
    dimask = ['di', masktype(3:end)];
    domask = ['do', masktype(3:end)];
    
    diblocks = find_system(bdroot, ...
                           'FollowLinks', 'on', ...
                           'LookUnderMasks', 'all', ...
                           'MaskType', dimask );
    doblocks = find_system(bdroot, ...
                           'FollowLinks', 'on', ...
                           'LookUnderMasks', 'all', ...
                           'MaskType', domask );
   
    % I need to accumulate all bits that are output for the current
    % board.  If a di block and a do block try to use the same bit,
    % then we have a fatal collision and throw an error.
    % Two di blocks can access the same board, it's not optimum, but
    % it will work.
    % The same with multiple do blocks.  Since the do driver reads back
    % the current value and modifies it, we can have multiple blocks using
    % the same board.
   
    slot = evalin( 'base', get_param( gcb, 'slot' ));
    control = 0;  % preset to all input
    inputs = zeros(1,8);
    outputs = zeros(1,8);
    
    % accumulate input and output bit assignments for this board
    for i = 1 : length(diblocks)
      tmpslot = evalin( 'base', get_param( diblocks{i}, 'slot' ));
      if isequal( tmpslot, slot )
        channel = eval( get_param( diblocks{i}, 'channel' ));
        for j = 1 : length(channel)
          inputs(channel(j)) = 1;
        end
      end
    end
    for i = 1 : length(doblocks)
      tmpslot = evalin( 'base', get_param( doblocks{i}, 'slot' ));
      if isequal( tmpslot, slot )
        channel = eval( get_param( doblocks{i}, 'channel' ));
        for j = 1 : length(channel)
          outputs(channel(j)) = 1;
        end
      end
    end

    % check for collisions
    if masktype(2) == 'i'
      channel = eval( get_param( gcb, 'channel' ));
      for j = 1 : length(channel)
        if outputs(channel(j)) >= 1
          error(message('xPCTarget:NIPCIEDIO:InputConflict'));
        end
      end
    end
    if masktype(2) == 'o'
      channel = eval( get_param( gcb, 'channel' ));
      for j = 1 : length(channel)
        if inputs(channel(j)) >= 1
          error(message('xPCTarget:NIPCIEDIO:OutputConflict'));
        end
      end
    end
    
    % create the DIO config register value, here a float.  The driver
    % converts the value to the integer register value.
    for i = 1:8
      if outputs(i) >= 1
        control = control + bitshift(1, i-1);
      end
    end
    
    set_param( gcb, 'UserData', control );
   
   case 2
    
    control = get_param( gcb, 'UserData' );
    
    switch boardType
     case 1
      maskdisplay='disp(''PCI-6023E\nNational Instr.\n';
     case 2
      maskdisplay='disp(''PCI-6024E\nNational Instr.\n';
     case 3
      maskdisplay='disp(''PCI-6025E\nNational Instr.\n';
     case 4
      maskdisplay='disp(''PCI-6070E\nNational Instr.\n';
     case 5
      maskdisplay='disp(''PCI-6040E\nNational Instr.\n';
     case 6
      maskdisplay='disp(''PXI-6070E\nNational Instr.\n';
     case 7
      maskdisplay='disp(''PXI-6040E\nNational Instr.\n';
     case 8
      maskdisplay='disp(''PCI-6071E\nNational Instr.\n';
     case 9
      maskdisplay='disp(''PCI-6052E\nNational Instr.\n';
     case 10
      maskdisplay='disp(''PCI-6030E\nNational Instr.\n';
     case 11
      maskdisplay='disp(''PCI-6031E\nNational Instr.\n';
     case 12
      maskdisplay='disp(''PXI-6071E\nNational Instr.\n';
     case 13
      maskdisplay='disp(''PCI-6713E\nNational Instr.\n';
     case 14
      maskdisplay='disp(''PXI-6713E\nNational Instr.\n';
     case 15
      maskdisplay='disp(''PCI-6711E\nNational Instr.\n';
     case 16
      maskdisplay='disp(''PXI-6052E\nNational Instr.\n';
     case 17
      maskdisplay='disp(''PXI-6711E\nNational Instr.\n';
     case 18
      maskdisplay='disp(''PCI-6731E\nNational Instr.\n';
     case 19
      maskdisplay='disp(''PXI-6731E\nNational Instr.\n';
     case 20
      maskdisplay='disp(''PCI-6733E\nNational Instr.\n';
     case 21
      maskdisplay='disp(''PXI-6733E\nNational Instr.\n';
     case 22
      maskdisplay='disp(''PCI-6011E\nNational Instr.\n';
    end

    % could use the second character of the masktype, but we're already
    % using the direct parameter so I won't change it.
    if direct==1
      maskdisplay=[maskdisplay,'Digital Input'');'];
      for i=1:length(channel)
        maskdisplay=[maskdisplay,'port_label(''output'',',num2str(i),',''',num2str(channel(i)),''');']; %#ok
      end
    elseif direct==2
      maskdisplay=[maskdisplay,'Digital Output'');'];
      for i=1:length(channel)
        maskdisplay=[maskdisplay,'port_label(''input'',',num2str(i),',''',num2str(channel(i)),''');']; %#ok
      end

      %%% check reset vector parameter
      if ~isa(reset, 'double')
        error(message('xPCTarget:NIPCIEDIO:ResetType'));
      end
      if all( size(reset) == 1 )
        reset = reset * ones(size(channel));
      elseif ~all(size(reset) == size(channel))
        error(message('xPCTarget:NIPCIEDIO:ResetLength'));
      end
      reset = round(reset);
      if ~all(ismember(reset, [0 1]))
        error(message('xPCTarget:NIPCIEDIO:ResetValue'));
      end

      %%% check initValue vector parameter
      if ~isa(initValue, 'double')
        error(message('xPCTarget:NIPCIEDIO:InitType'));
      end
      if all( size(initValue) == 1 )
        initValue = initValue * ones(size(channel));
      elseif ~all(size(initValue) == size(channel))
        error(message('xPCTarget:NIPCIEDIO:InitLength'));
      end
      initValue = round(initValue);
      if ~all(ismember(initValue, [0 1]))
        error(message('xPCTarget:NIPCIEDIO:InitValue'));
      end
    end
    set_param(gcb,'MaskDisplay',maskdisplay);

  end
