function [maskdisplay, config, reset, initial] = mdionipci660x( phase, ...
                                                  channel, ...
                                                  slot, boardType, ...
                                                  reset, initial ) %#ok

% Copyright 2004-2011 The MathWorks, Inc.

if phase == 1
  switch boardType
   case 1
    di = find_system(bdroot, 'FollowLinks', 'on', 'LookUnderMasks', 'all', ...
                     'MaskType', 'dinipci6602');
    do = find_system(bdroot, 'FollowLinks', 'on', 'LookUnderMasks', 'all', ...
                     'MaskType', 'donipci6602');
   case 2
    di = find_system(bdroot, 'FollowLinks', 'on', 'LookUnderMasks', 'all', ...
                     'MaskType', 'dinipxi6602');
    do = find_system(bdroot, 'FollowLinks', 'on', 'LookUnderMasks', 'all', ...
                     'MaskType', 'donipxi6602');
   case 3
    di = find_system(bdroot, 'FollowLinks', 'on', 'LookUnderMasks', 'all', ...
                     'MaskType', 'dinipci6601');
    do = find_system(bdroot, 'FollowLinks', 'on', 'LookUnderMasks', 'all', ...
                     'MaskType', 'donipci6601');
  end

  % rules:  Only one di block for each board address
  %         Only one do block for each board address
  %         di and do for the same board address can't share channels
  
  slot = eval(get_param(gcb, 'slot'));
  masktype = get_param( gcb, 'MaskType' );

  dicount = 0;
  docount = 0;
  disave = {};
  dosave = {};

  for i = 1 : length(di)
    tmp = eval(get_param(di{i}, 'slot'));
    if isequal(tmp, slot)
      dicount = dicount + 1;
      disave{dicount} = di{i}; %#ok, growing
    end
  end

  for i = 1 : length(do)
    tmp = eval(get_param(do{i}, 'slot'));
    if isequal(tmp, slot)
      docount = docount + 1;
      dosave{docount} = do{i}; %#ok, growing
    end
  end

  if strcmp( masktype(1:2), 'di' ) && dicount > 1
    error(message('xPCTarget:NI660x:DupDI'));
  end
  if strcmp( masktype(1:2), 'do' ) && docount > 1
    error(message('xPCTarget:NI660x:DupDO'));
  end

  if prod(length(slot)) > 2
    error(message('xPCTarget:NI660x:Slot'));
  end
  
  temp = { disave, dosave };
  set_param( gcb, 'UserData', temp );
else  % phase == 2
  % disp('mask init time');
  masktype = get_param( gcb, 'MaskType' );
  switch boardType
   case 1
    btype = 'PCI-';
   case 2
    btype = 'PXI-';
   case 3
    btype = 'PCI-';
  end
  
  maskdisplay = ['disp(''',btype, masktype(8:11),'\nNational Instr.\nDigital '];
  if strcmp( masktype(1:2), 'di' )
    maskdisplay = [maskdisplay, 'Input'');'];
  else
    maskdisplay = [maskdisplay, 'Output'');'];
  end

  % maskdisplay
  
  temp = get_param( gcb, 'UserData' );
  if isempty(temp)
      if strcmp( masktype(1:2), 'di' )
          disave = {gcb};
      else
          disave = {};
      end
      if strcmp( masktype(1:2), 'do' )
          dosave = {gcb};
      else
          dosave = {};
      end
  else
      disave = temp{1};
      dosave = temp{2};
  end
  config = [0 0];
  % make sure that di and do don't duplicate any channels
  inchans = [];
  outchans = [];
  % both disave and dosave must be length 0 or 1 or the init phase would have
  % gotten an error and we wouldn't be here.
  if length( disave ) > 0 %#ok
    inchans = eval(get_param( disave{1}, 'channel' ));
  end
  if length( dosave ) > 0 %#ok
    outchans = eval(get_param( dosave{1}, 'channel' ));
  end
  if length( inchans ) ~= length(unique(inchans))
    error(message('xPCTarget:NI660x:DupInput'));
  end
  if length( outchans ) ~= length(unique(outchans))
    error(message('xPCTarget:NI660x:DupOutput'));
  end
  
  if length( [inchans outchans] ) ~= length(unique([inchans outchans]))
    error(message('xPCTarget:NI660x:IOconflict'));
  end
  
  io = [0, 0];
  for i = 1:length(inchans)
    io(floor(inchans(i)/5)+1) = 1;
  end
  for i = 1:length(outchans)
    if io(floor(outchans(i)/5)+1) == 1
      error(message('xPCTarget:NI660x:IOGroups'));
    end
  end
  
  if strcmp( masktype(1:2), 'di' )
    for i = 1:length(inchans)
      outport = ['port_label(''output'',',num2str(i),',''',num2str(inchans(i)),''');'];
      maskdisplay = [maskdisplay,outport]; %#ok, growing
    end
  else
    for i = 1:length(outchans)
      inport = ['port_label(''input'',',num2str(i),',''',num2str(outchans(i)),''');'];
      maskdisplay = [maskdisplay,inport]; %#ok, growing
    end
  end
  
  if length(outchans) > 0 %#ok
    for i = 1:length(outchans)
      chan = outchans(i);
      if chan <= 4
        config(1) = config(1) + bitshift(1, 8*(chan - 1));
      else
        config(2) = config(2) + bitshift(1, 8*(chan - 5));
      end
    end
  end

  %%% check reset vector parameter
  if ~isa(reset, 'double')
    error(message('xPCTarget:NI660x:ResetType'));
  end
  if all( size(reset) == 1 )
    reset = reset * ones(size(channel));
  elseif ~all(size(reset) == size(channel))
    error(message('xPCTarget:NI660x:ResetLength'));
  end
  reset = round(reset);
  if ~all(ismember(reset, [0 1]))
    error(message('xPCTarget:NI660x:ResetValue'));
  end

  %%% check initial vector parameter
  if ~isa(initial, 'double')
    error(message('xPCTarget:NI660x:InitType'));
  end
  if all( size(initial) == 1 )
    initial = initial * ones(size(channel));
  elseif ~all(size(initial) == size(channel))
    error(message('xPCTarget:NI660x:InitLength'));
  end
  initial = round(initial);
  if ~all(ismember(initial, [0 1]))
    error(message('xPCTarget:NI660x:InitValue'));
  end
end
