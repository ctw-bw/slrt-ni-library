/* evntctrnipci660x.c - xPC Target, non-inlined S-function driver for PCI/PXI 6601
 *                       counter/timer boards. This driver is used for event
 *                       counting.
 */

#define         S_FUNCTION_LEVEL        2
#undef          S_FUNCTION_NAME
#define         S_FUNCTION_NAME         evntctrnipci660x

#include        <stddef.h>
#include        <stdlib.h>
#include        "simstruc.h"

#ifdef          MATLAB_MEX_FILE
#include        "mex.h"
#endif

#ifndef         MATLAB_MEX_FILE
#include        <windows.h>
#include        "xpcimports.h"
#include        "xpcnitio.h"
#include        "evntcntrpci660xhelper.h"
#endif

//#include "evntcntrpci660x.h"
/* Input Arguments */
#define NUMBER_OF_ARGS       (13)
#define DEV_ID_ARG                  ssGetSFcnParam(S, 0)
#define CHANNEL_ARG                 ssGetSFcnParam(S, 1)
#define MODE_ARG                    ssGetSFcnParam(S, 2)
#define ARM_ARG                     ssGetSFcnParam(S, 3)
#define SOURCE_ARG                  ssGetSFcnParam(S, 4)
#define SOURCE_POLARITY_ARG         ssGetSFcnParam(S, 5)
#define FILTER_ARG                  ssGetSFcnParam(S, 6)
#define HWGATE_PIN_ARG              ssGetSFcnParam(S, 7)
#define GATE_MODE_ARG               ssGetSFcnParam(S, 8)
#define RESET_MODE_ARG              ssGetSFcnParam(S, 9)
#define INIT_LOAD_VALUE_ARG         ssGetSFcnParam(S, 10)
#define SAMP_TIME_ARG               ssGetSFcnParam(S, 11)
#define PCI_DEV_ARG                 ssGetSFcnParam(S, 12)

#define SAMP_TIME_IND        (0)

#define NO_R_WORKS           (0)
#define NO_I_WORKS           (16)
#define NO_P_WORKS           (2)


static char msg[256];

static void mdlInitializeSizes(SimStruct *S) {
    
    /* Check if the SW gate or SW Reset is selected and set the number of
     * inputs to the S-function block accordingly
     */
    ushort_T numInputs = 0;
    
    ssSetNumSFcnParams(S, NUMBER_OF_ARGS);
    
    if (ssGetNumSFcnParams(S) != ssGetSFcnParamsCount(S))
    {
        static char_T msg[256];
        sprintf(msg, "Wrong number of input arguments passed.\n"
                "%d arguments are expected\n", NUMBER_OF_ARGS);
        ssSetErrorStatus(S, msg);
        return;
    }
    
    // ARM_ARG is a checkbox, 1 or 0
    numInputs = (mxGetPr(ARM_ARG)[0] == 1)? 1:0;
    // ResetMode is 1: no reset, 2: reset on read, 3: reset on input high
    numInputs =  numInputs + ((mxGetPr(RESET_MODE_ARG)[0] == 3)? 1:0);
    
    if( !ssSetNumInputPorts(S, numInputs) ) return;
    {
        int j;
        for ( j=0; j < numInputs; j++)
        {
            ssSetInputPortWidth(S, j, 1);
            ssSetInputPortDirectFeedThrough( S, j, 1);
            ssSetInputPortRequiredContiguous(S, j, 1);
        }
    }
    
    if( !ssSetNumOutputPorts(S, 1) )return;
    ssSetOutputPortWidth(S, 0, 1);
    
    ssSetNumSampleTimes(S, 1);
    
    ssSetNumRWork(S, NO_R_WORKS);
    ssSetNumIWork(S, NO_I_WORKS);
    ssSetNumPWork(S, NO_P_WORKS);
    
    ssSetNumModes(S, 0);
    ssSetNumNonsampledZCs(S, 0);
    ssSetNumContStates(S, 0);
    ssSetNumDiscStates(S, 0); 

    {
        int j;
        for (j = 0; j < NUMBER_OF_ARGS; j++)
            ssSetSFcnParamNotTunable(S, j);
    }
    
    ssSetSimStateCompliance(S, HAS_NO_SIM_STATE );
    
    ssSetOptions(S, SS_OPTION_EXCEPTION_FREE_CODE | SS_OPTION_DISALLOW_CONSTANT_SAMPLE_TIME);
    
    return;
}

static void mdlInitializeSampleTimes(SimStruct *S) {
    
    ssSetSampleTime(S, 0, mxGetPr(SAMP_TIME_ARG)[SAMP_TIME_IND]);
    ssSetOffsetTime(S, 0, 0.0);
    
}

#define MDL_START
static void mdlStart(SimStruct *S)
{
    
#ifndef MATLAB_MEX_FILE
    
    uint32_T *iWork = (uint32_T *)ssGetIWork(S);
    const char *devName;
    uint16_T  vendorId = 0x1093;
    uint16_T  deviceId;
    uint16_T  subvendorId = 0xffff;
    uint16_T  subdeviceId = 0xffff;
    xpcPCIDevice pciinfo;
    int32_T  bus, slot;
    void *Physical1, *Physical0;
    volatile unsigned int *MITE;
    volatile uint16_T *board, *TIO;
    unsigned int chipOffset = 0;
    int inittype = 0;
    int absChan, chan;
    uint32_T ctrMask;

    switch ((int)mxGetPr(DEV_ID_ARG)[0]) {
      case 1:
        devName = "NI PCI 6602";
        deviceId = 0x1310;
        inittype = 2;
        break;
      case 2:
        devName = "NI PXI 6602";
        deviceId = 0x1360;
        inittype = 2;
        break;
      case 3:
        devName = "NI PCI 6601";
        deviceId = 0x2c60;
        inittype = 1;
        break;
      default:
        devName = "Unknown device";
        deviceId = 0xFFFF;
        break;
    }

    if (mxGetN(PCI_DEV_ARG) == 1)
    {
        bus = 0;
        slot = (int_T)mxGetPr(PCI_DEV_ARG)[0];
    }
    else
    {
        bus = (int_T)mxGetPr(PCI_DEV_ARG)[0];
        slot = (int_T)mxGetPr(PCI_DEV_ARG)[1];
    }
    // look for the PCI-Device
    if( xpcGetPCIDeviceInfo( vendorId, deviceId, subvendorId, subdeviceId, bus, slot, &pciinfo ) )
    {
        sprintf(msg,"%s (bus %d, slot %d): board not present",devName, bus, slot );
        ssSetErrorStatus(S,msg);
        return;
    }

    pciinfo.VirtAddress[0] =
        (unsigned long)xpcReserveMemoryRegion( (void *)pciinfo.BaseAddress[0],
                                               4096,
                                               XPC_RT_PG_USERREADWRITE );
    pciinfo.VirtAddress[1] =
        (unsigned long)xpcReserveMemoryRegion( (void *)pciinfo.BaseAddress[1],
                                               4096,
                                               XPC_RT_PG_USERREADWRITE );

    Physical1 = (void *)pciinfo.BaseAddress[1];
    board     = (volatile uint16_T *)pciinfo.VirtAddress[1];

    Physical0 = (void *)pciinfo.BaseAddress[0];
    MITE      = (volatile unsigned int *)pciinfo.VirtAddress[0];
    if( inittype == 2 )  // 6602 MITE init
    {
        MITE[49]  = ((unsigned int)Physical1 & ~0xff) | 0x8C;
        MITE[61]  = 0;
    }
    else  // 6601 MITE init
    {
        MITE[48]  = ((unsigned int)Physical1 & ~0xff) | 0x80;
    }

    /* Store all the parameters in I work vector */
    absChan = (int)mxGetPr(CHANNEL_ARG)[0]; /* 0-based */
    iWork[ABS_CHAN_I_IND] = absChan;
    chan    = absChan % 4;
    if (absChan / 4) {                  /* counters 4 - 7, on chip 1 */
        chipOffset = 0x800;
        ctrMask    = 0x00200000;
    } else {
        chipOffset = 0;
        ctrMask    = 0;
    }
    iWork[CHANNEL_I_IND]         = chan;
    iWork[MODE_I_IND]            = (uint32_T)mxGetPr(MODE_ARG)[0];
    iWork[ARM_I_IND]             = (uint32_T)mxGetPr(ARM_ARG)[0];

    iWork[SOURCE_I_IND]          = (uint32_T)mxGetPr(SOURCE_ARG)[0];
    iWork[SOURCE_POLARITY_I_IND] = (uint32_T)mxGetPr(SOURCE_POLARITY_ARG)[0];
    iWork[FILTER_I_IND]          = (uint32_T)mxGetPr(FILTER_ARG)[0];

    // Subtract 1 from the gate pin value, 1 based menu -> 0 based register
    iWork[HWGATE_PIN_I_IND]      = (uint32_T)mxGetPr(HWGATE_PIN_ARG)[0] - 1;
    iWork[GATE_MODE_I_IND]       = (uint32_T)mxGetPr(GATE_MODE_ARG)[0];

    iWork[RESET_MODE_I_IND]      = (uint32_T)mxGetPr(RESET_MODE_ARG)[0];
    iWork[INIT_LOAD_VALUE_I_IND] = (uint32_T)mxGetPr(INIT_LOAD_VALUE_ARG)[0];

    iWork[LATCH_REGISTER_I_IND]  = (unsigned int) 0; // ??
    iWork[CTR_MASK_I_IND]        = ctrMask;
    /*
     * TIO is the base address of the _TIO Chip_, not the board. This is
     * the value that is saved in the PWork below. We will only address the
     * board in MdlStart, so we do not save the board address.
     */

    TIO = (unsigned short *)((char *)board + chipOffset);
    ssGetPWork(S)[BASE_ADDR_P_IND] = (int *)TIO;

    /* Reset all the registers on the channel */
    reset660xEvt(S);

    /* Set up the counter for simple event counting operation */
    setUp660xEvt(S, ctrMask);

#endif
}

static void mdlOutputs(SimStruct *S, int_T tid)
{
#ifndef MATLAB_MEX_FILE

    uint32_T  *iWork = (uint32_T *)ssGetIWork(S);

    static ushort_T SWSave[] = {0x018, 0x01C, 0x118, 0x11C};
    static ushort_T HWSave[] = {0x010, 0x014, 0x110, 0x114};

    volatile void *TIO  = ssGetPWork(S)[BASE_ADDR_P_IND];
    ushort_T temp;
    unsigned int value1, value2,outputValue,swRegValue;
    int resetport = 0;
    int armport = 0;
    int armval = 1;  // true if the port isn't in use
    
    //uint32_T initVal    = iWork[INIT_LOAD_VALUE_I_IND];    
    ushort_T channel    = (ushort_T) iWork[CHANNEL_I_IND];
    ushort_T arm        = (uint32_T)mxGetPr(ARM_ARG)[0];
    ushort_T mode       = (ushort_T) iWork[MODE_I_IND];
    //ushort_T gateMode   = (ushort_T) iWork[GATE_MODE_I_IND];
    //ushort_T hwGatePin  = (ushort_T) iWork[HWGATE_PIN_I_IND];
    ushort_T reset      = (ushort_T) iWork[RESET_MODE_I_IND];

    outputValue         = (unsigned int) iWork[OUTPUT_VALUE_I_IND];
    swRegValue          = (unsigned int) iWork[SW_REG_VALUE_I_IND];

    // Figure out which input port is for software reset and arm
    // The only time resetport isn't 0 is if both are in use.
    if( (arm == 1) && (reset == SW_RESET) )
    {
        armport = 0;
        resetport = 1;
    }

    if( arm == 1 )
        armval = ( (int)ssGetInputPortRealSignal(S, armport)[0] != 0 );
    
    /* If gating counting mode, then read from HW Save
     * register instead of the SW Save register.
     */
    if( (mode == 3) ) //|| (mode == 2) )
    {
        //temp = HWSave[channel];
        value1 = nitio_TIO_Read32(TIO, HWSave[channel]);
    }
    else
    {
        temp = SWSave[channel];
        /* Read the SW save register twice to assure correctness.*/
        value1 = nitio_TIO_Read32(TIO, temp);
        value2 = nitio_TIO_Read32(TIO, temp);
        /* If the values are not equal, read again */
        if(value1 != value2)
        {
            value1 = nitio_TIO_Read32(TIO, temp);
        }
    }

    ssGetOutputPortRealSignal(S, 0)[0] = (real_T)value1;

    if( (mode == 1) || (mode == 2) )  // No reset if HW latched mode
    {
        if( reset == RESET_AFTER_READ )
        {
            // Reset the COUNT
            nitio_TIO_Write16(TIO, channel, G0_Command_Reg, 0x0134 ); // load and disarm
            //nitio_TIO_Write16(TIO, channel, G0_Command_Reg, 0x0121 ); // arm
        }
        if(reset == SW_RESET)
        {
            if( ssGetInputPortRealSignal(S, resetport)[0] >= 0.5 )   // Is reset input true?
            {
                // Reset the COUNT
                nitio_TIO_Write16(TIO, channel, G0_Command_Reg, 0x0134 ); // load and disarm
                //nitio_TIO_Write16(TIO, channel, G0_Command_Reg, 0x0121 ); // arm
            }
        }
    }
    if( armval == 1 )
        nitio_TIO_Write16(TIO, channel, G0_Command_Reg, 0x0121 ); // arm
    else
        nitio_TIO_Write16(TIO, channel, G0_Command_Reg, 0x0130 ); // disarm

#endif // not defined MATLAB_MEX_FILE
}

    
static void mdlTerminate(SimStruct *S) {
#ifndef MATLAB_MEX_FILE
    void *TIO  = ssGetPWork(S)[BASE_ADDR_P_IND];
    int   channel = ssGetIWork(S)[CHANNEL_I_IND];

    nitio_resetCtr(TIO, channel);
#endif
}

#ifdef MATLAB_MEX_FILE  /* Is this file being compiled as a MEX-file? */
#include "simulink.c"   /* Mex glue */
#else
#include "cg_sfun.h"    /* Code generation glue */
#endif
