function maskDisplay = mnipcievt660x(phase,boardtype,ctr,slot,ResetMode,arm) 

% Copyright 2006-2011 The MathWorks, Inc.

%   if phase == 1  % InitFcn call, only phase is defined

% Cross block checking is all done in mnipci660x.m for all 4 counter types.
% This avoids having the same code in multiple places.

%   end
  
  if phase == 2
      if prod(length(slot)) > 2  || (prod(length(slot)) == 1 && (slot ~= -1))
          error(message('xPCTarget:PCI660xEvt:Slot'));
      end
      sprintf('phase2: ctr = %d\n', ctr );
      switch boardtype
          case 1
              maskDisplay = sprintf('disp(''PCI-6602\\nNational Instr\\nEvent Counter\\nCounter %d'');port_label(''output'',1,''C%d'');',ctr,ctr);
          case 2
              maskDisplay = sprintf('disp(''PXI-6602\\nNational Instr\\nEvent Counter\\nCounter %d'');port_label(''output'',1,''C%d'');',ctr,ctr);
          case 3
              maskDisplay = sprintf('disp(''PCI-6601\\nNational Instr\\nEvent Counter\\nCounter %d'');port_label(''output'',1,''C%d'');',ctr,ctr);
      end
      
      if (ResetMode == 3) && (arm == 0)
          str = sprintf('port_label(''input'',1,''R%d'');', ctr);
      elseif (ResetMode == 3) && (arm == 1)
          str = sprintf('port_label(''input'',1,''A%d'');', ctr);
          str = sprintf('%s port_label(''input'',2,''R%d'');', str, ctr);
      elseif (ResetMode ~= 3)  && (arm == 1)
          str = sprintf('port_label(''input'',1,''A%d'');', ctr);
      else
          str = '';
      end
      maskDisplay = sprintf('%s %s',maskDisplay,str);
  end
  
  if phase == 3  % set enables when changing mode
    mode = get_param( gcb, 'mode' );

    switch mode
    case 'Cumulative counter'
        enables = 'on,on,on,on,on,on,off,off,on,on,on,on';
    case 'HW gated cumulative counter' % HW gated cumulative
        enables = 'on,on,on,on,on,on,on,on,on,on,on,on';
    case 'HW gated latched counter' % HW gated latched
        enables = 'on,on,on,on,on,on,on,on,off,on,on,on';
    end
    set_param( gcb, 'MaskEnableString', enables );
  end
end
