/* danipcie.c - xPC Target, non-inlined S-function driver for D/A section of NI PCI-E series boards  */
/* Copyright 1996-2009 The MathWorks, Inc.
*/

#define         S_FUNCTION_LEVEL        2
#undef          S_FUNCTION_NAME
#define         S_FUNCTION_NAME         danipcie

#include        <stddef.h>
#include        <stdlib.h>

#include        "simstruc.h"

#ifdef          MATLAB_MEX_FILE
#include        "mex.h"
#endif

#ifndef         MATLAB_MEX_FILE
#include        <windows.h>
#include        "xpcimports.h"
#include        "xpcionie.h"
#endif

/* Input Arguments */
#define NUMBER_OF_ARGS          (7)
#define CHANNEL_ARG             ssGetSFcnParam(S,0)
#define RANGE_ARG               ssGetSFcnParam(S,1)
#define RESET_ARG               ssGetSFcnParam(S,2)
#define INIT_VAL_ARG            ssGetSFcnParam(S,3)
#define SAMP_TIME_ARG           ssGetSFcnParam(S,4)
#define SLOT_ARG                ssGetSFcnParam(S,5)
#define DEV_ARG                 ssGetSFcnParam(S,6)

#define SAMP_TIME_IND           (0)
#define BASE_ADDR_IND           (0)

#define NO_I_WORKS              (2)
#define CHANNELS_I_IND          (0)
#define BASE_ADDR_I_IND         (1)

#define NO_R_WORKS              (6)
#define GAIN_R_IND              (0)
#define MINVAL_R_IND            (2)
#define MAXVAL_R_IND            (4)


static char_T msg[256];

static void mdlInitializeSizes(SimStruct *S)
{
    size_t i;

    ssSetNumSFcnParams(S, NUMBER_OF_ARGS);
    if (ssGetNumSFcnParams(S) != ssGetSFcnParamsCount(S)) {
        sprintf(msg,"Wrong number of input arguments passed.\n%d arguments are expected\n",NUMBER_OF_ARGS);
        ssSetErrorStatus(S,msg);
        return;
    }

    ssSetNumContStates(S, 0);
    ssSetNumDiscStates(S, 0);

    if( !ssSetNumInputPorts(S, (int_T)mxGetN(CHANNEL_ARG)) )return;
    for (i=0;i<mxGetN(CHANNEL_ARG);i++) {
        ssSetInputPortWidth(S, i, 1);
        ssSetInputPortDirectFeedThrough(S, i, 1);
    }

    if( !ssSetNumOutputPorts(S, 0) )return;

    ssSetNumSampleTimes(S, 1);

    ssSetSimStateCompliance( S, HAS_NO_SIM_STATE );

    ssSetNumRWork(S, NO_R_WORKS);
    ssSetNumIWork(S, NO_I_WORKS);
    ssSetNumPWork(S, 0);

    ssSetNumModes(S, 0);
    ssSetNumNonsampledZCs(S, 0);

    for (i = 0; i < NUMBER_OF_ARGS; i++)
        ssSetSFcnParamTunable(S, i, 0);

    ssSetOptions(S, SS_OPTION_DISALLOW_CONSTANT_SAMPLE_TIME | SS_OPTION_EXCEPTION_FREE_CODE );
}

static void mdlInitializeSampleTimes(SimStruct *S)
{
    ssSetSampleTime(S, 0, mxGetPr(SAMP_TIME_ARG)[SAMP_TIME_IND]);
    ssSetOffsetTime(S, 0, 0.0);
}

#define MDL_START
static void mdlStart(SimStruct *S)
{

#ifndef MATLAB_MEX_FILE
    int_T nChannels, range;
    int_T i, channel;
    //int_T Control_Register;
    //short outval;

    PCIDeviceInfo pciinfo;
    void *Physical1, *Physical0;
    void *Virtual1, *Virtual0;
    volatile unsigned int *ioaddress0;
    volatile unsigned short *ioaddress;
    volatile unsigned char *ioaddress8;
    uint8_T *devName;
    int  devId;
    double res_float;

    nChannels = mxGetN(CHANNEL_ARG);
    ssSetIWorkValue(S, CHANNELS_I_IND, nChannels);

    switch ((int_T)mxGetPr(DEV_ARG)[0]) {
      case 1:
        devName = "NI PCI-6023E";
        devId=0x2a60;
        res_float=4096.0;
        break;
      case 2:
        devName = "NI PCI-6024E";
        devId=0x2a70;
        res_float=4096.0;
        break;
      case 3:
        devName = "NI PCI-6025E";
        devId=0x2a80;
        res_float=4096.0;
        break;
      case 4:
        devName = "NI PCI-MIO-16E-1";
        devId=0x1180;
        res_float=4096.0;
        break;
      case 5:
        devName = "NI PCI-MIO-16E-4";
        devId=0x1190;
        res_float=4096.0;
        break;
      case 6:
        devName = "NI PXI-6070E";
        devId=0x11b0;
        res_float=4096.0;
        break;
      case 7:
        devName = "NI PXI-6040E";
        devId=0x11c0;
        res_float=4096.0;
        break;
      case 8:
        devName = "NI PCI-6071E";
        devId=0x1350;
        res_float=4096.0;
        break;
      case 9:
        devName = "NI PCI-6052E";
        devId=0x18b0;
        res_float=65536.0;
        break;
      case 10:
        devName = "NI PCI-MIO-16XE-10";
        devId=0x1170;
        res_float=65536.0;
        break;
      case 11:
        devName = "NI PCI-6031E";
        devId=0x1330;
        res_float=65536.0;
        break;
      case 12:
        devName = "NI PXI-6071E";
        devId=0x15B0;
        res_float=4096.0;
        break;
      case 13:
      case 14:
      case 15:
        break;
      case 16:
        devName = "NI PXI-6052E";
        devId=0x18c0;
        res_float=65536.0;
        break;
      case 17: // PXI-6711
      case 18: // PCI-6731
      case 19: // PXI-6731
      case 20: // PCI-6733
      case 21: // PXI-6733
        break;  // These boards use a different driver, just holding the numbers
      case 22:
        devName = "NI PCI-6011E (PCI-MIO-16XE-50)";
        devId=0x0162;
        res_float=4096.0;  // 12 bit D/A
        break;
    }

    if ((int_T)mxGetPr(SLOT_ARG)[0]<0) {
        // look for the PCI-Device
        if (rl32eGetPCIInfo((unsigned short)0x1093,(unsigned short)devId,&pciinfo)) {
            sprintf(msg,"%s: board not present", devName);
            ssSetErrorStatus(S,msg);
            return;
        }
    } else {
        int_T bus, slot;
        if (mxGetN(SLOT_ARG) == 1) {
            bus = 0;
            slot = (int_T)mxGetPr(SLOT_ARG)[0];
        } else {
            bus = (int_T)mxGetPr(SLOT_ARG)[0];
            slot = (int_T)mxGetPr(SLOT_ARG)[1];
        }
        // look for the PCI-Device
        if (rl32eGetPCIInfoAtSlot((unsigned short)0x1093,(unsigned short)devId, (slot & 0xff) | ((bus & 0xff)<< 8),&pciinfo)) {
            sprintf(msg,"%s (bus %d, slot %d): board not present", devName, bus, slot );
            ssSetErrorStatus(S,msg);
            return;
        }
    }

    // show Device Information
    //rl32eShowPCIInfo(pciinfo);

    Physical1=(void *)pciinfo.BaseAddress[1];
    Virtual1 = rl32eGetDevicePtr(Physical1, 4096, RT_PG_USERREADWRITE);
    ioaddress=(void *)Virtual1;
    ioaddress8=(void *)Virtual1;

    Physical0=(void *)pciinfo.BaseAddress[0];
    Virtual0 = rl32eGetDevicePtr(Physical0, 4096, RT_PG_USERREADWRITE);
    ioaddress0=(void *) Virtual0;

    ioaddress0[48]=((unsigned int)Physical1 & 0xffffff00) | 0x00000080;
    ssSetIWorkValue(S, BASE_ADDR_I_IND, (uint_T)ioaddress);

    // calibration
    switch ((int_T)mxGetPr(DEV_ARG)[0]) {
      case 1:
        break;
      case 2:
        // calibrate both DAC's
        nie_writeCALDAC(ioaddress8, 5,nie_readEEPROM(ioaddress8, 426));
        nie_writeCALDAC(ioaddress8, 7,nie_readEEPROM(ioaddress8, 425));
        nie_writeCALDAC(ioaddress8, 6,nie_readEEPROM(ioaddress8, 424));
        nie_writeCALDAC(ioaddress8, 8,nie_readEEPROM(ioaddress8, 423));
        nie_writeCALDAC(ioaddress8,10,nie_readEEPROM(ioaddress8, 422));
        nie_writeCALDAC(ioaddress8, 9,nie_readEEPROM(ioaddress8, 421));
        break;
      case 3:
        // calibrate both DAC's
        nie_writeCALDAC(ioaddress8, 5,nie_readEEPROM(ioaddress8, 426));
        nie_writeCALDAC(ioaddress8, 7,nie_readEEPROM(ioaddress8, 425));
        nie_writeCALDAC(ioaddress8, 6,nie_readEEPROM(ioaddress8, 424));
        nie_writeCALDAC(ioaddress8, 8,nie_readEEPROM(ioaddress8, 423));
        nie_writeCALDAC(ioaddress8,10,nie_readEEPROM(ioaddress8, 422));
        nie_writeCALDAC(ioaddress8, 9,nie_readEEPROM(ioaddress8, 421));
        break;
      case 4:
        /*for (i=0;i<nChannels;i++) {
            channel=mxGetPr(CHANNEL_ARG)[i];
            range=(int_T)(mxGetPr(RANGE_ARG)[i]);
            if (range < 0) { */

                nie_writeCALDAC(ioaddress8,5,nie_readEEPROM(ioaddress8, 420));
                nie_writeCALDAC(ioaddress8,7,nie_readEEPROM(ioaddress8, 419));
                nie_writeCALDAC(ioaddress8,13,nie_readEEPROM(ioaddress8, 419));
                nie_writeCALDAC(ioaddress8,6,nie_readEEPROM(ioaddress8, 418));
                nie_writeCALDAC(ioaddress8,8,nie_readEEPROM(ioaddress8, 417));
                nie_writeCALDAC(ioaddress8,10,nie_readEEPROM(ioaddress8, 416));
                nie_writeCALDAC(ioaddress8,9,nie_readEEPROM(ioaddress8, 415));
        /*    } else {
                nie_writeCALDAC(ioaddress8,5,nie_readEEPROM(ioaddress8, 414));
                nie_writeCALDAC(ioaddress8,7,nie_readEEPROM(ioaddress8, 413));
                nie_writeCALDAC(ioaddress8,6,nie_readEEPROM(ioaddress8, 412));
                nie_writeCALDAC(ioaddress8,8,nie_readEEPROM(ioaddress8, 411));
                nie_writeCALDAC(ioaddress8,10,nie_readEEPROM(ioaddress8, 410));
                nie_writeCALDAC(ioaddress8,9,nie_readEEPROM(ioaddress8, 409));
           }
        } */
        break;
      case 5:
        /*for (i=0;i<nChannels;i++) {
            channel=mxGetPr(CHANNEL_ARG)[i];
            range=(int_T)(mxGetPr(RANGE_ARG)[i]);
            if (range < 0) { */
                nie_writeCALDAC(ioaddress8,5,nie_readEEPROM(ioaddress8, 420));
                nie_writeCALDAC(ioaddress8,7,nie_readEEPROM(ioaddress8, 419));
                nie_writeCALDAC(ioaddress8,13,nie_readEEPROM(ioaddress8, 419));
                nie_writeCALDAC(ioaddress8,6,nie_readEEPROM(ioaddress8, 418));
                nie_writeCALDAC(ioaddress8,8,nie_readEEPROM(ioaddress8, 417));
                nie_writeCALDAC(ioaddress8,10,nie_readEEPROM(ioaddress8, 416));
                nie_writeCALDAC(ioaddress8,9,nie_readEEPROM(ioaddress8, 415));
        /*    } else {
                nie_writeCALDAC(ioaddress8,5,nie_readEEPROM(ioaddress8, 414));
                nie_writeCALDAC(ioaddress8,7,nie_readEEPROM(ioaddress8, 413));
                nie_writeCALDAC(ioaddress8,6,nie_readEEPROM(ioaddress8, 412));
                nie_writeCALDAC(ioaddress8,8,nie_readEEPROM(ioaddress8, 411));
                nie_writeCALDAC(ioaddress8,10,nie_readEEPROM(ioaddress8, 410));
                nie_writeCALDAC(ioaddress8,9,nie_readEEPROM(ioaddress8, 409));
            }
        } */
        break;
      case 6:
        /* for (i=0;i<nChannels;i++) {
            channel=mxGetPr(CHANNEL_ARG)[i];
            range=(int_T)(mxGetPr(RANGE_ARG)[i]);
            if (range < 0) { */
                nie_writeCALDAC(ioaddress8,5,nie_readEEPROM(ioaddress8, 420));
                nie_writeCALDAC(ioaddress8,7,nie_readEEPROM(ioaddress8, 419));
                nie_writeCALDAC(ioaddress8,13,nie_readEEPROM(ioaddress8, 419));
                nie_writeCALDAC(ioaddress8,6,nie_readEEPROM(ioaddress8, 418));
                nie_writeCALDAC(ioaddress8,8,nie_readEEPROM(ioaddress8, 417));
                nie_writeCALDAC(ioaddress8,10,nie_readEEPROM(ioaddress8, 416));
                nie_writeCALDAC(ioaddress8,9,nie_readEEPROM(ioaddress8, 415));
        /*    } else {
                nie_writeCALDAC(ioaddress8,5,nie_readEEPROM(ioaddress8, 414));
                nie_writeCALDAC(ioaddress8,7,nie_readEEPROM(ioaddress8, 413));
                nie_writeCALDAC(ioaddress8,6,nie_readEEPROM(ioaddress8, 412));
                nie_writeCALDAC(ioaddress8,8,nie_readEEPROM(ioaddress8, 411));
                nie_writeCALDAC(ioaddress8,10,nie_readEEPROM(ioaddress8, 410));
                nie_writeCALDAC(ioaddress8,9,nie_readEEPROM(ioaddress8, 409));
            }
        } */
        break;
      case 7:
        /*for (i=0;i<nChannels;i++) {
            channel=mxGetPr(CHANNEL_ARG)[i];
            range=(int_T)(mxGetPr(RANGE_ARG)[i]);
            if (range < 0) { */
                nie_writeCALDAC(ioaddress8,5,nie_readEEPROM(ioaddress8, 420));
                nie_writeCALDAC(ioaddress8,7,nie_readEEPROM(ioaddress8, 419));
                nie_writeCALDAC(ioaddress8,13,nie_readEEPROM(ioaddress8, 419));
                nie_writeCALDAC(ioaddress8,6,nie_readEEPROM(ioaddress8, 418));
                nie_writeCALDAC(ioaddress8,8,nie_readEEPROM(ioaddress8, 417));
                nie_writeCALDAC(ioaddress8,10,nie_readEEPROM(ioaddress8, 416));
                nie_writeCALDAC(ioaddress8,9,nie_readEEPROM(ioaddress8, 415));
        /*    } else {
                nie_writeCALDAC(ioaddress8,5,nie_readEEPROM(ioaddress8, 414));
                nie_writeCALDAC(ioaddress8,7,nie_readEEPROM(ioaddress8, 413));
                nie_writeCALDAC(ioaddress8,6,nie_readEEPROM(ioaddress8, 412));
                nie_writeCALDAC(ioaddress8,8,nie_readEEPROM(ioaddress8, 411));
                nie_writeCALDAC(ioaddress8,10,nie_readEEPROM(ioaddress8, 410));
                nie_writeCALDAC(ioaddress8,9,nie_readEEPROM(ioaddress8, 409));
            }
        }*/
        break;
      case 8:
      case 12:
        /*for (i=0;i<nChannels;i++) {
            channel=mxGetPr(CHANNEL_ARG)[i];
            range=(int_T)(mxGetPr(RANGE_ARG)[i]);
            if (range < 0) { */
                nie_writeCALDAC(ioaddress8,5,nie_readEEPROM(ioaddress8, 420));
                nie_writeCALDAC(ioaddress8,7,nie_readEEPROM(ioaddress8, 419));
                nie_writeCALDAC(ioaddress8,13,nie_readEEPROM(ioaddress8, 419));
                nie_writeCALDAC(ioaddress8,6,nie_readEEPROM(ioaddress8, 418));
                nie_writeCALDAC(ioaddress8,8,nie_readEEPROM(ioaddress8, 417));
                nie_writeCALDAC(ioaddress8,10,nie_readEEPROM(ioaddress8, 416));
                nie_writeCALDAC(ioaddress8,9,nie_readEEPROM(ioaddress8, 415));
        /*    } else {
                nie_writeCALDAC(ioaddress8,5,nie_readEEPROM(ioaddress8, 414));
                nie_writeCALDAC(ioaddress8,7,nie_readEEPROM(ioaddress8, 413));
                nie_writeCALDAC(ioaddress8,6,nie_readEEPROM(ioaddress8, 412));
                nie_writeCALDAC(ioaddress8,8,nie_readEEPROM(ioaddress8, 411));
                nie_writeCALDAC(ioaddress8,10,nie_readEEPROM(ioaddress8, 410));
                nie_writeCALDAC(ioaddress8,9,nie_readEEPROM(ioaddress8, 409));
            }
        } */
        break;
      case 9:
      case 16:
        for (i=0;i<nChannels;i++) {
            channel = (int_T)mxGetPr(CHANNEL_ARG)[i];
            range=(int_T)(mxGetPr(RANGE_ARG)[i]);
            if (range < 0) {
                if (channel==1) {
                    nie_writeCALDAC1(ioaddress8,0,nie_readEEPROM(ioaddress8, 406));
                    nie_writeCALDAC1(ioaddress8,8,nie_readEEPROM(ioaddress8, 405));
                    nie_writeCALDAC1(ioaddress8,4,nie_readEEPROM(ioaddress8, 404));
                    nie_writeCALDAC1(ioaddress8,12,nie_readEEPROM(ioaddress8, 403));
                } else {
                    nie_writeCALDAC1(ioaddress8,2,nie_readEEPROM(ioaddress8, 402));
                    nie_writeCALDAC1(ioaddress8,10,nie_readEEPROM(ioaddress8, 401));
                    nie_writeCALDAC1(ioaddress8,6,nie_readEEPROM(ioaddress8, 400));
                    nie_writeCALDAC1(ioaddress8,14,nie_readEEPROM(ioaddress8, 399));
                }
            } else {
                if (channel==1) {
                    nie_writeCALDAC1(ioaddress8,0,nie_readEEPROM(ioaddress8, 398));
                    nie_writeCALDAC1(ioaddress8,8,nie_readEEPROM(ioaddress8, 397));
                    nie_writeCALDAC1(ioaddress8,4,nie_readEEPROM(ioaddress8, 396));
                    nie_writeCALDAC1(ioaddress8,12,nie_readEEPROM(ioaddress8, 395));
                } else {
                    nie_writeCALDAC1(ioaddress8,2,nie_readEEPROM(ioaddress8, 394));
                    nie_writeCALDAC1(ioaddress8,10,nie_readEEPROM(ioaddress8, 3931));
                    nie_writeCALDAC1(ioaddress8,6,nie_readEEPROM(ioaddress8, 392));
                    nie_writeCALDAC1(ioaddress8,14,nie_readEEPROM(ioaddress8, 391));
                }
            }
        }
        break;
      case 10:
      case 11:
        range=(int_T)(mxGetPr(RANGE_ARG)[0]);
        if (range < 0) {
            nie_writeCALDAC2(ioaddress8,6,nie_readEEPROM(ioaddress8, 417));
            nie_writeCALDAC2(ioaddress8,4,nie_readEEPROM(ioaddress8, 416));
            nie_writeCALDAC2(ioaddress8,7,nie_readEEPROM(ioaddress8, 415));
            nie_writeCALDAC2(ioaddress8,5,nie_readEEPROM(ioaddress8, 414));
        } else {
            nie_writeCALDAC2(ioaddress8,6,nie_readEEPROM(ioaddress8, 413));
            nie_writeCALDAC2(ioaddress8,4,nie_readEEPROM(ioaddress8, 412));
            nie_writeCALDAC2(ioaddress8,7,nie_readEEPROM(ioaddress8, 411));
            nie_writeCALDAC2(ioaddress8,5,nie_readEEPROM(ioaddress8, 410));
        }
        break;
      case 22:
        nie_writeCALDAC2(ioaddress8,6,nie_readEEPROM(ioaddress8, 426));
        nie_writeCALDAC2(ioaddress8,4,nie_readEEPROM(ioaddress8, 425));
        nie_writeCALDAC2(ioaddress8,7,nie_readEEPROM(ioaddress8, 424));
        nie_writeCALDAC2(ioaddress8,5,nie_readEEPROM(ioaddress8, 423));
        break;
    }


    for (i=0;i<nChannels;i++) {

        channel = (int_T)mxGetPr(CHANNEL_ARG)[i];
        range=(int_T)(mxGetPr(RANGE_ARG)[i]);

        switch (range) {
          case -10:
            switch (channel) {
              case 1:
                nie_Board_Write(ioaddress, AO_Configuration_Register,0x0001);
                break;
              case 2:
                nie_Board_Write(ioaddress, AO_Configuration_Register,0x0101);
                break;
            }
            ssSetRWorkValue(S, GAIN_R_IND+i, 20.0/res_float);
            ssSetRWorkValue(S, MINVAL_R_IND+i, -10.0);
            ssSetRWorkValue(S, MAXVAL_R_IND+i, 10.0-20.0/res_float);
            break;
          case 10:
            switch (channel) {
              case 1:
                nie_Board_Write(ioaddress, AO_Configuration_Register,0x0000);
                break;
              case 2:
                nie_Board_Write(ioaddress, AO_Configuration_Register,0x0100);
                break;
            }
            ssSetRWorkValue(S, GAIN_R_IND+i, 10.0/res_float);
            ssSetRWorkValue(S, MINVAL_R_IND+i, 0.0);
            ssSetRWorkValue(S, MAXVAL_R_IND+i, 10.0-10.0/res_float);
            break;
        }
    }


    nie_AO_Reset_All(ioaddress);
    nie_AO_Board_Personalize(ioaddress);
    nie_AO_LDAC_Source_And_Update_Mode(ioaddress);

#endif

}

static void mdlOutputs(SimStruct *S, int_T tid)
{

#ifndef MATLAB_MEX_FILE

    uint_T  base = ssGetIWorkValue(S, BASE_ADDR_I_IND);
    size_t nChannels = ssGetIWorkValue(S, CHANNELS_I_IND);
    size_t i;
    real_T output;
    int_T channel;
    short outval;
    unsigned short *ioaddress;
    InputRealPtrsType uPtrs;

    ioaddress=(void *) base;

    for (i=0;i<nChannels;i++) {
        channel = (int_T)mxGetPr(CHANNEL_ARG)[i];
        uPtrs=ssGetInputPortRealSignalPtrs(S,i);
        output=*uPtrs[0];
        if (output<ssGetRWorkValue(S, MINVAL_R_IND+i)) 
            output=ssGetRWorkValue(S, MINVAL_R_IND+i);
        if (output>ssGetRWorkValue(S, MAXVAL_R_IND+i)) 
            output=ssGetRWorkValue(S, MAXVAL_R_IND+i);
        outval=(short)(output/ssGetRWorkValue(S, GAIN_R_IND+i));
        switch (channel) {
          case 1:
            nie_Board_Write(ioaddress, AO_DAC_0_Data_Register,outval);
            break;
          case 2:
            nie_Board_Write(ioaddress, AO_DAC_1_Data_Register,outval);
            break;
        }
    }

#endif

}


static void mdlTerminate(SimStruct *S)
{

#ifndef MATLAB_MEX_FILE

    uint_T base = ssGetIWorkValue(S, BASE_ADDR_I_IND);
    size_t nChannels = ssGetIWorkValue(S, CHANNELS_I_IND);
    unsigned short *ioaddress;
    int_T channel;
    size_t i;
    short outval;
    real_T output;

    ioaddress=(void *) base;

    outval=(short) 0;

    // At load time, set channels to their initial values.
    // At model termination, reset resettable channels to their initial values.
    for (i=0;i<nChannels;i++) {
        if (xpceIsModelInit() || (uint_T)mxGetPr(RESET_ARG)[i]) {
            channel = (int_T)mxGetPr(CHANNEL_ARG)[i];
            output=mxGetPr(INIT_VAL_ARG)[i];
            if (output<ssGetRWorkValue(S, MINVAL_R_IND+i)) 
                output=ssGetRWorkValue(S, MINVAL_R_IND+i);
            if (output>ssGetRWorkValue(S, MAXVAL_R_IND+i)) 
                output=ssGetRWorkValue(S, MAXVAL_R_IND+i);
            outval=(short)(output/ssGetRWorkValue(S, GAIN_R_IND+i));
            switch (channel) {
              case 1:
                nie_Board_Write(ioaddress, AO_DAC_0_Data_Register,outval);
                break;
              case 2:
                nie_Board_Write(ioaddress, AO_DAC_1_Data_Register,outval);
                break;
            }
        }
    }

#endif

}

#ifdef MATLAB_MEX_FILE  /* Is this file being compiled as a MEX-file? */
#include "simulink.c"   /* Mex glue */
#else
#include "cg_sfun.h"    /* Code generation glue */
#endif
