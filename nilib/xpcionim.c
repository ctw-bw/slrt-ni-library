
/* Abstract: NI PCI-M functions
 *
 *  Copyright 2008-2009 The MathWorks, Inc.
 *
 */


#include   <stddef.h>
#include   <stdlib.h>

#include   "simstruc.h"
#include   "xpctarget.h"
#include   "xpcionim.h"

// Define some macros to make channel setting look neater.
#define POLARITY(x) (((x) & 0x1) << 12)
#define GAIN(x)     (((x) & 0x7) << 9)
#define CHANNEL(x)  ((x-1) & 0x3f)
#define CHANTYPE(x) (((x) & 0x7) << 6)
#define DITHER(x)   (((x) & 0x1) << 13)
#define LAST(x)     (((x) & 0x1) << 14)

#define START1_SELECT(val)    ((val) & 0x001f)
#define START1_EDGE(val)     (((val) & 0x0001) << 5)
#define START1_SYNC(val)     (((val) & 0x0001) << 6)
#define START2_SELECT(val)   (((val) & 0x001f) << 7)
#define START2_EDGE(val)     (((val) & 0x0001) << 12)
#define START2_SYNC(val)     (((val) & 0x0001) << 13)
#define START2_POLARITY(val) (((val) & 0x0001) << 14)
#define START1_POLARITY(val) (((val) & 0x0001) << 15)

void niM_AISetup( void *base,
                  bool reset5x,
                  uint32_T *channels,
                  uint32_T *range,
                  uint32_T *coupling,
                  uint32_T *polarity,
                  uint32_T nchans,
                  uint32_T scancount )
{
    volatile uint32_T *lreg = (volatile uint32_T *)base;
    volatile uint16_T *sreg = (volatile uint16_T *)base;
    volatile uint8_T  *breg = (volatile uint8_T  *)base;
    uint32_T idx;
    uint16_T ststop;
    volatile uint16_T dummy;

    niM_configureTimebase( base );
    niM_pllReset( base );
    niM_analogTriggerReset( base );

    sreg[S_Joint_Reset] = 1;  // do the reset
    sreg[S_Joint_Reset] = 0x10;  // configuration mode, no glitches

    dummy = sreg[S_Joint_Status_1 ];  // else Watcom combines writes!

    sreg[S_Interrupt_A_Enable] = 0;  // None enabled
    sreg[S_Interrupt_A_Ack] = 0x3f8; // all ints cleared
    sreg[S_AI_Mode_1] = 0x0008;
    dummy = sreg[S_Joint_Status_1 ];  // else Watcom combines writes!
    sreg[S_AI_Mode_2] = 0x0000;
    sreg[S_AI_Mode_3] = 0x0000;

    sreg[S_Joint_Reset] = 0x100;  // Configuration end
    dummy = sreg[S_Joint_Status_1 ];  // else Watcom combines writes!
    sreg[S_Joint_Reset] = 0x10;  // configuration mode, no glitches

    if( reset5x )  // 5x boards
        sreg[S_AI_Output_Control] = 0x0002; // kAI_CONVERT_Output_SelectActive_Low=2
    else  // all the rest
        sreg[S_AI_Output_Control] = 0x0000;

    sreg[S_AI_Personal] = 0x0000;

    sreg[S_Joint_Reset] = 0x100;  // Configuration end
    dummy = sreg[S_Joint_Status_1 ];  // else Watcom combines writes!
    sreg[S_Joint_Reset] = 0x10;  // configuration mode, no glitches

    sreg[S_AI_START_STOP_Select] = 0x0000;
    dummy = sreg[S_Joint_Status_1 ];  // else Watcom combines writes!
    sreg[S_AI_Trigger_Select] = 0x0000;

    if( reset5x )  // 5x boards
        sreg[S_AI_Output_Control] = 0x302;
    else  // all the rest
        sreg[S_AI_Output_Control] = 0x303;
    sreg[S_AI_Personal] = 0x0400;

    sreg[S_AI_START_STOP_Select] = 0x0000;
    dummy = sreg[S_Joint_Status_1 ];  // else Watcom combines writes!
    sreg[S_AI_Trigger_Select] = 0x0000;

    sreg[S_AI_FIFO_Clear] = 1;

    if( reset5x )
    {
        // NOTE: the correct order is write 0, then 1, per a comment
        // on the NI website in the DDK message board from 11-21-2005.
        //board->Static_AI_Control[0].writeRegister (0);
        breg[B_Static_AI_Control0] = 0;
        //board->Static_AI_Control[0].writeRegister (1);
        breg[B_Static_AI_Control0] = 1;
    
        //board->AI_Command_1.writeAI_CONVERT_Pulse (1);
        sreg[S_AI_Command_1] = 1;  // strobe bit, resets
        //board->AI_Command_1.writeAI_CONVERT_Pulse (1);
        sreg[S_AI_Command_1] = 1;
        //board->AI_Command_1.writeAI_CONVERT_Pulse (1);
        sreg[S_AI_Command_1] = 1;
    }

    sreg[S_Joint_Reset] = 0x0001;  // Disarm and reset AI registers

    sreg[S_Configuration_Memory_Clear] = 1; // clear config memory
    lreg[L_AI_Config_FIFO_Bypass] = 0;

    for( idx = 0 ; idx < nchans ; idx++ )
    {
        niM_aiConfigureChannel( base,
                                (uint16_T)channels[idx],
                                (uint16_T)range[idx],
                                (uint16_T)polarity[idx],  /* bipolar == 0 */
                                (uint16_T)coupling[idx],
                                (bool)((idx == (nchans-1)) ? true : false ));
    }

    sreg[ S_AI_Mode_3 ] = 0;  // set FIFO request mode

    // aiTrigger
    sreg[ S_AI_Mode_1 ] = 0x9;  // require a software write for each scan
    sreg[ S_AI_Trigger_Select ] = START1_SELECT(0)
                                | START1_EDGE(1)
                                | START1_SYNC(1)
                                | START1_POLARITY(0)
                                | START2_SELECT(0)
                                | START2_EDGE(1)
                                | START2_SYNC(1)
                                | START2_POLARITY(0);
    sreg[ S_AI_Mode_2 ] = 0;

    // aiSampleStop
    // AI_Stop_SelectIN = 0x13 << 7 = 0x0980
    // AI_STOP_PolarityActive_High = 0
    // AI_STOP_Sync = 1 << 13 = 0x2000
    // AI_STOP_Edge = 1 << 12 = 0x1000, if multichannel
    ststop = 0x2980 | ((nchans > 1) ? 0x1000 : 0 );
    sreg[ S_AI_START_STOP_Select ] = ststop;
    
    // aiNumberOfSamples, number of scans through the channel list
    // original: sreg[ S_AI_Mode_1 ] = 0x9 | 0x2;  // AI_Continuous | AI_Trigger_Once;
    sreg[ S_AI_Mode_1 ] = 0x9 | 0x2;  // AI_Continuous | AI_Trigger_Once
    lreg[ L_AI_SC_Load_A ] = 0; // postTriggerSamples-1;
    lreg[ L_AI_SC_Load_B ] = 0;
    sreg[ S_AI_Command_1 ] = 0x20;  // AI_SC_Load, strobe bit, resets

    // aiSampleStart
    // AI_START_SelectPulse = 0x12
    // AI_START_PolarityRisingEdge = 0 in bit 15
    // AI_START_Sync = 1 << 6 = 0x40
    // AI_START_Edge = 1 << 5 = 0x20
    ststop |= 0x12 | 0x60;
    sreg[ S_AI_START_STOP_Select ] = ststop;

    // aiConvert
    sreg[ S_AI_Mode_1 ] = 0xb;  // Sum of all bits we already set.
    dummy = sreg[S_Joint_Status_1 ];  // else Watcom combines writes!

    sreg[ S_AI_Mode_2 ] = 0x100;
    sreg[ S_AI_Mode_3 ] = 0; // given externalSampleClock == false
    lreg[ L_AI_SI2_Load_A ] = 10; // scancount;  // delayDivisor;, use a minimum val?
    lreg[ L_AI_SI2_Load_B ] = scancount;  // periodDivisor;
    sreg[ S_AI_Mode_2 ] = 0x100;     // Load A, initial delay
    sreg[ S_AI_Command_1 ] = 0x800;  // Strobe bit, resets
    sreg[ S_AI_Mode_2 ] = 0x300;     // Load B, sample separation

    sreg[S_AI_FIFO_Clear] = 1;

    sreg[S_Joint_Reset] = 0x100;  // Configuration end

    // aiArm
    sreg[ S_AI_Command_1 ] = 0x1040;

    // aiStart
    sreg[ S_AI_Command_2 ] = 0x0001;
}

void niM_aiConfigureChannel (void *base,
                             uint16_T channel, 
                             uint16_T gain, 
                             uint16_T polarity, 
                             uint16_T channelType,
                             bool lastChannel)
{
    volatile uint16_T *sreg = (volatile uint16_T *)base;

    //board->AI_Config_FIFO_Data.setAI_Config_Polarity (polarity);
    //board->AI_Config_FIFO_Data.setAI_Config_Gain (gain);   
    //board->AI_Config_FIFO_Data.setAI_Config_Channel (channel & 0xF);
    //board->AI_Config_FIFO_Data.setAI_Config_Bank ((channel & 0x30) >> 4);
    //board->AI_Config_FIFO_Data.setAI_Config_Channel_Type (channelType);
    //board->AI_Config_FIFO_Data.setAI_Config_Dither (0);
    //board->AI_Config_FIFO_Data.setAI_Config_Last_Channel (lastChannel);
    //board->AI_Config_FIFO_Data.flush ();   

    // Debug output
    //printf("CHAN = 0x%x\n", POLARITY(polarity) |
    //   GAIN(gain) |
    //   CHANNEL(channel) |
    //   CHANTYPE(channelType) |
    //   DITHER(0) |
    //   LAST(lastChannel ));
    sreg[S_AI_Config_FIFO_Data] = POLARITY(polarity) |
                                  GAIN(gain) |
                                  CHANNEL(channel) |
                                  CHANTYPE(channelType) |
                                  DITHER(0) |
                                  LAST(lastChannel);
    if (lastChannel)
    {
        //board->AI_Command_1.setAI_LOCALMUX_CLK_Pulse (kTrue);
        //board->AI_Command_1.flush ();
        sreg[S_AI_Command_1] = 0x4;  // strobe bit, resets
    }
    
    return;
}

void niM_configureTimebase (void *base)
{
    volatile unsigned short *sreg = (volatile unsigned short *)base;

    //board->Clock_and_FOUT.setSlow_Internal_Timebase(1);
    //board->Clock_and_FOUT.flush();
    sreg[ S_Clock_and_FOUT ] = 0x0800; // 0x1b00; // 1 << 11;
    
    return;
}

void niM_pllReset (void *base)
{
    volatile unsigned short *sreg = (volatile unsigned short *)base;
    volatile uint16_T dummy;

    //board->Clock_And_Fout2.setTB1_Select(tMSeries::tClock_And_Fout2::kTB1_SelectSelect_OSC);
    //board->Clock_And_Fout2.setTB3_Select(tMSeries::tClock_And_Fout2::kTB3_SelectSelect_OSC);
    //board->Clock_And_Fout2.flush();
    sreg[ S_Clock_And_Fout2 ] = 0;
    
    dummy = sreg[S_Joint_Status_1 ];  // else Watcom combines writes!

    //board->PLL_Control.setPLL_Enable (kFalse);
    //board->PLL_Control.flush ();
    sreg[ S_PLL_Control ] = 0;

    return;
}

void niM_analogTriggerReset (void *base)
{
    volatile unsigned short *sreg = (volatile unsigned short *)base;
    volatile unsigned  char *breg = (volatile unsigned  char *)base;
    volatile uint16_T dummy;

    //board->Analog_Trigger_Etc.setAnalog_Trigger_Reset(1); // 1 << 5 = 0x20
    //board->Analog_Trigger_Etc.setAnalog_Trigger_Mode(tMSeries::tAnalog_Trigger_Etc::kAnalog_Trigger_ModeLow_Window); // 0 << 0 = 0
    //board->Analog_Trigger_Etc.flush();
    sreg[ S_Analog_Trigger_Etc ] = 0x20;
   
    //board->Analog_Trigger_Control.setAnalog_Trigger_Select(tMSeries::tAnalog_Trigger_Control::kAnalog_Trigger_SelectGround);  // 3 << 0 = 0x3
    //board->Analog_Trigger_Control.flush();
    breg[ B_Analog_Trigger_Control ] = 0x3;

    //board->Gen_PWM[0].writeRegister(0);
    sreg[ S_Gen_PWM0 ] = 0;

    dummy = sreg[S_Joint_Status_1 ];  // else Watcom combines writes!

    //board->Gen_PWM[1].writeRegister(0);
    sreg[ S_Gen_PWM1 ] = 0;
    
    //board->Analog_Trigger_Etc.setAnalog_Trigger_Enable(tMSeries::tAnalog_Trigger_Etc::kAnalog_Trigger_EnableDisabled);  // 0 << 3 = 0, already written to this value
    //board->Analog_Trigger_Etc.flush();
    sreg[ S_Analog_Trigger_Etc ] = 0; // x20;
    
    return; 
}

static MseriesBoard SupportedBoard[] =
{
    // Low cost board family, 622x
    { 20, {0x70b0,      0, 0x70ae,      0}, 0, false, 16, {80, 80},  8, 0, 0, "NI PCI-6220" },
    { 21, {0x70af,      0, 0x70b5,      0}, 0, false, 16, {80, 80},  8, 2, 0, "NI PCI-6221" },
    { 27, {0x71bc,      0,      0,      0}, 0, false, 16, {80, 80},  2, 2, 0, "NI PCI-6221/37" },
    { 24, {0x70f2,      0, 0x70f3,      0}, 0, false, 32, {80, 80}, 32, 0, 0, "NI PCI-6224" },
    { 25, {0x716c,      0, 0x716d,      0}, 0, false, 80, {80, 80},  8, 2, 0, "NI PCI-6225" },
    { 29, {0x70aa,      0, 0x70b1,      0}, 0, false, 32, {80, 80}, 32, 4, 0, "NI PCI-6229" },

    // Isolated input boards, 623x
    { 30, {0x716b,      0, 0x7177,      0}, 1, false,  8, {80, 80},  0, 4, 0, "NI PCI-6230" },
    { 32, {0x7279,      0, 0x727a,      0}, 1, false, 16, {80, 80},  0, 2, 0, "NI PCI-6232" },
    { 33, {0x7209,      0, 0x720a,      0}, 1, false, 16, {80, 80},  0, 2, 0, "NI PCI-6233" },
    { 36, {0x7281,      0, 0x7282,      0}, 1, false,  4, {80, 80},  0, 4, 0, "NI PCI-6236" },
    { 38, {0x720b,      0, 0x720c,      0}, 1, false,  8, {80, 80},  0, 2, 0, "NI PCI-6238" },
    { 39, {0x727b,      0, 0x727c,      0}, 1, false,  8, {80, 80},  0, 2, 0, "NI PCI-6239" },

    // High speed board family, 625x
    { 50, {0x70b4,      0, 0x70b9,      0}, 1,  true, 16, {16, 20},  8, 0, 0, "NI PCI-6250" },
    { 51, {0x70b8, 0x717d, 0x70ad, 0x72e8}, 1,  true, 16, {16, 20},  8, 2, 2, "NI PCI-6251" },
    { 54, {0x70b7,      0, 0x70ba,      0}, 1,  true, 32, {16, 20}, 32, 0, 0, "NI PCI-6254" },
    { 55, {0x71e0,      0, 0x71e1,      0}, 1,  true, 80, {16, 20},  8, 2, 2, "NI PCI-6255" },
    { 59, {0x70ab, 0x717f, 0x70b2, 0x72e9}, 1,  true, 32, {16, 20}, 32, 4, 3, "NI PCI-6259" },

    // High precision, 18 bit family, 628x
    { 80, {0x70b6,      0, 0x70bb,      0}, 1, false, 16, {30, 40},  8, 0, 0, "NI PCI-6280" },
    { 81, {0x70bd,      0, 0x70bf,      0}, 1, false, 16, {30, 40},  8, 2, 2, "NI PCI-6281" },
    { 84, {0x70bc,      0, 0x70be,      0}, 1, false, 32, {30, 40}, 32, 0, 0, "NI PCI-6284" },
    { 89, {0x70ac,      0, 0x70b3,      0}, 1, false, 32, {30, 40}, 32, 4, 3, "NI PCI-6289" },
};

static char msg[256]; // for seterror calls

MseriesBoard *niMIdentify( SimStruct *S, int deviceType, int bus, int slot, xpcPCIDevice *pciinfo )
{
    int i;
    int j;
    const char *devName;
    uint32_T devId;
    uint32_T found = 0;

    devId = 0;
    for( i = 0 ; i < sizeof(SupportedBoard)/sizeof(MseriesBoard) ; i++ )
    {
        if( SupportedBoard[i].Type == (uint32_T)deviceType )
        {
            for( j = 0 ; j < 4 ; j++ )
            {
                devId = SupportedBoard[i].devID[j];
                if( devId != 0 )
                {
                    // return 1 if getinfo error
                    // return 0 if ok
                    if( niMconfig( devId, bus, slot, pciinfo ) != 1 )
                    {
                        found = 1;
                        break;
                    }
                }
            }
            if( found == 0 ) // devtype found, but board isn't present
            {
                devName = SupportedBoard[i].name;
                sprintf(msg,"%s (bus %d, slot %d): board not present", devName, bus, slot );
                ssSetErrorStatus(S,msg);
                return 0;
            }

            return &SupportedBoard[i];
       }
    }
    sprintf( msg, "NI M series: Unknown board type %d", deviceType );
    ssSetErrorStatus(S,msg);
    return 0;
}

// ----- PCI configuration subroutine --------
int niMconfig( int deviceId, int bus, int slot, xpcPCIDevice *pciinfo )
{
    // return 1 if get info error
    // return 0 if ok
    uint16_T  vendorId;
    uint16_T  subvendorId, subdeviceId;
    volatile unsigned int *lbar0;

    vendorId = 0x1093;
    subvendorId = XPC_NO_SUB;  // Not checked
    subdeviceId = XPC_NO_SUB;  // Not checked

    // look for the PCI-Device
    if( xpcGetPCIDeviceInfo( vendorId, (uint16_T)deviceId, subvendorId, subdeviceId, bus, slot, pciinfo ) )
    {
        return 1;
    }

    // The MITE chip
    pciinfo->VirtAddress[0] =
        (unsigned long)xpcReserveMemoryRegion( (void *)pciinfo->BaseAddress[0],
                                               4096,
                                               XPC_RT_PG_USERREADWRITE );
    // Register space for IO
    pciinfo->VirtAddress[1] =
        (unsigned long)xpcReserveMemoryRegion( (void *)pciinfo->BaseAddress[1],
                                               4096,
                                               XPC_RT_PG_USERREADWRITE );

    // show Device Information
    //xpcShowPCIDeviceInfo( pciinfo );

    // We need to get to register space as long, short or byte.
    // For 32 bit wide registers: lreg[L_REGNAME]
    // For 16 bit wide registers: sreg[S_REGNAME]
    // For 8 bit wide registers: breg[B_REGNAME]
    // The register names indicate byte, short or long.

    // Enable the MITE
    lbar0 = (unsigned int *)pciinfo->VirtAddress[0];
    lbar0[48] = ((unsigned int)pciinfo->BaseAddress[1] & 0xffffff00) | 0x00000080;

    return 0;  // no error
    // --- end common PCI config subroutine ----
}

#define NIM_MAXBOARDS 10
static struct niM_private
{
    void *base;
    uint32_T command[2];
    uint32_T pfi_config[6];
    uint32_T pfi_filter;
    uint32_T pfi_do;
} private_data[NIM_MAXBOARDS];

uint32_T niM_Private_Data( void *base )
{
    static int firsttime = 1;
    int i;
    
    if( firsttime == 1 )
    {
        for( i = 0 ; i < NIM_MAXBOARDS ; i++ )
        {
            private_data[i].base = 0;
            private_data[i].command[0] = 0;
            private_data[i].command[1] = 0;
            private_data[i].pfi_config[0] = 0;  // All default PFI
            private_data[i].pfi_config[1] = 0;
            private_data[i].pfi_config[2] = 0;
            private_data[i].pfi_config[3] = 0;
            private_data[i].pfi_config[4] = 0;
            private_data[i].pfi_config[5] = 0;
            private_data[i].pfi_filter = 0;
            private_data[i].pfi_do = 0;
        }
        firsttime = 0;  // Don't repeat the initialization
    }
    // Look for this base address, return index if found
    for( i = 0 ; i < NIM_MAXBOARDS ; i++ )
    {
        if( base == private_data[i].base ) // found
            return i;
    }
    // Didn't find it, look for the first 0 base, claim it.
    for( i = 0 ; i < NIM_MAXBOARDS ; i++ )
    {
        if( private_data[i].base == 0 )
        {
            private_data[i].base = base;
            return i;
        }
    }
    return -1;  // Didn't find it and no more room.
}

int32_T niM_SetPFIFilter( uint32_T bnum, uint32_T chan, uint32_T value )
{
    // chan is the PFI number, [0, 15]
    // value is in [0, 1, 2, 3]
    uint32_T mask = 3 << (2*chan);
    uint32_T filt = private_data[bnum].pfi_filter;
    volatile uint32_T *lreg = (volatile uint32_T *)private_data[bnum].base;

    if( lreg == 0 )
        return -1;  // Null pointer, bad bnum
    
    if( value > 3 )
        return -2;  // error, out of range
    
    if( chan > 15 )
        return -3; // error, channel out of range
    
    filt = (filt & ~mask) | ((value & 3) << (2*chan));
    private_data[bnum].pfi_filter = filt;
    lreg[ L_PFI_Filter ] = filt;
    return 0;
}

// Set connect to 0 to get the default connection, or
// Set it to the value from the NI doc to choose a non-default connection
void niM_SetPFI_Select( uint32_T bnum, uint32_T pfinum, uint32_T connect )
{
    uint32_T idx = pfinum/3;
    uint32_T shift = (pfinum % 3) * 5;
    uint32_T val = private_data[bnum].pfi_config[idx];
    volatile uint16_T *sreg = (volatile uint16_T *)private_data[bnum].base;
    
    connect &= 0x1f;
    val = (val & ~(0x1f << shift)) | (connect << shift);
    
    private_data[bnum].pfi_config[idx] = val;
    sreg[S_PFI_Output_Select_1 + idx] = val;
}

// Get the previous DO value, add the specified bit and return it.
// The caller writes it to the register.
uint16_T niM_SetPFI_DO( uint32_T bnum, uint32_T pfinum, uint32_T val )
{
    uint16_T doval = private_data[bnum].pfi_do;
    doval = (doval & ~(1 << pfinum)) | ((val & 1) << pfinum);
    private_data[bnum].pfi_do = doval;
    return doval;
}

/*
 * When using the G0 and G1 register names, simply adding
 * 1 to the G0 define gives the correct value for the G1
 * define.
 * That works for both the short and long registers.
 * L_G0_Load_A +1 == L_G0_Load_B since the byte addresses
 * differ by 4, but the symbols divide by 4 to get a word offset.
 */

/* Which Bank (X or Y) is being used (X => 0, Y => 1) */
uint32_T niM_whichBank(volatile uint16_T *sreg, uint32_T chan)
{
    /* If ctr 0, it's bit 0; else it's bit 1 */
    ushort_T mask = 0x1 << chan;  // channels 0 or 1

    return (sreg[S_G01_Joint_Status_1] & mask) == mask;
}

/* Reset given counter */
void niM_resetCtr(volatile uint16_T *sreg, int chan)
{
    /* if chan 0, bit 2, otherwise bit 3 */
    sreg[S_G01_Joint_Reset] = 0x4 << chan;
}

// PWM output arm and disarm routines
void niM_CtrArm(void *base, uint32_T chan )
{
    volatile uint16_T *sreg = (volatile uint16_T *)base;
    
    sreg[ S_G0_Command + chan ] = 0x1905;  // load and arm
    //sreg[ S_G0_Command + chan ] = 0x1901;  // arm, no load
}

void niM_CtrDisarm(void *base, uint32_T chan )
{
    volatile uint16_T *sreg = (volatile uint16_T *)base;
    
    sreg[ S_G0_Command + chan ] = 0x1910;
}

// literal copy of example
void niM_Cont_Pulse_Train_Generation(void     *base,
                                     uint32_T chan,
                                     uint32_T low, uint32_T high )
{
    volatile uint32_T *lreg = (volatile uint32_T *)base;
    volatile uint16_T *sreg = (volatile uint16_T *)base;
    volatile uint8_T  *breg = (volatile uint8_T *)base;
    uint16_T inputselect = 0;
    uint16_T command = 0;
    uint16_T mode = 0;
    
	//MSeries.CTR.Source
    //board->G0_Input_Select.setG0_Source_Select (30);  // 80 MHz
    //board->G0_Input_Select.setG0_Source_Polarity(0); // rising=0
	//board->G0_Input_Select.flush();
    // Gn_SourceSelect(30) = 30 << 2 = 0x1e << 2 = 0x78
    // Gn_Source_Polarity(0) = 0 << 15 = 0
    inputselect = 0x78;
    sreg[ S_G0_Input_Select + chan ] = inputselect;
    
	//MSeries.CTR.Gate
	//board->G0_Input_Select.setG0_Gate_Select(31); // logic low  
	//board->G0_Input_Select.setG0_Output_Polarity(0); //active low=0   
	//board->G0_Input_Select.flush ();
    // setG0_Gate_Select(31) = 0x1f << 7 = 0x0f80
    inputselect |= 0x0f80;
    sreg[ S_G0_Input_Select + chan ] = inputselect;

	//MSeries.CTR.IncrementRegisters
	//board->G0_AutoIncrement.writeRegister(0);
    sreg[ S_G0_AutoIncrement + chan ] = 0;

	//MSeries.CTR.InitialCountRegisters
	//board->G0_Mode.writeG0_Load_Source_Select(tTIO::tG0_Mode::tG0_Load_Source_Select::kG0_Load_Source_SelectLoad_A);
    // kG0_Load_Source_SelectLoad_A = 0, SelectLoad_B = 1
    // Load_Source_Select(0) = 0 << 7, 1 bit
    sreg[ S_G0_Mode + chan ] = 0;
    
    //board->G0_Input_Select.setG0_Source_Polarity(1); // rising=0, overwritten
	//board->G0_Input_Select.setG0_Source_Polarity(0); // rising=0
    // Source_Polarity(1) = 1 << 15 = 0x8000
    // Source_Polarity(0) = 0 << 15 = 0x0000
    sreg[ S_G0_Input_Select + chan ] = inputselect;  // redundant
   
    //board->G0_Load_A.writeRegister(2); // initial delay
    lreg[ L_G0_Load_A + 2*chan ] = 2;

    //board->G0_Command.writeG0_Load(1);
    // Gn_Load(1) = 1 << 2 = 4, strobe bit, it goes away
    command = 0x104;  // Retains the Synchronized gate bit
    sreg[ S_G0_Command + chan ] = command;
    command = 0x100;  // load bit resets itself

	//board->G0_Load_A.writeRegister(4096); // high time
    lreg[ L_G0_Load_A + 2*chan ] = high;

	//board->G0_Mode.writeG0_Load_Source_Select(tTIO::tG0_Mode::tG0_Load_Source_Select::kG0_Load_Source_SelectLoad_B);
    // kG0_Load_Source_SelectLoad_B = 1
    // Load_Source_Select(1) = 1 << 7 = 0x0080, 1 bit
    sreg[ S_G0_Mode + chan ] = 0x0080;

	//board->G0_Load_B.writeRegister(4096); // low time
    lreg[ L_G0_Load_B + 2*chan ] = low;

	//board->G0_Command.setG0_Bank_Switch_Enable(tTIO::tG0_Command::kG0_Bank_Switch_EnableBank_Y);
    // Set the bank switch bit
    // kG0_Bank_Switch_EnableBank_Y = 1
    // Bank_Switch_Enable(1) = 1 << 12 = 0x1000
    // No flush with this write, don't write to the hardware yet
    command |= 0x1000;
    
    // High and low are reversed.
	//board->G0_Load_A.writeRegister(4096); // high time
    lreg[ L_G0_Load_A + 2*chan ] = high;
	//board->G0_Load_B.writeRegister(4096); // low time
    lreg[ L_G0_Load_B + 2*chan ] = low;

	//board->G0_Command.setG0_Bank_Switch_Mode(tTIO::tG0_Command::kG0_Bank_Switch_ModeSoftware);
    // kG0_Bank_Switch_ModeSoftware = 1
    // Bank_Switch_Mode(1) = 1 << 11 = 0x0800
    // No flush yet, don't write to the hardware yet
    command |= 0x0800;
    
	//MSeries.CTR.ApplicationRegisters
	//board->G0_Input_Select.setG0_Gate_Select_Load_Source(0);
    // setG0_Gate_Select_Load_Source(0) = 0 << 12 = 0
    // The external gate does not select A versus B.
    // Nothing to add to the inputselect variable.
    
	//board->G0_Mode.setG0_Reload_Source_Switching(tTIO::tG0_Mode::kG0_Reload_Source_SwitchingAlternate);
    //  kG0_Reload_Source_SwitchingAlternate = 1
    //  setG0_Reload_Source_Switching(1) = 1 << 15 = 0x8000
    mode = 0x8000;
	//board->G0_Mode.setG0_Loading_On_Gate(tTIO::tG0_Mode::kG0_Loading_On_GateNo_Reload);
    //  kG0_Loading_On_GateNo_Reload = 0
    //  setG0_Loading_On_Gate(0) = 0 << 14 = 0  (1 << 14 = 0x4000)
	//board->G0_Mode.setG0_Loading_On_TC(tTIO::tG0_Mode::kG0_Loading_On_TCReload_On_TC);
    //  kG0_Loading_On_TCReload_On_TC = 1
    //  setG0_Loading_On_TC(1) = 1 << 12 = 0x1000
    mode |= 0x1000;
	//board->G0_Mode.setG0_Gating_Mode (tTIO::tG0_Mode::kG0_Gating_ModeGating_Disabled);
    //  kG0_Gating_ModeGating_Disabled = 0
    //  setG0_Gating_Mode(0) = 0 << 0, bottom 2 bits
	//board->G0_Mode.setG0_Gate_On_Both_Edges (tTIO::tG0_Mode::kG0_Gate_On_Both_EdgesBoth_Edges_Disabled);
    //  kG0_Gate_On_Both_EdgesBoth_Edges_Disabled = 0
    //  setG0_Gate_On_Both_Edges(0) = 0 << 2 = 0
	//board->G0_Mode.setG0_Trigger_Mode_For_Edge_Gate(tTIO::tG0_Mode::kG0_Trigger_Mode_For_Edge_GateGate_Starts_TC_Stops);
    //  kG0_Trigger_Mode_For_Edge_GateGate_Starts_TC_Stops = 2
    //  setG0_Trigger_Mode_For_Edge_Gate(2) = 2 << 3 = 0x0010, 2 bits
    mode |= 0x0010;
	//board->G0_Mode.setG0_Stop_Mode(tTIO::tG0_Mode::kG0_Stop_ModeStop_On_Gate);
    //  kG0_Stop_ModeStop_On_Gate = 0
    //  setG0_Stop_Mode(0) = 0 << 5 = 0
	//board->G0_Mode.setG0_Counting_Once(tTIO::tG0_Mode::kG0_Counting_OnceNo_HW_Disarm);
    //  kG0_Counting_OnceNo_HW_Disarm = 0
    //  setG0_Counting_Once(0) = 0 << 10 = 0, 2 bits
    
    //board->G0_Second_Gate.setG0_Second_Gate_Gating_Mode(0);
    //  setG0_Second_Gate_Gating_Mode(0) = 0 << 0
    
    //board->G0_Input_Select.flush();
    sreg[ S_G0_Input_Select + chan ] = inputselect;
    
    //board->G0_Mode.flush();
    sreg[ S_G0_Mode + chan ] = mode;
    
	//board->G0_Second_Gate.flush();
    sreg[ S_G0_Second_Gate + chan ] = 0;

	//MSeries.CTR.UpDown.Registers
	//board->G0_Command.writeG0_Up_Down(tTIO::tG0_Command::kG0_Up_DownSoftware_Down); //kG0_Up_DownSoftware_Down
	//  kG0_Up_DownSoftware_Down = 0, nothing to add to command.
    // but this is a write command, so flush out the current command word
    sreg[ S_G0_Command + chan ] = command;
    
	//MSeries.CTR.OutputRegisters
	//board->G0_Mode.writeG0_Output_Mode(tTIO::tG0_Mode::kG0_Output_ModeToggle);
    //  kG0_Output_ModeToggle = 2
    //  writeG0_Output_Mode(2) = 2 << 8 = 0x0200
    mode |= 0x200;
    sreg[ S_G0_Mode + chan ] = mode;

	//board->G0_Input_Select.writeG0_Output_Polarity(0);
    // writeG0_Output_Polarity(0) = 0 << 14 = 0
    sreg[ S_G0_Input_Select + chan ] = inputselect;  // seems redundant

	//MSeries.CTR.BufferEnable	
    //boardM->G0_DMA_Config.setG0_DMA_Enable(0);
	//boardM->G0_DMA_Config.setG0_DMA_Int_Enable(0);
	//boardM->G0_DMA_Config.setG0_DMA_Write(0);
    //boardM->G0_DMA_Config.flush();
    // DMA_Config is board reg at the same addr as TIO DMA_Control reg
    sreg[ S_G0_DMA_Control + chan ] = 0;

	//boardM->G0_DMA_Config.writeG0_DMA_Reset(1);
    sreg[ S_G0_DMA_Control + chan ] = 0x8;  // bit clears itself

	//MSeries.PFI.LineDirection
	//This selects the actual output line
	//boardM->IO_Bidirection_Pin.writePFI12_Pin_Dir(tMSeries::tIO_Bidirection_Pin::tPFI12_Pin_Dir::kPFI12_Pin_DirOutput);
    // G0 -> PFI12
    // G1 -> PFI13
    //   kPFI12_Pin_DirOutput = 1, Input = 0
    //   writePFI12_Pin_Dir(1) = 1 << 12 = 0x1000
    //   in general writePFIn_Pin_Dir(1) = 1 << n, 0 <= n <= 15
    // Need to share an accumulated value for this with other blocks
    sreg[ S_IO_Bidirection_Pin ] = 0x3000;  // 1 << (12+chan);
	//boardM->PFI_Output_Select_5.writePFI12_Output_Select(tMSeries::tPFI_Output_Select_5::tPFI12_Output_Select::kPFI12_Output_SelectG0_Out);
    //sreg[ S_PFI_Output_Select_5 ] = 0; // 0x01cd;  // G0 out -> PFI12, G1 out -> PFI13

	//MSeries.CTR.EnableOutput
	//boardM->Analog_Trigger_Etc.setGPFO_0_Output_Enable(tMSeries::tAnalog_Trigger_Etc::tGPFO_0_Output_Enable::kGPFO_0_Output_EnableOutput);
    //  kGPFO_0_Output_EnableOutput = 1
    //  setGPFO_0_Output_Enable(1) = 1 << 14 = 0x4000
    //  setGPFO_1_Output_Enable(1) = 1 << 15 = 0x8000
	//boardM->Analog_Trigger_Etc.setGPFO_0_Output_Select(tMSeries::tAnalog_Trigger_Etc::tGPFO_0_Output_Select::kGPFO_0_Output_SelectG_OUT);
    //  kGPFO_0_Output_SelectG_OUT = 0
    //  setGPFO_0_Output_Select(0) = 0 << 11, 3 bits
    //  setGPFO_1_Output_Select(0) = 0 << 7, 1 bit
	//boardM->Analog_Trigger_Etc.flush();
    //sreg[ S_Analog_Trigger_Etc ] = 0xc000;  // affects both counters, needs work
    // Writing to this register appears to be irrelevant

	//MSeries.CTR.StartTriggerRegisters
	//board->G0_MSeries_Counting_Mode.writeG0_MSeries_HW_Arm_Enable(0);
    // Rewrites the register with the MSeries_Counting_Mode values!
    sreg[ S_G0_MSeries_Counting_Mode + chan ] = 0x4000;

	//MSeries.CTR.Hw.ConfigureDMA
	//boardM->G0_G1_Select.writeG0_DMA_Select(0);  
    breg[ B_G0_G1_Select ] = 0;
}

// PWM input arm and disarm routines
void niM_CtrReadArm(void *base, uint32_T chan )
{
    volatile uint16_T *sreg = (volatile uint16_T *)base;
    
    sreg[ S_G0_Command + chan ] = 0x0121;  // load and arm
    //sreg[ S_G0_Command + chan ] = 0x1901;  // arm, no load
}

void niM_CtrReadDisarm(void *base, uint32_T chan )
{
    volatile uint16_T *sreg = (volatile uint16_T *)base;
    
    sreg[ S_G0_Command + chan ] = 0x1910;
}
// Configure for measuring a period
// Need to pass in:
// trigger mode: level or edge
// trigger polarity: high or low active for level
//                   or high->rising edge, low->falling edge
// filter: sync or min pulse width
void niM_PeriodConfigure (void *base,
                          uint32_T chan,
                          uint32_T trigmode,
                          int devtype )
{
    volatile uint32_T *lreg = (volatile uint32_T *)base;
    volatile uint16_T *sreg = (volatile uint16_T *)base;
    uint16_T inputselect = 0;
    uint16_T command = 0;
    uint16_T mode = 0;

    //MSeries.CTR.Source
    //board->G0_Input_Select.setG0_Source_Select (30);  // 80 MHz
    //board->G0_Input_Select.setG0_Source_Polarity(0); // rising=0
	//board->G0_Input_Select.flush();
    // Gn_SourceSelect(30) = 30 << 2 = 0x1e << 2 = 0x78
    // Gn_Source_Polarity(0) = 0 << 15 = 0
	//MSeries.CTR.Gate
	//board->G0_Input_Select.setG0_Gate_Select(9); // PFI9  
	//board->G0_Input_Select.setG0_Output_Polarity(1); //active low=0   
	//board->G0_Input_Select.flush ();
    // if chan == 0 gate select = PFI9 = 0x0500
    // if chan == 1 gate select = PFI4 = 0x0280
    // setG0_OutputPolarity(1) = 1 << 14 = 0x4000
    inputselect = 0x4078;
    if( devtype == 27 ) // special for 6221/37
    {
        if( chan == 0 )
            inputselect |= 0x0100;
        else
            inputselect |= 0x0280;
    }else
    {
        if( chan == 0 )
            inputselect |= 0x0500;
        else
            inputselect |= 0x0280;
    }
    sreg[ S_G0_Input_Select + chan ] = inputselect;

    // Should use G0_MSeries_Counting_Mode
    // Already set to 0x4000 in reset
    //board->G0_MSeries_Counting_Mode.writeG0_Alternate_Synchronization(1);


	//MSeries.CTR.IncrementRegisters
	//board->G0_AutoIncrement.writeRegister(0);
    sreg[ S_G0_AutoIncrement + chan ] = 0;

	//MSeries.CTR.InitialCountRegisters
	//board->G0_Mode.writeG0_Load_Source_Select(tTIO::tG0_Mode::tG0_Load_Source_Select::kG0_Load_Source_SelectLoad_A);
    // kG0_Load_Source_SelectLoad_A = 0, SelectLoad_B = 1
    // Load_Source_Select(0) = 0 << 7, 1 bit
    sreg[ S_G0_Mode + chan ] = 0;
    
	//board->G0_Load_A.writeRegister(0);
    lreg[ L_G0_Load_A + 2*chan ] = 0;

	//board->G0_Command.writeG0_Load(1);
    // Gn_Load(1) = 1 << 2 = 4, strobe bit, it goes away
    command = 0x104;  // Retains the Synchronized gate bit
    sreg[ S_G0_Command + chan ] = command;
    command = 0x100;  // load bit resets itself

	//board->G0_Load_B.writeRegister(0);
    lreg[ L_G0_Load_B + 2*chan ] = 0;

    //board->G0_Load_A.writeRegister(0);
    lreg[ L_G0_Load_A + 2*chan ] = 0;   // Counters reset to 0 on gate end.

	//board->G0_Command.setG0_Bank_Switch_Enable(tTIO::tG0_Command::kG0_Bank_Switch_EnableBank_X);
    // kG0_Bank_Switch_EnableBank_X = 0
    // setG0_Bank_Switch_Enable(0) = 0 << 12 = 0 (0x1000)
    //board->G0_Command.setG0_Bank_Switch_Mode(tTIO::tG0_Command::kG0_Bank_Switch_ModeGate);
    // kG0_Bank_Switch_ModeGate = 0
    // setG0_Bank_Switch_Mode(0) = 0 << 11 = 0 (0x800)
    //board->G0_Command.flush();
    sreg[ S_G0_Command + chan ] = command;  // Just the Synchronized gate bit, choose bank X

	//MSeries.CTR.ApplicationRegisters
	//board->G0_Input_Select.setG0_Gate_Select_Load_Source(0);
    // sets no new bits in inputselect
	//board->G0_Mode.setG0_Reload_Source_Switching(tTIO::tG0_Mode::kG0_Reload_Source_SwitchingUse_Same);
    // kG0_Reload_Source_SwitchingUse_Same = 0
    // setG0_Reload_Source_Switching(0) = 0 << 15 (0x8000)
	//board->G0_Mode.setG0_Loading_On_Gate(tTIO::tG0_Mode::kG0_Loading_On_GateReload_On_Stop_Gate);
    // kG0_Loading_On_GateReload_On_Stop_Gate = 1
    // setG0_Loading_On_Gate(1) = 1 << 14 = 0x4000
    mode = 0x4000;
	//board->G0_Mode.setG0_Loading_On_TC(tTIO::tG0_Mode::kG0_Loading_On_TCRollover_On_TC);
    // kG0_Loading_On_TCRollover_On_TC = 0
    // setG0_Loading_On_TC(0) = 0 << 12 = 0 (0x1000)
	//board->G0_Mode.setG0_Gating_Mode (tTIO::tG0_Mode::kG0_Gating_ModeEdge_Gating_Active_High);
    // kG0_Gating_ModeEdge_Gating_Active_High = 2 (2 bits)
    // setG0_Gating_Mode(2) = 2 << 0 = 0x0002  -- Edge gating
    // setG0_Gating_Mode(1) = 1 << 0 = 0x0001  -- Level gating
    switch( trigmode )
    {
        case 0:  // Gate high
            mode |= 0x0001;  // level gating, active high gate
            break;
        case 1:  // Gate low
            mode |= 0x2001;  // level gating, active low gate
            break;
        case 2:
            mode |= 0x0002;  // rising edge gating
            break;
        case 3:
            mode |= 0x0003;  // falling edge gating
            break;
    }
    
	//board->G0_Mode.setG0_Gate_On_Both_Edges (tTIO::tG0_Mode::kG0_Gate_On_Both_EdgesBoth_Edges_Disabled);
    // kG0_Gate_On_Both_EdgesBoth_Edges_Disabled = 0
    // setG0_Gate_On_Both_Edges(0) = 0 << 2 = 0 (0x0004)
	//board->G0_Mode.setG0_Trigger_Mode_For_Edge_Gate(tTIO::tG0_Mode::kG0_Trigger_Mode_For_Edge_GateGate_Does_Not_Stop);
    // kG0_Trigger_Mode_For_Edge_GateGate_Does_Not_Stop = 3
    // setG0_Trigger_Mode_For_Edge_Gate(3) = 3 << 3 = 0x0018
    mode |= 0x0018;
	//board->G0_Mode.setG0_Stop_Mode(tTIO::tG0_Mode::kG0_Stop_ModeStop_On_Gate);
    // kG0_Stop_ModeStop_On_Gate = 0, 2bits
    // setG0_Stop_Mode(0) = 0 << 5 = 0 (0x0060)
	//board->G0_Mode.setG0_Counting_Once(tTIO::tG0_Mode::kG0_Counting_OnceNo_HW_Disarm);
    // kG0_Counting_OnceNo_HW_Disarm = 0 (2 bits) (keep running, just latch on gate)
    // setG0_Counting_Once(0) = 0 << 10 = 0 (0x0c00)
	//board->G0_Second_Gate.setG0_Second_Gate_Gating_Mode(0);
    // setG0_Second_Gate_Gating_Mode(0) = 0 << 0 = 0 (0x0001)

    //board->G0_Input_Select.flush();
    sreg[ S_G0_Input_Select + chan ] = inputselect;
	//board->G0_Mode.flush();
    sreg[ S_G0_Mode + chan ] = mode;
	//board->G0_Second_Gate.flush();
    sreg[ S_G0_Second_Gate + chan ] = 0;
    
	//MSeries.CTR.UpDown.Registers
	//board->G0_Command.writeG0_Up_Down(tTIO::tG0_Command::kG0_Up_DownSoftware_Up); //kG0_Up_DownSoftware_Down
	// kG0_Up_DownSoftware_Up = 1, 2 bits
    // writeG0_Up_Down(1) = 1 << 5 = 0x0020
    command |= 0x0020;
    sreg[ S_G0_Command + chan ] = command;  // Add the UP bit

	//MSeries.CTR.OutputRegisters
	//board->G0_Mode.writeG0_Output_Mode(tTIO::tG0_Mode::kG0_Output_ModePulse);
    // kG0_Output_ModePulse = 1 (2 bits)
    // writeG0_Output_Mode(1) = 1 << 8 = 0x0100  (field mask is 0x0300)
    mode |= 0x0100;
    sreg[ S_G0_Mode + chan ] = mode;
    
	//board->G0_Input_Select.writeG0_Output_Polarity(0);
    // writeG0_Output_Polarity(0) = 0 << 14 = 0  (0x4000 bit)
    inputselect &= ~0x4000;
    sreg[ S_G0_Input_Select + chan ] = inputselect;

	//MSeries.CTR.BufferEnable
	//boardM->G0_DMA_Config.writeG0_DMA_Reset(1);

	//boardM->G0_DMA_Config.setG0_DMA_Write(0);  
	//boardM->G0_DMA_Config.setG0_DMA_Int_Enable(1);  // No, not using it
    //boardM->G0_DMA_Config.setG0_DMA_Enable(1);   // No, not using DMA
    //boardM->G0_DMA_Config.flush();
    sreg[ S_G0_DMA_Control + chan ] = 0;    // no enables.
    sreg[ S_G0_DMA_Control + chan ] = 0x8;  // bit clears itself

    // Shouldn't use g0_Counting_Mode, incorrect bits
    // Should use G0_MSeries_Counting_Mode
	//board->G0_Counting_Mode.setG0_Encoder_Counting_Mode(0);
	//board->G0_Counting_Mode.setG0_Alternate_Synchronization(1);
	//board->G0_Counting_Mode.flush();

	//MSeries.CTR.EnableOutput
	//boardM->Analog_Trigger_Etc.setGPFO_0_Output_Enable(tMSeries::tAnalog_Trigger_Etc::tGPFO_0_Output_Enable::kGPFO_0_Output_EnableInput);
    //  kGPFO_0_Output_EnableInput = 0
	//boardM->Analog_Trigger_Etc.setGPFO_0_Output_Select(tMSeries::tAnalog_Trigger_Etc::tGPFO_0_Output_Select::kGPFO_0_Output_SelectG_OUT);
    //  kGPFO_0_Output_SelectG_OUT = 0
	//boardM->Analog_Trigger_Etc.flush();
    //sreg[ S_Analog_Trigger_Etc ] = 0x0000;  // affects both counters, needs work
    // Writing to this register appears to be irrelevant

	//MSeries.CTR.StartTriggerRegisters
	//board->G0_MSeries_Counting_Mode.writeG0_MSeries_HW_Arm_Enable(0);	    
    sreg[ S_G0_MSeries_Counting_Mode + chan ] = 0x4000;
}

/* reset all the registers which are needed to perform the operation */
void niM_Counter_Reset_All(void *base, uint32_T chan )
{
    volatile uint16_T *sreg = (volatile uint16_T *)base;

    // Reset GPCTn, do channel based registers, not just G0
    //board->G01_Joint_Reset.writeG0_Reset(1);
    sreg[ S_G01_Joint_Reset ] = 1 << (2+chan);
    
    //board->G0_Mode.writeRegister(0);
    sreg[ S_G0_Mode + chan ] = 0;
    
    //board->G0_Command.writeRegister(0);
    sreg[ S_G0_Command + chan ] = 0;
    
    //board->G0_Input_Select.writeRegister(0);
    sreg[ S_G0_Input_Select + chan ] = 0;
    
    //board->G0_AutoIncrement.writeRegister(0);
    sreg[ S_G0_AutoIncrement + chan ] = 0;
    
    //board->Interrupt_G0_Enable.setG0_TC_Interrupt_Enable(0);
    //board->Interrupt_G0_Enable.setG0_Gate_Interrupt_Enable(0);
    //board->Interrupt_G0_Enable.flush();
    sreg[ S_Interrupt_G0_Enable + 2*chan ] = 0;
    
    //board->G0_Command.writeG0_Synchronized_Gate(tTIO::tG0_Command::kG0_Synchronized_GateEnabled);
    // Synchronized_Gate(1) = 1 << 8 = 0x100
    sreg[ S_G0_Command + chan ] = 0x100;
    
    //board->Interrupt_G0_Ack.setG0_Gate_Error_Confirm(1);
    //board->Interrupt_G0_Ack.setG0_TC_Error_Confirm(1);
    //board->Interrupt_G0_Ack.setG0_TC_Interrupt_Ack(1);
    //board->Interrupt_G0_Ack.setG0_Gate_Interrupt_Ack(1);
    //board->Interrupt_G0_Ack.flush();
    // Gate_Error_Confirm(1) = 1 << 5 = 0x20
    // TC_Error_Confirm(1) = 1 << 6 = 0x40
    // TC_Interrupt_Ack = 1 << 14 = 0x4000
    // Gate_Interrupt_Ack = 1 << 15 = 0x8000
    sreg[ S_Interrupt_G0_Ack + chan ] = 0xc060;
    
    //board->G0_AutoIncrement.write(0);
    sreg[ S_G0_AutoIncrement + chan ] = 0;
    
    //board->G0_MSeries_Counting_Mode.setG0_MSeries_Alternate_Synchronization (1); // 1 if using 80 MHz 
    //board->G0_MSeries_Counting_Mode.setG0_MSeries_Index_Enable (0);
    //board->G0_MSeries_Counting_Mode.setG0_MSeries_Prescale (0);
    //board->G0_MSeries_Counting_Mode.setG0_MSeries_Encoder_Counting_Mode (0);
    //board->G0_MSeries_Counting_Mode.flush();
    // Alternate_Synchronization(1) = 1 << 14 = 0x4000
    // Index_Enable(0) = 0 << 4 = 0
    // Prescale(0) = 0 << 13 = 0
    // Encoder_Counting_Mode(0) = 0 << 0, 3 bits = 0
    sreg[ S_G0_MSeries_Counting_Mode + chan ] = 0x4000;
    
    //board->G0_Second_Gate.setG0_Second_Gate_Select(0);
    //board->G0_Second_Gate.setG0_Second_Gate_Polarity(0);
    //board->G0_Second_Gate.flush();
    // Second_Gate_Select(0) = 5 bits, 0 << 7 = 0
    // Second_Gate_Polarity(0) = 0 << 13 = 0
    sreg[ S_G0_Second_Gate + chan ] = 0;
}

// private type definitions

typedef union {
    uint8_T  b[4];
    float f;
}tEepromF32;

typedef struct
{
    uint32_T order;
    float c[4];
}tMode;

typedef struct
{
    float gain;
    float offset;
}tInterval; 


// private eeprom map constants

#define  kCalibrationAreaOffset  24

#define  kHeaderSize             16

#define  kAiModeBlockStart       kHeaderSize  
#define  kAiModeBlockSize        17
#define  kAiMaxNumberOfModes     4
#define  kAiModeBlockEnd         (kAiModeBlockStart + kAiModeBlockSize*kAiMaxNumberOfModes)

#define  kAiIntervalBlockStart   kAiModeBlockEnd
#define  kAiIntervalBlockSize    8
#define  kAiMaxNumberOfIntervals 7
#define  kAiIntervalBlockEnd     (kAiIntervalBlockStart + kAiIntervalBlockSize*kAiMaxNumberOfIntervals)

#define  kAoModeBlockStart       kAiIntervalBlockEnd
#define  kAoModeBlockSize        17
#define  kAoMaxNumberOfModes     1
#define  kAoModeBlockEnd         (kAoModeBlockStart + kAoModeBlockSize*kAoMaxNumberOfModes)

#define  kAoIntervalBlockStart   kAoModeBlockEnd 
#define  kAoMaxNumberOfIntervals 4
#define  kAoIntervalBlockSize    8
#define  kAoIntervalBlockEnd     (kAoIntervalBlockStart + kAoIntervalBlockSize*kAoMaxNumberOfIntervals)

#define  kAiChannelBlockSize     (kAiModeBlockSize*kAiMaxNumberOfModes + kAiIntervalBlockSize*kAiMaxNumberOfIntervals)
#define  kAiMaxNumberOfChannels  1

#define  kAoChannelBlockSize     (kAoModeBlockSize*kAoMaxNumberOfModes + kAoIntervalBlockSize*kAoMaxNumberOfIntervals)
#define  kAoMaxNumberOfChannels  4


static float getF32FromEeprom (const uint8_T *eepromMemory, uint32_T offset)
{
    tEepromF32 value;
    
    value.b[3] = eepromMemory[offset++];
    value.b[2] = eepromMemory[offset++];
    value.b[1] = eepromMemory[offset++];
    value.b[0] = eepromMemory[offset++];
    
    return value.f;
}

static uint32_T getCalibrationAreaOffset (const uint8_T *eepromMemory)
{
    return (eepromMemory[kCalibrationAreaOffset] << 8) | eepromMemory[kCalibrationAreaOffset+1];
}

//
// aoGetScalingCoefficients --
//
// modeIdx
//    ignored - AO does not use the mode constants
//
// intervalIdx
//    0 -> 10V reference
//    1 -> 5V reference
//    2 -> 2V reference
//    3 -> 1V reference
//
// channel
//   dac number
//
void niM_aoGetScalingCoefficients (const uint8_T *eepromMemory,
                                   uint32_T intervalIdx,
                                   uint32_T modeIdx,
                                   uint32_T channel,
                                   tScalingCoefficients *scaling)
{
    uint32_T calOffset = 0;
    tInterval interval; 
    uint32_T intervalOffset;

    calOffset = getCalibrationAreaOffset (eepromMemory); 
    
    intervalOffset = calOffset + kAoIntervalBlockStart + (intervalIdx*kAoIntervalBlockSize) + (channel*kAoChannelBlockSize);

    interval.gain   = getF32FromEeprom (eepromMemory, intervalOffset);
    intervalOffset += 4;
    interval.offset = getF32FromEeprom (eepromMemory, intervalOffset);

    scaling->order = 1; 
    scaling->c[0] = interval.offset;
    scaling->c[1] = interval.gain;

    return; 
}

//
// aiGetScalingCoefficients --
//
// modeIdx
//    0 -> default
//
// intervalIdx
//    0 -> +/- 10V
//    1 -> +/- 5V
//    2 -> +/- 2V
//    3 -> +/- 1V
//    4 -> +/- 500mV
//    5 -> +/- 200mV
//    6 -> +/- 100mV
//
// channel
//    ignored - all channels use the same ADC
//
void niM_aiGetScalingCoefficients (const uint8_T *eepromMemory,
                                   uint32_T intervalIdx,
                                   uint32_T modeIdx,
                                   uint32_T channel,
                                   tScalingCoefficients *scaling)
{
    uint32_T calOffset = 0;
    uint32_T modeOffset;
    tMode mode;
    tInterval interval; 
    uint32_T intervalOffset;
    uint32_T i;

    calOffset = getCalibrationAreaOffset (eepromMemory);

    modeOffset = calOffset + kAiModeBlockStart + (modeIdx*kAiModeBlockSize);

    mode.order = eepromMemory[modeOffset++];
    mode.c[0] = getF32FromEeprom (eepromMemory, modeOffset);
    modeOffset += 4;
    mode.c[1] = getF32FromEeprom (eepromMemory, modeOffset);
    modeOffset += 4;
    mode.c[2] = getF32FromEeprom (eepromMemory, modeOffset);
    modeOffset += 4;
    mode.c[3] = getF32FromEeprom (eepromMemory, modeOffset);

    intervalOffset = calOffset + kAiIntervalBlockStart + (intervalIdx*kAiIntervalBlockSize);
    
    interval.gain = getF32FromEeprom (eepromMemory, intervalOffset);
    intervalOffset += 4;
    interval.offset = getF32FromEeprom (eepromMemory, intervalOffset);

    scaling->order = mode.order;
    
    for ( i=0 ; i <= mode.order ; i++)
    {
        scaling->c[i] = mode.c[i] * interval.gain;
        if (i == 0)
            scaling->c[i] = scaling->c[i] + interval.offset;
    }
    
    return; 
}

void niM_aiPolynomialScaler (const int32_T *raw,
                             float *scaled,
                             const tScalingCoefficients *scaling)
{
    int32_T j;

    *scaled = scaling->c[scaling->order];

    for( j = scaling->order-1 ; j >= 0 ; j--)
    {
        (*scaled) *= (float)*raw;
        (*scaled) += scaling->c[j];
    } 
    
    return; 
}

int32_T niM_aoLinearScaler( const float scaled, const tScalingCoefficients *scaling)
{
    //printf("scaling = 0x%x, scaled = %f\n", scaling, scaled );
    //printf("%d, %f %f\n", scaling->order, scaling->c[0], scaling->c[1] );
    //printf("%f => %f\n", scaled, (scaled) * scaling->c[1] + scaling->c[0] );

    return (int32_T)((scaled) * scaling->c[1] + scaling->c[0]);
}

void niM_eepromReadMSeries(void *bar0,
                           void *bar1,
                           uint8_T *eepromBuffer,
                           uint32_T bufferSize)
{
#define kStartCalEEPROM  1024
#define kEndCalEEPROM    kStartCalEEPROM + 1024
    
    uint32_T _iowcr1Initial;
    uint32_T _iowbsr1Initial;
    uint32_T _iodwbsrInitial; 
    uint32_T _bar1Value;

    volatile uint32_T *lbar0 = (volatile uint32_T *)bar0;
    volatile uint8_T  *bbar1 = (volatile uint8_T *)bar1;
    uint32_T wSize;
    uint32_T i;

    // ---- Open EEPROM 
    
    _iodwbsrInitial = lbar0[ 0xc0 / 4 ];  //bar0.read32 (0xC0);
    lbar0[ 0xc0 / 4 ] = 0;  // bar0.write32 (0xC0, 0x00);

    _iowbsr1Initial = lbar0[ 0xc4 / 4 ];  //bar0.read32 (0xC4);
    _bar1Value      = lbar0[ 0x314 / 4 ]; //bar0.read32 (0x314);

    wSize  = 10;
    lbar0[ 0xc4 / 4 ] = (0x80+wSize) | _bar1Value; // bar0.write32 (0xC4, ((0x80+wSize) | _bar1Value));

    _iowcr1Initial = lbar0[ 0xf4 / 4 ];  // bar0.read32(0xF4);

    lbar0[ 0xf4 / 4 ] = 0;  // bar0.write32 (0xF4, 0x00);
    lbar0[ 0x30 / 4 ] = 0xf;  // bar0.write32 (0x30, 0xF);
        
    // ---- Read EEPROM
    
    for( i = 0; (i < (kEndCalEEPROM - kStartCalEEPROM)) && (i < bufferSize); i++)
    {
        eepromBuffer[i] = bbar1[ i + kStartCalEEPROM ];  // bar1.read8(i + kStartCalEEPROM);
    }

    // ---- Close EEPROM
    lbar0[ 0xc4 / 4 ] = _iowbsr1Initial;    // bar0.write32 (0xC4, _iowbsr1Initial);
    lbar0[ 0xc0 / 4 ] = _iodwbsrInitial;    // bar0.write32 (0xC0, _iodwbsrInitial);
    lbar0[ 0xf4 / 4 ] = _iowcr1Initial;     // bar0.write32 (0xF4, _iowcr1Initial);
    lbar0[ 0x30 / 4 ] = 0;                  // bar0.write32 (0x30, 0x00);
}

void niM_AO_Setup( void *base )
{
    volatile uint32_T *lreg = (volatile uint32_T *)base;
    volatile unsigned short *sreg = (volatile unsigned short *)base;
    volatile unsigned char  *breg = (volatile unsigned char *)base;
    uint32_T i;

    // Do AO Reset
    niM_configureTimebase( base );
    niM_pllReset( base );
    niM_analogTriggerReset( base );

    // aoReset
    sreg[S_Joint_Reset] = 0x0002;   // AO reset bit, strobe bit
    sreg[S_Joint_Reset] = 0x0020;   // AO configuration start, sticky
    sreg[S_AO_Command_1] = 0x2000;  // AO Disarm bit, strobe bit

    sreg[S_Interrupt_B_Enable] = 0; // disable all interrupts

    sreg[S_AO_Personal] = 0x10;     // source UC_TC

    sreg[S_Interrupt_B_Ack] = 0xffff;  // clear all

    //sreg[S_Joint_Reset] = 0x0200;  // AO configuration end, strobe

    // aoPersonalize
    //sreg[S_Joint_Reset] = 0x0020;   // AO configuration start, sticky
    
    // AO_Number_Of_DAC_Packages                       = 0x4000
    // AO_TMRDACWR_Pulse_WidthAbout_3_TIMEBASE_Periods = 0x1000
    // Update Pulse Width    = 0x0020
    // Update Pulse Timebase = 0 // 0x0040
    // BC Source Select      = 0x0010
    sreg[S_AO_Personal] = 0x5030;

    sreg[S_AO_Output_Control] = 0;
    sreg[S_AO_START_Select] = 0;

    // AO_Continuous = 1
    sreg[S_AO_Mode_1] = 2;

    // AO_START1_Select = 0
    // AO_START1_Polarity = 0
    // AO_START1_Edge = 1 = 0x20
    // AO_START1_Sync = 1 = 0x40
    sreg[S_AO_Trigger_Select] = 0x60;

    // AO_Last_Gate_Disable = 0x0001 
    sreg[S_AO_Mode_3] = 1;

    lreg[L_AO_BC_Load_A] = 1;
    sreg[S_AO_Command_1] = 0x20;  // AO_BC_Load = 1 = 0x20
    sreg[S_AO_Command_2] = 0;

    //sreg[S_Joint_Reset] = 0x0200;  // AO configuration end, strobe

    // aoResetWaveformChannels
    for( i = 0 ; i < 4 ; i++ )
    {
        //board->AO_Config_Bank[i].writeAO_Update_Mode (tMSeries::tAO_Config_Bank::kAO_Update_ModeImmediate);  = 0 << 6, single bit
        breg[ B_AO_Config_Bank0 + 0x4*i ] = 0;
    }
    for( i = 0 ; i < 16 ; i++ )
    {
        //board->AO_Waveform_Order[i].writeRegister (0xF);
        breg[ B_AO_Waveform_Order0 + 0x4*i ] = 0xf;
    }

    // aoClearFIFO
    //board->AO_FIFO_Clear.writeRegister (1);
    sreg[ S_AO_FIFO_Clear ] = 1;

    // unground AO reference
    //board->AO_Calibration.writeAO_RefGround (kFalse);
    breg[ B_AO_Calibration ] = 0;

    sreg[S_Joint_Reset] = 0x0200;   // AO configuration end, strobe
    // End AO Reset

}

// These 8 addresses aren't all contiguous, so I use an array of them
// Can't compute based on the first one.
static uint32_T Static_AI_Control[8] =
{
    B_Static_AI_Control0, 
    B_Static_AI_Control1, 
    B_Static_AI_Control2, 
    B_Static_AI_Control3, 
    B_Static_AI_Control4, 
    B_Static_AI_Control5, 
    B_Static_AI_Control6, 
    B_Static_AI_Control7, 
};

void niM_aoConfigureDAC( void *base,
                         uint32_T chan,
                         uint32_T order,
                         uint32_T reference )
{
    volatile unsigned char  *breg = (volatile unsigned char *)base;

    // 0x80 = bipolar polarity, always turned on
    // 0x40 = timed/immediate, leave off for immediate update
    // 0x38 = 3 bits for reference voltage, 0 is +-10, 1 is +- 5, 2 is +-2 and 3 is +-1
    // 0x07 = 3 bits for offset, no doc for the values, leave it 0.

    // chan is zero based here.
    breg[ B_AO_Config_Bank0 + 4*chan ] = 0x80 | ((reference & 7) << 3);

    breg[ Static_AI_Control[chan+4] ] = 0;  // reference attenuation off

    breg[ B_AO_Waveform_Order0 + 4*chan ] = order;
}
