function [maskdisplay, outmask, reset, initial ] = ...
    mpwmnipcim(phase, chan, slot, boardType, func, arm)

% Copyright 2003-2011 The MathWorks, Inc.

% Cross block check for PFI pins on PWM out, PWM in and Encoder blocks
% pwmoutnipci62xx  PWM output
% pwminpnipci62xx  PWM input
% pfioutnipci62xx  PFI DIO output
% pfiinpnipci62xx  PFI DIO input
% encinpnipci62xx  Encoder input

  if phase == 1  % InitFcn call, only phase is defined
      ctr = evalin( 'base', get_param(gcb, 'channel'));
      masktype = get_param( gcb, 'MaskType' );
      maskbase = masktype(7:end);  % just the 'nipci62xx' part
      
      if strcmp( maskbase(10:end), '37' )
          altpins = 1;
      else
          altpins = 0;
      end

      pwminm  = ['pwminp',maskbase];
      pwmoutm = ['pwmout',maskbase];
      pfiinm  = ['pfiinp',maskbase];
      pfioutm = ['pfiout',maskbase];
      encinpm = ['encinp',maskbase];

      myslot = evalin( 'base', get_param(gcb, 'slot') );
      pfiincnt = zeros(1,16);
      pwmoutcnt = zeros(1,16);
      pfioutcnt = zeros(1,16);
      % Make it an error to have both input and output using the same PFI.

      % Look for blocks with these masktypes and qualify with the slot
      % number.  Gather the PFI assignments and verify that there is no
      % conflict.  Then prepare the hardware specific values that the
      % drivers will write to select direction and pin assignment.

      pwmin = find_system( bdroot, ...
          'FollowLinks', 'on', ...
          'LookUnderMasks', 'all', ...
          'MaskType', pwminm );

      pwmout = find_system( bdroot, ...
          'FollowLinks', 'on', ...
          'LookUnderMasks', 'all', ...
          'MaskType', pwmoutm );

      pfiin = find_system( bdroot, ...
          'FollowLinks', 'on', ...
          'LookUnderMasks', 'all', ...
          'MaskType', pfiinm );

      pfiout = find_system( bdroot, ...
          'FollowLinks', 'on', ...
          'LookUnderMasks', 'all', ...
          'MaskType', pfioutm );

      encinp = find_system( bdroot, ...
          'FollowLinks', 'on', ...
          'LookUnderMasks', 'all', ...
          'MaskType', encinpm );

      matching = zeros(1,2);  % will contain all blocks for the
      % same board.
      % check pwm input blocks for board and channel
      for i = 1 : length(pwmin)
          tmpslot = evalin( 'base', get_param(pwmin{i}, 'slot'));
          if isequal(tmpslot, myslot)
              tmpchan = evalin( 'base', get_param(pwmin{i}, 'Channel'));
              matching(tmpchan+1) = matching(tmpchan+1) + 1;
              if altpins == 1  % 6221/37 special
                  if tmpchan == 0
                      pfi = 1;  % PFI are 0 based
                  else
                      pfi = 4;
                  end
              else
                  if tmpchan == 0
                      pfi = 9;  % PFI are 0 based
                  else
                      pfi = 4;
                  end
              end
              % array is 1 based, add 1
              pfiincnt(pfi+1) = pfiincnt(pfi+1) + 1;
          end
      end

      % check pwm output blocks for board and channel
      for i = 1 : length(pwmout)
          tmpslot = evalin( 'base', get_param(pwmout{i}, 'slot'));
          if isequal(tmpslot, myslot)
              tmpchan = evalin( 'base', get_param(pwmout{i}, 'Channel'));
              matching(tmpchan+1) = matching(tmpchan+1) + 1;
              if altpins == 1  % 6221/37 special
                  if tmpchan == 0
                      pfi = 6;  % PFI are 0 based
                  else
                      pfi = 7;
                  end
              else
                  if tmpchan == 0
                      pfi = 12;
                  else
                      pfi = 13;
                  end
              end
              pwmoutcnt(pfi+1) = pwmoutcnt(pfi+1) + 1;
          end
      end

      % check encoder input blocks for board and channel
      for i = 1 : length(encinp)
          tmpslot = evalin( 'base', get_param(encinp{i}, 'slot'));
          if isequal(tmpslot, myslot)
              tmpchan = evalin( 'base', get_param(encinp{i}, 'Channel'));
              matching(tmpchan+1) = matching(tmpchan+1) + 1;
              % Index with PFI+1, keep the PFI number explicitly visible
              % even while indexing into the arrays with 1 based indexing.
              if altpins == 1  % 6221/37 special
                  if tmpchan == 0
                      pfiincnt(0+1) = pfiincnt(0+1) + 1;   % A
                      pfiincnt(2+1) = pfiincnt(2+1) + 1;   % B
                      pfiincnt(1+1) = pfiincnt(1+1) + 1;   % Z
                  else
                      pfiincnt(3+1) = pfiincnt(3+1) + 1;   % A
                      pfiincnt(4+1) = pfiincnt(4+1) + 1;   % B
                      pfiincnt(5+1) = pfiincnt(5+1) + 1;   % Z
                  end
              else
                  if tmpchan == 0
                      pfiincnt(8+1) = pfiincnt(8+1) + 1;   % A
                      pfiincnt(9+1) = pfiincnt(9+1) + 1;   % B
                      pfiincnt(10+1) = pfiincnt(10+1) + 1; % Z
                  else
                      pfiincnt(3+1) = pfiincnt(3+1) + 1;   % A
                      pfiincnt(4+1) = pfiincnt(4+1) + 1;   % B
                      pfiincnt(11+1) = pfiincnt(11+1) + 1; % Z
                  end
              end
          end
      end

      maskfunc = masktype(1:6);  % just the function type part
      if( strcmp( maskfunc, 'pwminp' ) ...
              || strcmp( maskfunc, 'pwmout' ) ...
              || strcmp( maskfunc, 'encinp' ) )
          if matching(ctr+1) ~= 1
              error(message('xPCTarget:PCIMCounters:DupBlock'));
          end
      end

      % Next check the PFI DIO in and out blocks to make sure they're not
      % trying to use the same PFI as one of the counters.
      % check pwm output blocks for board and channel
      for i = 1 : length(pfiin)
          tmpslot = evalin( 'base', get_param(pfiin{i}, 'slot'));
          if isequal(tmpslot, myslot)
              tmpchan = evalin( 'base', get_param(pfiin{i}, 'Channel'));
              if any( tmpchan < 0 ) || any( tmpchan > 15 )
                  error(message('xPCTarget:PCIMCounters:DIValue'));
              end
              for cnt = 1:length(tmpchan)
                  pfiincnt(tmpchan(cnt)+1) = pfiincnt(tmpchan(cnt)+1) + 1;
              end
          end
      end

      for i = 1 : length(pfiout)
          tmpslot = evalin( 'base', get_param(pfiout{i}, 'slot'));
          if isequal(tmpslot, myslot)
              tmpchan = evalin( 'base', get_param(pfiout{i}, 'Channel'));
              if any( tmpchan < 0 ) || any( tmpchan > 15 )
                  error(message('xPCTarget:PCIMCounters:DOValue'));
              end
              for cnt = 1:length(tmpchan)
                  pfioutcnt(tmpchan(cnt)+1) = pfioutcnt(tmpchan(cnt)+1) + 1;
              end
          end
      end

      dircheck = ( pfiincnt & pfioutcnt );
      ncheck = 0;
      if any(dircheck)
          msg = 'PFI [';
          for idx = 1 : 16
              if dircheck(idx) == 1
                  msg = [msg, num2str(idx-1),' ']; %#ok
                  ncheck = ncheck + 1;
              end
          end
          msg = [msg,']'];
          error(message('xPCTarget:PCIMCounters:PFIDup', msg));
      end

      % Can't let a PWM output and a PFI DO use the same PFI, it won't work.
      outcheck = ( pwmoutcnt & pfioutcnt );
      ncheck = 0;
      if any( outcheck )
          msg = 'PFI [';
          for idx = 1 : 16
              if (pfioutcnt(idx) > 0) && (pwmoutcnt(idx) > 0 )
                  msg = [msg, num2str(idx-1),' ']; %#ok
                  ncheck = ncheck + 1;
              end
          end
          msg = [msg,']'];
        
          error(message('xPCTarget:PCIMCounters:PWMPFI', msg));
      end

      % Save the PFI output assignment vector.  We need to explicitly
      % tell each driver to set the Bidirection register using this info.
      % Prepare a 16 position value, a 1 indicates output,
      % 0 (default) is input.
      pfioutmask = uint16(0);
      for idx = 1:16
          if (pfioutcnt(idx) + pwmoutcnt(idx)) >= 1
              pfioutmask = bitset( pfioutmask, idx );
          end
      end
      set_param( gcb, 'UserData', double(pfioutmask) );
  end

  if phase == 2
      % Pass the PFI pin directions back out for the driver.
      % pass it as a double, convert back to uint16 in the driver.
      outmask = get_param( gcb, 'UserData' );

      if prod(length(slot)) > 2
          error(message('xPCTarget:PCIMCounters:Slot'));
      end

      switch boardType
          case 20
              name = 'PCI-6220';
          case 21
              name = 'PCI-6221';
          case 24
              name = 'PCI-6224';
          case 25
              name = 'PCI-6225';
          case 27
              name = 'PCI-6221/37 pin';
          case 29
              name = 'PCI-6229';
          case 50
              name = 'PCI-6250';
          case 51
              name = 'PCI-6251';
          case 54
              name = 'PCI-6254';
          case 55
              name = 'PCI-6255';
          case 59
              name = 'PCI-6259';
          case 80
              name = 'PCI-6280';
          case 81
              name = 'PCI-6281';
          case 84
              name = 'PCI-6284';
          case 89
              name = 'PCI-6289';
          otherwise
              error(message('xPCTarget:NIMSeries:Unknown', boardType));
      end
      switch func
          case 1
              maskdisplay = sprintf( 'disp(''%s\\nNational Instr.\\nPWM Gen'');', name );
              maskdisplay = sprintf('%s port_label(''input'', 1, ''H%d'');port_label(''input'', 2, ''L%d'');', maskdisplay, chan, chan );
              if arm
                  maskdisplay = sprintf('%s port_label(''input'', 3, ''A%d'');', maskdisplay, chan );
              end
          case 2
              maskdisplay = sprintf( 'disp(''%s\\nNational Instr.\\nPWM Measure'');', name );
              maskdisplay = sprintf('%s port_label(''output'', 1, ''C%d'');', maskdisplay, chan );
          case 3
              maskdisplay = sprintf( 'disp(''%s\\nNational Instr.\\nEncoder'');', name );
              maskdisplay = sprintf('%s port_label(''output'', 1, ''%d'');', maskdisplay, chan );
          case 4
              maskdisplay = sprintf( 'disp(''%s\\nNational Instr.\\nPFI Digital Input'');', name );
              % multiple outputs
              if any( chan < 0 ) || any( chan > 15 )
                  error(message('xPCTarget:PCIMCounters:DIValue'));
              end
              for idx = 1:length(chan)
                  maskdisplay = sprintf('%s port_label(''output'', %d, ''%d'');', maskdisplay, idx, chan(idx) );
              end
              
              reset = 0;
              initial = 0;

          case 5
              maskdisplay = sprintf( 'disp(''%s\\nNational Instr.\\nPFI Digital Output'');', name );
              % multiple inputs
              for idx = 1:length(chan)
                  maskdisplay = sprintf('%s port_label(''input'', %d, ''%d'');', maskdisplay, idx, chan(idx) );
              end

              % Fix up reset and initial value vectors.
              reset = evalin( 'base', get_param(gcb, 'reset') );
              initial = evalin( 'base', get_param(gcb, 'initial') );

              lchan = length(chan);
              if length(reset) == 1
                  reset = ones(1,lchan) * reset;
              end

              if length(reset) ~= lchan
                  error(message('xPCTarget:PCIMCounters:ResetLength'));
              end

              if any(reset < 0) || any(reset > 1)
                  error(message('xPCTarget:PCIMCounters:ResetValue'));
              end
              
              if length(initial) == 1
                  initial = ones(1,lchan) * initial;
              end

              if length(initial) ~= lchan
                  error(message('xPCTarget:PCIMCounters:InitLength'));
              end
      end
  end

  if phase == 3  % Modify the mask enables, used for PWM output ARM
      arm = get_param( gcb, 'arm' );
      if strcmp( arm, 'on' )
          enables = 'on,on,on,on,on,on,on,on';
      else
           enables = 'on,on,on,on,off,on,on,on';
      end
      set_param( gcb, 'MaskEnableString', enables );
  end
