function [maskdisplay, range, reset, initValue] = ...
        mdanipcim(flag, channel, srange, reset, initValue, boardType)

% MDANIPCIM - InitFcn and Mask Initialization for NI PCI-E series D/A section

% Copyright 1996-2011 The MathWorks, Inc.

  switch flag
    case 1
      ck = mxpccrosscheckers;
      boards = ck.pciuniqa();
      if length(boards) > 1
          myslot = evalin( 'base', get_param( gcb, 'slot' ) );
          for bd = 1 : length(boards)
              slots{bd} = evalin('base', get_param( boards{bd}, 'slot' )); %#ok
          end
          ck.pciuniqb( boards, myslot, slots );
      end
    
    case 2

      % srange is a string with values '10', '5', 'APFI0', 'APFI1'
      % convert to '10'->0, '5'->1, 'APFI0'->2, 'APFI1'->3
      srange(srange == '[') = [];
      srange(srange == ']') = [];
      srange(srange == ',') = ' ';
      crange = textscan( srange, '%s %s %s %s' );

      for idx=1:length(crange)
          tmprange = upper( crange{idx} );
          if isempty(tmprange)
              break;
          end

          if strcmp( tmprange, '10' )
              range(idx) = 0; %#ok<AGROW>
          elseif strcmp( tmprange, '5' )
              range(idx) = 1; %#ok<AGROW>
          elseif strcmp( tmprange, 'APFI0' )
              range(idx) = 2; %#ok<AGROW>
          elseif strcmp( tmprange, 'APFI1' )
              range(idx) = 3; %#ok<AGROW>
          else
              error(message('xPCTarget:NIMSeriesDA:Range', tmprange));
          end
      end

      family = floor(mod( boardType/10, 10 ));
      type = floor(mod( boardType, 10 ));
    
      if family == 2  % 622x boards
                      % 6220, 6221, 6224, 6225, 6229
                      % The index into supRange is 1 more than the code
                      % that the driver needs to put into the channel ram
                      % for the gain.  For the 622x boards, the +-10 volt
                      % range is gain code 0.  +-5 is gain code 1, etc.
          rangevals = 0;  % only 1 range allowed, +-10
          rangestr = '10';
        
          switch type
            case 0
              name = 'PCI-6220';
              maxChannel = 0;
            case 1
              name = 'PCI-6221';
              maxChannel = 2;
            case 4
              name = 'PCI-6224';
              maxChannel = 0;
            case 5
              name = 'PCI-6225';
              maxChannel = 2;
            case 7
              name = 'PCI-6221/37 pin';
              maxChannel = 2;
            case 9
              name = 'PCI-6229';
              maxChannel = 4;
            otherwise
              error(message('xPCTarget:NIMSeriesDA:Unknown', boardType));
          end

      % Add a family == 3 for the 3x boards when we decide
      % to support them.
          
      elseif family == 5  %625x boards
                          % 6250, 6251, 6254, 6255, 6259
          switch type
            case 0
              name = 'PCI-6250';
              maxChannel = 0;
              rangevals = [];  % No D/A channels
              rangestr = '';
           case 1
              name = 'PCI-6251';
              maxChannel = 2;
              rangevals = [0, 1, 2];  % Allow +-10, +-5, APFI0
              rangestr = '10, 5, APFI0';
            case 4
              name = 'PCI-6254';
              maxChannel = 0;
              rangevals = [];  % No D/A channels
              rangestr = '';
            case 5
              name = 'PCI-6255';
              maxChannel = 2;
              rangevals = [0, 1, 2];  % Allow +-10, +-5, APFI0
              rangestr = '10, 5, APFI0';
            case 9
              name = 'PCI-6259';
              maxChannel = 4;
              rangevals = [0, 1, 2, 3];  % Allow +-10, +-5, APFI0 and APFI1
              rangestr = '10, 5, APFI0, APFI1';
            otherwise
              error(message('xPCTarget:NIMSeriesDA:Unknown', boardType));
          end
        
      elseif family == 8  % 628x boards
                          % 6280, 6281, 6284, 6289
          switch type
            case 0
              name = 'PCI-6280';
              maxChannel = 0;
              rangevals = [];  % No D/A channels
              rangestr = '';
            case 1
              name = 'PCI-6281';
              maxChannel = 2;
              rangevals = [0, 1, 2];  % Allow +-10, +-5, APFI0
              rangestr = '10, 5, APFI0';
            case 4
              name = 'PCI-6284';
              maxChannel = 0;
              rangevals = [];  % No D/A channels
              rangestr = '';
            case 9
              name = 'PCI-6289';
              maxChannel = 4;
              rangevals = [0, 1, 2, 3];  % Allow +-10, +-5, APFI0 and APFI1
              rangestr = '10, 5, APFI0, APFI1';
           otherwise
              error(message('xPCTarget:NIMSeriesDA:Unknown', boardType));
          end
      else
          error(message('xPCTarget:NIMSeriesDA:BadFamily', family));
      end
      maskdisplay = sprintf( 'disp(''%s\\nNational Instr.\\nAnalog Output'');', name );
    
      for i=1:length(channel)
          maskdisplay = sprintf('%s port_label(''input'', %d, ''%d'');', maskdisplay, i, channel(i) );
      end

      % do scaler to vector expansion
      lc = length(channel);
      if length(range) ~= lc
          if length(range) == 1
              range = range * ones( 1, lc );
          else
              error(message('xPCTarget:NIMSeriesDA:RangeLength'));
          end
      end

      if ~all(ismember( range, rangevals ))
          error(message('xPCTarget:NIMSeriesDA:RangeValue', rangestr));
      end
    
      chUsed = zeros( 1, maxChannel );
      for i = 1:length(channel)
          chan = channel(i);
          if chan < 1 || chan > maxChannel
              error(message('xPCTarget:NIMSeriesDA:ChannelValue', maxChannel));
          end
          if chUsed(chan)
              error(message('xPCTarget:NIMSeriesDA:DupChannel', chan));
          else
              chUsed(chan)=1;
          end
      end

      %%% check reset vector parameter
      if all(size(reset) == 1)
          reset = reset * ones(size(channel));
      elseif ~all(size(reset) == size(channel))
          error(message('xPCTarget:NIMSeriesDA:ResetLength'));
      end
      reset = floor(reset);
      if ~all(ismember(reset, [0 1]))
          error(message('xPCTarget:NIMSeriesDA:ResetValue'));
      end

      %%% check initValue vector parameter
      if ~isa(initValue, 'double')
          error(message('xPCTarget:NIMSeriesDA:InitType'));
      end
      if all(size(initValue) == 1)
          initValue = initValue * ones(size(channel));
      elseif ~all(size(initValue) == size(channel))
          error(message('xPCTarget:NIMSeriesDA:InitLengthO'));
      end
  end
