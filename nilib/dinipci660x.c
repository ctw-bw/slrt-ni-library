/* dinipci660x.c - xPC Target, non-inlined S-function driver for PCI/PXI 6602
 *                  counter/timer boards. This driver is used for digital
 *                  input.
 */
/* Copyright 1996-2009 The MathWorks, Inc.
*/

#define         S_FUNCTION_LEVEL        2
#undef          S_FUNCTION_NAME
#define         S_FUNCTION_NAME         dinipci660x

#include        <stddef.h>
#include        <stdlib.h>

#include        "simstruc.h"

#ifdef          MATLAB_MEX_FILE
#include        "mex.h"
#endif

#ifndef         MATLAB_MEX_FILE
#include        <windows.h>
#include        "xpcimports.h"
#endif

/* Input Arguments */
#define NUMBER_OF_ARGS       (5)
#define CHANNEL_ARG          ssGetSFcnParam(S, 0)
#define SAMP_TIME_ARG        ssGetSFcnParam(S, 1)
#define SLOT_ARG             ssGetSFcnParam(S, 2)
#define PORTCONFIG_ARG       ssGetSFcnParam(S, 3)
#define DEV_ID_ARG           ssGetSFcnParam(S, 4)

#define SAMP_TIME_IND        (0)

#define NO_R_WORKS           (0)
#define NO_I_WORKS           (0)
#define NO_P_WORKS           (1)

/* Index into PWork */
#define BASE_ADDR_P_IND      (0)

static char_T msg[256];

static void mdlInitializeSizes(SimStruct *S)
{
    size_t j;

    ssSetNumSFcnParams(S, NUMBER_OF_ARGS);
    if (ssGetNumSFcnParams(S) != ssGetSFcnParamsCount(S)) {
        sprintf(msg,"Wrong number of input arguments passed.\n"
                "%d arguments are expected\n", NUMBER_OF_ARGS);
        ssSetErrorStatus(S,msg);
        return;
    }

    ssSetNumContStates(S, 0);
    ssSetNumDiscStates(S, 0);

    if( !ssSetNumInputPorts(S, 0) )return;

    if( !ssSetNumOutputPorts(S, (int_T)mxGetN(CHANNEL_ARG)) )return;
    for( j = 0 ; j < mxGetN(CHANNEL_ARG) ; j++ )
    {
        ssSetOutputPortWidth(S, j, 1);
    }

    ssSetNumSampleTimes(S, 1);

    ssSetSimStateCompliance( S, HAS_NO_SIM_STATE );

    ssSetNumRWork(S, NO_R_WORKS);
    ssSetNumIWork(S, NO_I_WORKS);
    ssSetNumPWork(S, NO_P_WORKS);

    ssSetNumModes(S, 0);
    ssSetNumNonsampledZCs(S, 0);

    for (j = 0; j < NUMBER_OF_ARGS; j++)
        ssSetSFcnParamTunable(S, j, 0);

    ssSetOptions(S, SS_OPTION_DISALLOW_CONSTANT_SAMPLE_TIME | SS_OPTION_EXCEPTION_FREE_CODE );
    return;
}

static void mdlInitializeSampleTimes(SimStruct *S) {
    ssSetSampleTime(S, 0, mxGetPr(SAMP_TIME_ARG)[SAMP_TIME_IND]);
    ssSetOffsetTime(S, 0, 0.0);
}

#define MDL_START
static void mdlStart(SimStruct *S) {
#ifndef MATLAB_MEX_FILE
    int i;
    const char *devName;
    unsigned short  devId;
    PCIDeviceInfo pciinfo;
    void *Physical1, *Physical0;
    void *Virtual1,  *Virtual0;
    volatile unsigned int *MITE;
    volatile uint32_T *board;
    int inittype = 0;
    uint32_T config[2];
    int dioconfig;

    switch ((int)mxGetPr(DEV_ID_ARG)[0]) {
      case 1:
        devName = "NI PCI 6602";
        devId = 0x1310;
        inittype = 2;
        break;
      case 2:
        devName = "NI PXI 6602";
        devId = 0x1360;
        inittype = 2;
        break;
      case 3:
        devName = "NI PCI 6601";
        devId = 0x2c60;
        inittype = 1;
        break;
      default:
        devName = "Unknown device";
        printf("Unknown device detected.\n[%d x %d], first element\n"
               "%f\n", mxGetM(DEV_ID_ARG), mxGetN(DEV_ID_ARG),
               mxGetPr(DEV_ID_ARG));    /* xxxxxxxxx */

        devId = 0xFFFF;
        break;
    }
    if ((int_T)mxGetPr(SLOT_ARG)[0]<0) {
        // look for the PCI-Device
        if (rl32eGetPCIInfo( 0x1093, devId, &pciinfo ))
        {
            sprintf(msg, "%s: board not present", devName);
            ssSetErrorStatus(S, msg);
            return;
        }
    } else {
        int_T bus, slot;
        if (mxGetN(SLOT_ARG) == 1) {
            bus = 0;
            slot = (int_T)mxGetPr(SLOT_ARG)[0];
        } else {
            bus = (int_T)mxGetPr(SLOT_ARG)[0];
            slot = (int_T)mxGetPr(SLOT_ARG)[1];
        }
        // look for the PCI-Device
        if (rl32eGetPCIInfoAtSlot(0x1093, devId,
                                  (slot & (int_T)0xff) | 
                                  ((bus & (int_T)0xff)<< 8),
                                  &pciinfo))
        {
            sprintf(msg, "%s (bus %d, slot %d): board not present",
                    devName, bus, slot);
            ssSetErrorStatus(S,msg);
            return;
        }
    }

    // show Device Information
    //rl32eShowPCIInfo(pciinfo);

    Physical1 = (void *)pciinfo.BaseAddress[1];
    Virtual1  = rl32eGetDevicePtr(Physical1, 4096, RT_PG_USERREADWRITE);
    board     = (volatile uint32_T *)Virtual1;

    Physical0 = (void *)pciinfo.BaseAddress[0];
    Virtual0  = rl32eGetDevicePtr(Physical0, 4096, RT_PG_USERREADWRITE);
    MITE      = (volatile unsigned int *) Virtual0;

    if( inittype == 2 )  // 6602 MITE init
    {
        MITE[49]  = ((unsigned int)Physical1 & ~0xff) | 0x0000008C;
        MITE[61]  = 0;
    }
    else  // 6601 MITE init
    {
        MITE[48]  = ((unsigned int)Physical1 & ~0xff) | 0x00000080;
    }

    // 0x77c gets the first 32 bits of config arg, 0x780 gets the second 32
    // bits.  Both di and do drivers write the same values to these registers.
    // The mask init file constructs this config value.
    config[0]  = (uint32_T)mxGetPr(PORTCONFIG_ARG)[0];
    board[ 0x77c/4 ] = config[0];
    config[1] = (uint32_T)mxGetPr(PORTCONFIG_ARG)[1];
    board[ 0x780/4 ] = config[1];

    dioconfig = 0;
    for( i = 0 ; i < 8 ; i++ )
    {
        if( config[i/4] & (1 << 8*(i%4) ) )
            dioconfig |= 1 << i;
    }
    ((short *)board)[0x16/2] = dioconfig;  // DIO config register

    ssGetPWork(S)[BASE_ADDR_P_IND] = (int *)board;
#endif
}

static void mdlOutputs(SimStruct *S, int_T tid) {
#ifndef MATLAB_MEX_FILE
    volatile short *board   = ssGetPWork(S)[BASE_ADDR_P_IND];
    short ival;
    real_T *y;
    size_t i;
    int chan;

    // Read the digital input register and construct the outputs
    ival = board[0xE/2];
    for( i = 0 ; i < mxGetN(CHANNEL_ARG); i++ )
    {
        y=ssGetOutputPortSignal(S, i);
        chan = (int)mxGetPr(CHANNEL_ARG)[i] - 1;
        if( ival & (1 << chan) )
            y[0] = 1;
        else
            y[0] = 0;
    }

    return;
#endif /* not defined MATLAB_MEX_FILE */
}

static void mdlTerminate(SimStruct *S)
{
#ifndef MATLAB_MEX_FILE
#endif
}

#ifdef MATLAB_MEX_FILE  /* Is this file being compiled as a MEX-file? */
#include "simulink.c"   /* Mex glue */
#else
#include "cg_sfun.h"    /* Code generation glue */
#endif
