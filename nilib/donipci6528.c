/*
 * donipci6528.c - xPC Target, non-inlined S-function driver for the 
 *                  digital output section of the National Instruments 6528 boards.
 *               
 *   The 6528 povides 24 input channels, and 24 output channels.  The 24 input channels are located
 *      in 3 separate ports: 0, 1, and 2, with each port containing 8 bits.  The 24 output channels are
 *      in port 3, 4, 5.   To make things consistent with the 6527 board driver, we create this mode, 
 *      which only handles the output for one port. 
 *      
 * Copyright 2007-2009 The MathWorks, Inc.
*/

#define         S_FUNCTION_LEVEL        2
#undef          S_FUNCTION_NAME
#define         S_FUNCTION_NAME         donipci6528

#include        <stddef.h>
#include        <stdlib.h>

#include        "simstruc.h"

#ifdef          MATLAB_MEX_FILE
#include        "mex.h"
#endif

#ifndef         MATLAB_MEX_FILE
#include        <windows.h>
#include        "xpctarget.h"
#include        "dionipci6528.h"
#endif

/* Input Arguments */
#define NUMBER_OF_ARGS          (7)
#define CHANNEL_ARG             ssGetSFcnParam(S,0)
#define PORT_ARG                ssGetSFcnParam(S,1)
#define RESET_ARG               ssGetSFcnParam(S,2)
#define INIT_VAL_ARG            ssGetSFcnParam(S,3)
#define SAMP_TIME_ARG           ssGetSFcnParam(S,4)
#define SLOT_ARG                ssGetSFcnParam(S,5)
#define DEVTYPE_ARG             ssGetSFcnParam(S,6)

#define SAMP_TIME_IND           (0)
#define BASE_ADDR_IND           (0)

#define NO_I_WORKS              (1)
#define BASE_I_IND              (0)

#define NO_R_WORKS              (0)
#define NO_P_WORKS              (0)

#define THRESHOLD               (0.5)

static char_T msg[256];

static void mdlInitializeSizes(SimStruct *S)
{

    size_t i;

    ssSetNumSFcnParams(S, NUMBER_OF_ARGS);
    if (ssGetNumSFcnParams(S) != ssGetSFcnParamsCount(S)) {
        sprintf(msg,"Wrong number of input arguments passed.\n%d arguments are expected\n",NUMBER_OF_ARGS);
        ssSetErrorStatus(S,msg);
        return;
    }

    ssSetNumContStates(S, 0);
    ssSetNumDiscStates(S, 0);

    if( !ssSetNumInputPorts(S, (int_T)mxGetN(CHANNEL_ARG)) )return;
    for (i = 0; i < mxGetN(CHANNEL_ARG); i++) {
        ssSetInputPortWidth(S, i, 1);
        ssSetInputPortDirectFeedThrough(S, i, 1);
    }

    if( !ssSetNumOutputPorts(S, 0) )return;

    ssSetNumSampleTimes(S, 1);

    ssSetSimStateCompliance( S, HAS_NO_SIM_STATE );

    ssSetNumRWork(S, NO_R_WORKS);
    ssSetNumIWork(S, NO_I_WORKS);
    ssSetNumPWork(S, NO_P_WORKS);

    ssSetNumModes(S, 0);
    ssSetNumNonsampledZCs(S, 0);

    for( i = 0 ; i < NUMBER_OF_ARGS ; i++ )
    {
        ssSetSFcnParamTunable(S,i,SS_PRM_NOT_TUNABLE);
    }

    ssSetOptions(S, SS_OPTION_DISALLOW_CONSTANT_SAMPLE_TIME | SS_OPTION_EXCEPTION_FREE_CODE );
}

static void mdlInitializeSampleTimes(SimStruct *S)
{
    if( mxGetN(SAMP_TIME_ARG) == 1 )
    {
        if( mxGetPr(SAMP_TIME_ARG)[0] == -1 )
        {
            ssSetSampleTime( S, 0, INHERITED_SAMPLE_TIME );
            ssSetOffsetTime( S, 0, FIXED_IN_MINOR_STEP_OFFSET );
        }
        else
        {
            ssSetSampleTime( S, 0, mxGetPr(SAMP_TIME_ARG)[0] );
            ssSetOffsetTime( S, 0, 0.0 );
        }
    }
    else
    {
        ssSetSampleTime( S, 0, mxGetPr(SAMP_TIME_ARG)[0] );
        ssSetOffsetTime( S, 0, mxGetPr(SAMP_TIME_ARG)[1] );
    }
}

#define MDL_START
static void mdlStart(SimStruct *S)
{

#ifndef MATLAB_MEX_FILE
    //int_T             port;
    char_T            *devName;
    uint16_T           deviceId;
    xpcPCIDevice      pciinfo;
    volatile uint32_T *Physical0, *Virtual0;
    volatile uint32_T *Physical1, *Virtual1;
    int32_T bus, slot;

    switch( (int_T)mxGetPr(DEVTYPE_ARG)[0] )
    {
      case 1:
        devName = "NI PCI-6528";
        deviceId = NI_PCI_6528_ID;    /* PCI-6528 */
        break;
      case 2:
        devName = "NI PXI-6528";
        deviceId = NI_PXI_6528_ID;    /* PXI-6528 */
        break;
    }

    if (mxGetN(SLOT_ARG) == 1) {
        bus = 0;
        slot = (int32_T)mxGetPr(SLOT_ARG)[0];
    } else {
        bus = (int32_T)mxGetPr(SLOT_ARG)[0];
        slot = (int32_T)mxGetPr(SLOT_ARG)[1];
    }
    // look for the PCI-Device
    if (xpcGetPCIDeviceInfo((uint16_T)NI_VENDOR_ID, deviceId, XPC_NO_SUB, XPC_NO_SUB, bus, slot, &pciinfo)) {
            sprintf(msg,"%s (bus %d,slot %d): board not present",devName, bus, slot );
            ssSetErrorStatus(S,msg);
            return;
    }

    // Map MITE space and mark it read/write
    Physical0 = (volatile uint32_T *)pciinfo.BaseAddress[0];
    Virtual0 = (volatile uint32_T *)xpcReserveMemoryRegion((void*)Physical0, 4096, 
        XPC_RT_PG_USERREADWRITE);

    // Map IO space and mark it read/write
    Physical1 = (volatile uint32_T *)pciinfo.BaseAddress[1];
    Virtual1 = (volatile uint32_T *)xpcReserveMemoryRegion((void*)Physical1, 8192, 
        XPC_RT_PG_USERREADWRITE);
    
    /* Only do the initialization once */
    if ( xpcIsModelInit() && ((volatile uint8_T *)Virtual1)[NI_ID_REG] !=  NI_LAST_TWO_DIGITS_OF_MODEL ){
       // Initialize the board
       Virtual0[NI_BAR0_INIT_REG>>2] = NI_BAR0_INIT_CODE;
       Virtual0[N1_BAR0_WINDOW_DATA_REG>>2] = ((((uint32_T)Physical1) & ~0xFF) | 0x80);// Set address window size.
    }

    /* Check the initialization result */
    if (((volatile uint8_T *)Virtual1)[NI_ID_REG] !=  NI_LAST_TWO_DIGITS_OF_MODEL ){
            sprintf(msg,"%s isn't initialized. ID register %x",devName, 
                ((volatile uint8_T *)Virtual1)[NI_ID_REG] );
            ssSetErrorStatus(S,msg);
            return;
    }

    // Save IO space base address for later.
    ssSetIWorkValue( S, BASE_I_IND, (uint_T)Virtual1 );

#endif
}

static void mdlOutputs(SimStruct *S, int_T tid)
{
#ifndef MATLAB_MEX_FILE
    int_T port;
    size_t  i;
    uchar_T output, channel;
    InputRealPtrsType uPtrs;
    volatile uint8_T *base = (volatile uint8_T *)ssGetIWorkValue(S, BASE_I_IND);
    
    port = (int_T)mxGetPr(PORT_ARG)[0]-1;
   
    output = base[NI_OUT_PORT_DATA_REG+(port<<4)];

    for (i = 0; i < mxGetN(CHANNEL_ARG); i++) {
        uPtrs=ssGetInputPortRealSignalPtrs(S,i);
        channel = (uchar_T)mxGetPr(CHANNEL_ARG)[i] - 1;
        // a 1 in the output register turns the corresponding opto OFF
        if (*uPtrs[0]>=THRESHOLD)
            output &= ~(1 << channel);
        else
             output |= 1 << channel;
   }
   base[NI_OUT_PORT_DATA_REG+(port<<4)] = output;
#endif
}

static void mdlTerminate(SimStruct *S)
{

#ifndef MATLAB_MEX_FILE

    volatile uint8_T *base = (volatile uint8_T *)ssGetIWorkValue(S, BASE_I_IND);
    size_t i;
    int_T port, channel;
    uchar_T output;

    port = (int_T)mxGetPr(PORT_ARG)[0]-1;

    // At load time, set channels to their initial values.
    // At model termination, reset resettable channels to their initial values.

    output = base[NI_OUT_PORT_DATA_REG+(port<<4)];

    for (i = 0; i < mxGetN(CHANNEL_ARG); i++) {
        if (xpcIsModelInit() || (uint_T)mxGetPr(RESET_ARG)[i]) {
            channel = (int_T)mxGetPr(CHANNEL_ARG)[i] - 1;
            // a 1 in the output register turns the corresponding opto OFF
            if ((uint_T)mxGetPr(INIT_VAL_ARG)[i])
                output &= ~(1 << channel);
            else
                output |= 1 << channel;
        }
    }

    base[NI_OUT_PORT_DATA_REG+(port<<4)] = output;
#endif

}

#ifdef MATLAB_MEX_FILE  /* Is this file being compiled as a MEX-file? */
#include "simulink.c"   /* Mex glue */
#else
#include "cg_sfun.h"    /* Code generation glue */
#endif
