
/* Abstract: include file for NI PCI-M functions
 *
 *  Copyright 1996-2008 The MathWorks, Inc.
 *
 */
#ifndef xpcionimh
#define xpcionimh

#include "simstruc.h"

// Access registers using a pointer to the proper type.
// For an S_* register, use 
//  unsigned short *sreg;
//  sreg[S_*] = value;
//
// For an L_* register, use
//  unsigned long *lreg;
//  lreg[L_*] = value;
//
// For a B_* register, use
//  unsigned char *breg;
//  breg[B_*] = value;
//
// The actual pointer values of sreg, lreg and breg are all
// the same, being the base of the register space, usually bar1.

// TIO Register offsets
#define S_Interrupt_G0_Ack           (0x104/2)
#define S_G0_Status_1                (0x104/2)
#define S_G01_Status                 (0x108/2)  /* BOTH */
#define S_G0_Command                 (0x10c/2)
#define L_G0_HW_Save                 (0x110/4)
//#define S_G0_HW_Save_High            (0x110/2)
//#define S_G0_HW_Save_Low             (0x112/2)
#define L_G0_Save                    (0x118/4)
//#define S_G0_Save_High               (0x118/2)
//#define S_G0_Save_Low                (0x11a/2)
#define S_G0_Mode                    (0x134/2)
#define S_G01_Joint_Status_1         (0x136/2)  /* BOTH */
#define L_G0_Load_A                  (0x138/4)
#define S_G01_Joint_Status_2         (0x13a/2)  /* BOTH */
#define L_G0_Load_B                  (0x13c/4)
#define S_G0_Input_Select            (0x148/2)
#define S_G0_AutoIncrement           (0x188/2)
#define S_G01_Joint_Reset            (0x190/2)  /* BOTH */
#define S_Interrupt_G0_Enable        (0x192/2)
#define S_G0_Counting_Mode           (0x1b0/2)
#define S_G0_MSeries_Counting_Mode   (0x1b0/2)
#define S_G0_Second_Gate             (0x1b4/2)
#define S_G0_DMA_Control             (0x1b8/2)
#define S_G0_DMA_Status              (0x1b8/2)
#define S_G0_MSeries_ABZ             (0x1c0/2)

#define S_Interrupt_G1_Ack           (0x106/2)
#define S_G1_Status_1                (0x106/2)
#define S_G1_Command                 (0x10e/2)
#define L_G1_HW_Save                 (0x114/4)
//#define S_G1_HW_Save_High            (0x114/2)
//#define S_G1_HW_Save_Low             (0x116/2)
#define L_G1_Save                    (0x11c/4)
//#define S_G1_Save_High               (0x11c/2)
//#define S_G1_Save_Low                (0x11e/2)
#define S_G1_Mode                    (0x136/2)
#define L_G1_Load_A                  (0x140/4)  /* G0 reg + 2*long */
#define L_G1_Load_B                  (0x144/4)  /* G0 reg + 2*long */
#define S_G1_Input_Select            (0x14a/2)
#define S_G1_AutoIncrement           (0x18a/2)
#define S_Interrupt_G1_Enable        (0x196/2)  /* G0 reg + 2*short */
#define S_G1_Counting_Mode           (0x1b2/2)
#define S_G1_MSeries_Counting_Mode   (0x1b2/2)
#define S_G1_Second_Gate             (0x1b6/2)
#define S_G1_DMA_Control             (0x1ba/2)
#define S_G1_DMA_Status              (0x1ba/2)
#define S_G1_MSeries_ABZ             (0x1c2/2)


// STC Register offsets
#define S_AI_Command_1               (0x110/2) /* 16 bit */
#define S_AI_Command_2               (0x108/2) /* 16 bit */
#define S_AI_Mode_2                  (0x11a/2) /* 16 bit */
#define S_AI_Mode_3                  (0x1ae/2) /* 16 bit */
#define S_AI_Personal                (0x19a/2) /* 16 bit */
#define S_AI_Output_Control          (0x178/2) /* 16 bit */
#define L_AI_SC_Load_A               (0x124/4) /* 32 bit */
#define L_AI_SC_Load_B               (0x128/4) /* 32 bit */
#define L_AI_SC_Save                 (0x184/4) /* 32 bit */
#define L_AI_SI_Load_A               (0x11c/4) /* 32 bit */
#define L_AI_SI_Load_B               (0x120/4) /* 32 bit */
#define L_AI_SI_Save                 (0x180/4) /* 32 bit */
#define L_AI_SI2_Load_A              (0x12c/4) /* 32 bit */
#define L_AI_SI2_Load_B              (0x130/4) /* 32 bit */
#define S_AI_START_STOP_Select       (0x17c/2) /* 16 bit */
#define S_AI_Status_1                (0x104/2) /* 16 bit */
#define S_Analog_Trigger_Etc         (0x17a/2) /* 16 bit */
#define S_AI_Trigger_Select          (0x17e/2) /* 16 bit */
#define L_AO_BC_Load_A               (0x158/4) /* 32 bit */
#define L_AO_BC_Load_B               (0x15c/4) /* 32 bit */
#define L_AO_BC_Save                 (0x124/4) /* 32 bit */
#define S_AO_Command_2               (0x10a/2) /* 16 bit */
#define S_AO_Command_1               (0x112/2) /* 16 bit */
#define S_AO_Mode_1                  (0x14c/2) /* 16 bit */
#define S_AO_Mode_2                  (0x14e/2) /* 16 bit */
#define S_AO_Output_Control          (0x1ac/2) /* 16 bit */
#define S_AO_Mode_3                  (0x18c/2) /* 16 bit */
#define S_AO_Personal                (0x19c/2) /* 16 bit */
#define S_AO_START_Select            (0x184/2) /* 16 bit */
#define S_AO_Status_2                (0x10c/2) /* 16 bit */
#define S_AO_Status_1                (0x106/2) /* 16 bit */
#define S_AO_Trigger_Select          (0x186/2) /* 16 bit */
#define L_AO_UC_Load_A               (0x160/4) /* 32 bit */
#define L_AO_UC_Load_B               (0x164/4) /* 32 bit */
#define L_AO_UC_Save                 (0x128/4) /* 32 bit */
#define L_AO_UI_Load_A               (0x150/4) /* 32 bit */
#define L_AO_UI_Load_B               (0x154/4) /* 32 bit */
#define L_AO_UI_Save                 (0x120/4) /* 32 bit */
#define S_Clock_and_FOUT             (0x170/2) /* 16 bit */
#define S_Interrupt_A_Ack            (0x104/2) /* 16 bit */
#define S_Interrupt_A_Enable         (0x192/2) /* 16 bit */
#define S_Interrupt_B_Ack            (0x106/2) /* 16 bit */
#define S_Interrupt_Control          (0x176/2) /* 16 bit */
#define S_Interrupt_B_Enable         (0x196/2) /* 16 bit */
#define S_IO_Bidirection_Pin         (0x172/2) /* 16 bit */
#define S_Joint_Reset                (0x190/2) /* 16 bit */
#define S_Joint_Status_2             (0x13a/2) /* 16 bit */
#define S_Joint_Status_1             (0x136/2) /* 16 bit */
#define S_RTSI_Shared_MUX            (0x1a2/2) /* 16 bit */
#define S_RTSI_Trig_A_Output         (0x19e/2) /* 16 bit */
#define S_RTSI_Trig_Direction        (0x174/2) /* 16 bit */
#define S_RTSI_Trig_B_Output         (0x1a0/2) /* 16 bit */
//#define S_G0_DMA_Config              (0x1b8/2) /* 16 bit */
//#define S_G1_DMA_Config              (0x1ba/2) /* 16 bit */
//#define S_G0_DMA_Status              (0x1b8/2) /* 16 bit */
//#define S_G1_DMA_Status              (0x1ba/2) /* 16 bit */
#define B_CDIO_DMA_Select            0x7   /* 8 bit */
#define B_SCXI_Control               0x13  /* 8 bit */
#define S_AI_Mode_1                  (0x118/2) /* 16 bit */
#define S_AI_DIV_Load_A              (0x180/2) /* 16 bit */
#define B_SCXI_Serial_Data_In        0x9   /* 8 bit */
#define B_SCXI_Serial_Data_Out       0x11  /* 8 bit */
#define B_SCXI_Status                0x7   /* 8 bit */
#define B_SCXI_Output_Enable         0x15  /* 8 bit */
#define B_SCXI_DIO_Enable            0x21c /* 8 bit */
#define L_Static_Digital_Output      (0x24/4) /* 32 bit */
#define L_Static_Digital_Input       (0x24/4) /* 32 bit */
#define L_DIO_Direction              (0x28/4) /* 32 bit */
#define B_AO_Serial_Interrupt_Enable 0xa0  /* 8 bit */
#define B_AO_Serial_Interrupt_Ack    0xa1  /* 8 bit */
#define B_AO_Serial_Interrupt_Status 0xa1  /* 8 bit */
#define B_Interrupt_C_Enable         0x88  /* 8 bit */
#define B_Interrupt_C_Status         0x88  /* 8 bit */
#define B_Analog_Trigger_Control     0x8c  /* 8 bit */
#define L_AI_FIFO_Data               (0x1c/4) /* 32 bit */
#define S_AI_FIFO_Clear              (0x1a6/2) /* 16 bit */
#define AI_AO_Select                 0x9   /* 8 bit */
#define S_AI_Config_FIFO_Data        (0x5e/2) /* 16 bit */
#define S_Configuration_Memory_Clear (0x1a4/2) /* 16 bit */
#define L_AO_FIFO_Data               (0xa4/4) /* 32 bit */
#define S_AO_FIFO_Clear              (0x1a8/2) /* 16 bit */
#define B_G0_G1_Select               0xb   /* 8 bit */
#define B_Misc_Command               0xf   /* 8 bit */
#define B_AO_Calibration             0xa3  /* 8 bit */
#define S_PFI_Output_Select_1        (0x1d0/2) /* 16 bit */
#define S_PFI_Output_Select_2        (0x1d2/2) /* 16 bit */
#define S_PFI_Output_Select_3        (0x1d4/2) /* 16 bit */
#define S_PFI_Output_Select_4        (0x1d6/2) /* 16 bit */
#define S_PFI_Output_Select_5        (0x1d8/2) /* 16 bit */
#define S_PFI_Output_Select_6        (0x1da/2) /* 16 bit */
#define S_PFI_DI                     (0x1dc/2) /* 16 bit */
#define S_PFI_DO                     (0x1de/2) /* 16 bit */
#define L_PFI_Filter                 (0xb0/4) /* 32 bit */
#define L_RTSI_Filter                (0xb4/4) /* 32 bit */
#define S_Clock_And_Fout2            (0x1c4/2) /* 16 bit */
#define S_PLL_Control                (0x1c6/2) /* 16 bit */
#define S_PLL_Status                 (0x1c8/2) /* 16 bit */
#define L_AI_Config_FIFO_Bypass      (0x218/4) /* 32 bit */
#define L_CDI_FIFO_Data              (0x220/4) /* 32 bit */
#define L_CDO_FIFO_Data              (0x220/4) /* 32 bit */
#define L_CDIO_Status                (0x224/4) /* 32 bit */
#define L_CDIO_Command               (0x224/4) /* 32 bit */
#define L_CDI_Mode                   (0x228/4) /* 32 bit */
#define L_CDO_Mode                   (0x22c/4) /* 32 bit */
#define L_CDI_Mask_Enable            (0x230/4) /* 32 bit */
#define L_CDO_Mask_Enable            (0x234/4) /* 32 bit */
#define B_SCXI_Legacy_Compatibility  0xbc  /* 8 bit */
#define L_DIO_Reserved_0             (0x240/4) /* 32 bit */
#define L_DIO_Reserved_1             (0x244/4) /* 32 bit */
#define L_DIO_Reserved_2             (0x240/4) /* 32 bit */
#define L_DIO_Reserved_3             (0x244/4) /* 32 bit */
#define L_DIO_Reserved_4             (0x248/4) /* 32 bit */
#define L_DIO_Reserved_5             (0x24c/4) /* 32 bit */
#define L_DIO_Reserved_6             (0x250/4) /* 32 bit */
#define L_DIO_Reserved_7             (0x254/4) /* 32 bit */
#define S_LB_Reserved_0              (0x204/2) /* 16 bit */
#define L_LB_Reserved_1              (0x200/4) /* 32 bit */
#define L_Cal_PWM                    (0x40/4) /* 32 bit */

// The following registers come as sets of registers
// See the tMSeries.cpp file in the NI M series DDK, tMSeries::_initialize()
// Address is 0xc2 + 0x4*i, i = 0:15
#define B_AO_Waveform_Order0          0xc2          /* 8 bit */
#define B_AO_Waveform_Order1         (0xc2 + 0x04)  /* 8 bit */
#define B_AO_Waveform_Order2         (0xc2 + 0x08)  /* 8 bit */
#define B_AO_Waveform_Order3         (0xc2 + 0x0c)  /* 8 bit */
#define B_AO_Waveform_Order4         (0xc2 + 0x10)  /* 8 bit */
#define B_AO_Waveform_Order5         (0xc2 + 0x14)  /* 8 bit */
#define B_AO_Waveform_Order6         (0xc2 + 0x18)  /* 8 bit */
#define B_AO_Waveform_Order7         (0xc2 + 0x1c)  /* 8 bit */
#define B_AO_Waveform_Order8         (0xc2 + 0x20)  /* 8 bit */
#define B_AO_Waveform_Order9         (0xc2 + 0x24)  /* 8 bit */
#define B_AO_Waveform_Order10        (0xc2 + 0x28)  /* 8 bit */
#define B_AO_Waveform_Order11        (0xc2 + 0x2c)  /* 8 bit */
#define B_AO_Waveform_Order12        (0xc2 + 0x30)  /* 8 bit */
#define B_AO_Waveform_Order13        (0xc2 + 0x34)  /* 8 bit */
#define B_AO_Waveform_Order14        (0xc2 + 0x38)  /* 8 bit */
#define B_AO_Waveform_Order15        (0xc2 + 0x3c)  /* 8 bit */

// 4 bank config registers
// address is 0xc3 + 0x4*i, i = 0:3
#define B_AO_Config_Bank0             0xc3          /* 8 bit */
#define B_AO_Config_Bank1            (0xc3 + 0x04)  /* 8 bit */
#define B_AO_Config_Bank2            (0xc3 + 0x08)  /* 8 bit */
#define B_AO_Config_Bank3            (0xc3 + 0x0c)  /* 8 bit */

// 16 DAC direct registers.  These look odd since they seem to
// overlap the waveform order and config bank registers.  Is
// it read versus write?  Or does the hardware differentiate
// between a byte access and a long access?
// byte address is 0xc0 + 0x4*i, i = 0:15, divide byte address by 4
#define L_DAC_Direct_Data0            0xc0/4
#define L_DAC_Direct_Data1           (0xc0/4 + 0x01)  /* 32 bit */
#define L_DAC_Direct_Data2           (0xc0/4 + 0x02)  /* 32 bit */
#define L_DAC_Direct_Data3           (0xc0/4 + 0x03)  /* 32 bit */
#define L_DAC_Direct_Data4           (0xc0/4 + 0x04)  /* 32 bit */
#define L_DAC_Direct_Data5           (0xc0/4 + 0x05)  /* 32 bit */
#define L_DAC_Direct_Data6           (0xc0/4 + 0x06)  /* 32 bit */
#define L_DAC_Direct_Data7           (0xc0/4 + 0x07)  /* 32 bit */
#define L_DAC_Direct_Data8           (0xc0/4 + 0x08)  /* 32 bit */
#define L_DAC_Direct_Data9           (0xc0/4 + 0x09)  /* 32 bit */
#define L_DAC_Direct_Data10          (0xc0/4 + 0x0a)  /* 32 bit */
#define L_DAC_Direct_Data11          (0xc0/4 + 0x0b)  /* 32 bit */
#define L_DAC_Direct_Data12          (0xc0/4 + 0x0c)  /* 32 bit */
#define L_DAC_Direct_Data13          (0xc0/4 + 0x0d)  /* 32 bit */
#define L_DAC_Direct_Data14          (0xc0/4 + 0x0e)  /* 32 bit */
#define L_DAC_Direct_Data15          (0xc0/4 + 0x0f)  /* 32 bit */

// 8 PWM generation registers
// byte address is 0x44 + 0x2*i, i = 0:7, divide byte address by 2
#define S_Gen_PWM0                    0x44/2
#define S_Gen_PWM1                   (0x44/2 + 0x01)  /* 16 bit */
#define S_Gen_PWM2                   (0x44/2 + 0x02)  /* 16 bit */
#define S_Gen_PWM3                   (0x44/2 + 0x03)  /* 16 bit */
#define S_Gen_PWM4                   (0x44/2 + 0x04)  /* 16 bit */
#define S_Gen_PWM5                   (0x44/2 + 0x05)  /* 16 bit */
#define S_Gen_PWM6                   (0x44/2 + 0x06)  /* 16 bit */
#define S_Gen_PWM7                   (0x44/2 + 0x07)  /* 16 bit */

// The AI static control register addresses came from
// a list in the NI DDK.
#define B_Static_AI_Control0         0x64     /* 8 bit */
#define B_Static_AI_Control1         0x261    /* 8 bit */
#define B_Static_AI_Control2         0x262    /* 8 bit */
#define B_Static_AI_Control3         0x263    /* 8 bit */
#define B_Static_AI_Control4         0x264    /* 8 bit */
#define B_Static_AI_Control5         0x265    /* 8 bit */
#define B_Static_AI_Control6         0x266    /* 8 bit */
#define B_Static_AI_Control7         0x267    /* 8 bit */
 

// New names prototyped and modified from NI examples
//
//  Model specific information:
//
//  Gain range map
//      Range     -> gain value
//
//  NI 622x: 
//      +/- 10V   -> 0
//      +/- 5V    -> 1
//      +/- 1V    -> 4
//      +/- 200mV -> 5
//
//  NI 625x / NI 628x:
//      +/- 10V   -> 1
//      +/- 5V    -> 2
//      +/- 2V    -> 3
//      +/- 1V    -> 4
//      +/- 500mV -> 5
//      +/- 200mV -> 6
//      +/- 100mV -> 7
//
// -------------------------------
//
// Minimum convert period divisor: (20 MHz timebase)
//
//  NI 622x        
//      Single Channel    -> 80
//      Multiple Channels -> 80
//
//  NI 625x        
//      Single Channel    -> 16
//      Multiple Channels -> 20
//
//  NI 628x         
//      Single Channel    -> 30
//      Multiple Channels -> 40
//
// Minimum convert delay divisor -> 3
//

#ifdef CPPHDR  // if included from a C++ file
extern "C" {
#endif
    
typedef struct MSeriesBoard
{
    uint32_T    Type;
    uint32_T    devID[4];   // pci, pcie, pxi, pxie, 0 if not supported
    uint32_T    calslot;    // 0 for 622x, 1 for the rest, first index in gainidx[][]
    bool        AdReset;    // True for 5x boards only
    uint32_T    maxADChan;
    uint32_T    minConvert[2]; // single or multiple channel minimum convert count at 20 MHz
    uint32_T    maxDIOStatic;
    uint32_T    maxDAChan;
    uint32_T    maxDARange;
    const char  *name;  //uint8_T     *name;
} MseriesBoard;

// Return the entry in the MseriesBoard SupportedBoard array and a filled in pciinfo struct.
MseriesBoard *niMIdentify( SimStruct *S,
                           int deviceType,
                           int bus, int slot,
                           xpcPCIDevice *pciinfo );

int  niMconfig( int deviceId, int bus, int slot, xpcPCIDevice *pciinfo );

void niM_AISetup( void *base,
                  bool reset5x,
                  uint32_T *channels,
                  uint32_T *range,
                  uint32_T *coupling,
                  uint32_T *polarity,
                  uint32_T nchans,
                  uint32_T scancount );

void niM_aiConfigureChannel (void *base,
                             uint16_T channel, 
                             uint16_T gain, 
                             uint16_T polarity, 
                             uint16_T channelType,
                             bool lastChannel);


void niM_configureTimebase (void *base);
void niM_pllReset (void *base);
void niM_analogTriggerReset (void *base);

MseriesBoard *niMIdentify( SimStruct *S,
                           int deviceType,
                           int bus, int slot,
                           xpcPCIDevice *pciinfo );

uint32_T niM_Private_Data( void *base );
int32_T niM_SetPFIFilter( uint32_T bnum, uint32_T chan, uint32_T value );

#define PFI_SELECT_DEFAULT 0
#define PFI_SELECT_G0OUT   13
#define PFI_SELECT_G1OUT   14
#define PFI_SELECT_DO      16
// Add others from NI doc as we need them.
void niM_SetPFI_Select( uint32_T bnum, uint32_T pfinum, uint32_T connect );
uint16_T niM_SetPFI_DO( uint32_T bnum, uint32_T pfinum, uint32_T val );

uint32_T niM_whichBank(volatile uint16_T *sreg, uint32_T chan);
void niM_resetCtr(volatile uint16_T *sreg, int chan);

// Functions for setting up, arming and disarming counting for
// PWM generation
void niM_Cont_Pulse_Train_Generation(void *base,
                                     uint32_T chan,
                                     uint32_T low, uint32_T high );
void niM_CtrArm(void *base, uint32_T chan );
void niM_CtrDisarm(void *base, uint32_T chan );

// Functions for setting up, arming and disarming pulse width
// measurement.
void niM_PeriodConfigure (void *base,
                          uint32_T chan,
                          uint32_T trigmode,
                          int devtype );
void niM_CtrReadArm(void *base, uint32_T chan );
void niM_CtrReadDisarm(void *base, uint32_T chan );

void niM_Counter_Reset_All(void *base, uint32_T chan );


typedef struct  // size is 5, 32 bit quantities
{
    uint32_T order;
    float c[4]; 
}tScalingCoefficients;


void niM_eepromReadMSeries (void *bar0,
                            void *bar1,
                            uint8_T *eepromBuffer,
                            uint32_T bufferSize);

//
// aiGetScalingCoefficients --
//
// modeIdx
//    0 -> default
//
// intervalIdx
//    0 -> +/- 10V
//    1 -> +/- 5V
//    2 -> +/- 2V
//    3 -> +/- 1V
//    4 -> +/- 500mV
//    5 -> +/- 200mV
//    6 -> +/- 100mV
//
// channel
//    ignored - all channels use the same ADC
//
void niM_aiGetScalingCoefficients (const uint8_T *eepromMemory,
                                   uint32_T intervalIdx,
                                   uint32_T modeIdx,
                                   uint32_T channel,
                                   tScalingCoefficients *scaling);

//
// aoGetScalingCoefficients --
//
// modeIdx
//    ignored - AO does not use the mode constants
//
// intervalIdx
//    0 -> 10V reference
//    1 -> 5V reference
//    2 -> 2V reference
//    3 -> 1V reference
//
// channel
//   dac number
//
void niM_aoGetScalingCoefficients (const uint8_T *eepromMemory,
                                   uint32_T intervalIdx,
                                   uint32_T modeIdx,
                                   uint32_T channel,
                                   tScalingCoefficients *scaling);

void niM_aiPolynomialScaler (const int32_T *raw,
                             float *scaled,
                             const tScalingCoefficients *scaling);

int32_T niM_aoLinearScaler(  const float scaled,
                             const tScalingCoefficients *scaling);

void niM_AO_Setup( void *base );

void niM_aoConfigureDAC( void *base,
                         uint32_T chan,
                         uint32_T order,
                         uint32_T reference );
#ifdef CPPHDR  // if included from a C++ file
}
#endif
#endif // xpcionimh
