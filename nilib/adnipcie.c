/* adnipcie.c - xPC Target, non-inlined S-function driver for A/D section of NI PCI-E series boards  */
/* Copyright 1996-2009 The MathWorks, Inc.
*/

#define         S_FUNCTION_LEVEL        2
#undef          S_FUNCTION_NAME
#define         S_FUNCTION_NAME         adnipcie

#include        <stddef.h>
#include        <stdlib.h>

#include        "simstruc.h"

#ifdef          MATLAB_MEX_FILE
#include        "mex.h"
#endif

#ifndef         MATLAB_MEX_FILE
#include        <windows.h>
#include        "xpcimports.h"
#include        "xpcionie.h"
#endif

/* Input Arguments */
#define NUMBER_OF_ARGS          (6)
#define CHANNEL_ARG             ssGetSFcnParam(S,0)
#define RANGE_ARG               ssGetSFcnParam(S,1)
#define COUPLING_ARG            ssGetSFcnParam(S,2)
#define SAMP_TIME_ARG           ssGetSFcnParam(S,3)
#define SLOT_ARG                ssGetSFcnParam(S,4)
#define DEV_ARG                 ssGetSFcnParam(S,5)

#define SAMP_TIME_IND           (0)
#define BASE_ADDR_IND           (0)

#define NO_I_WORKS              (66)
#define CHANNELS_I_IND          (0)
#define BASE_ADDR_I_IND         (1)
#define POL_I_IND               (2)

#define NO_R_WORKS              (64)
#define GAIN_R_IND              (0)

static char_T msg[256];

static void mdlInitializeSizes(SimStruct *S)
{
    size_t i;

    ssSetNumSFcnParams(S, NUMBER_OF_ARGS);
    if (ssGetNumSFcnParams(S) != ssGetSFcnParamsCount(S)) {
        sprintf(msg, "Wrong number of input arguments passed.\n%d arguments are expected\n", NUMBER_OF_ARGS);
        ssSetErrorStatus(S, msg);
        return;
    }

    ssSetNumContStates(S, 0);
    ssSetNumDiscStates(S, 0);

    if( !ssSetNumOutputPorts(S, (int_T)mxGetN(CHANNEL_ARG)) )return;
    for (i=0;i<mxGetN(CHANNEL_ARG);i++) {
        ssSetOutputPortWidth(S, i, 1);
    }

    if( !ssSetNumInputPorts(S, 0) )return;

    ssSetNumSampleTimes(S, 1);

    ssSetSimStateCompliance( S, HAS_NO_SIM_STATE );

    ssSetNumRWork(S, NO_R_WORKS);
    ssSetNumIWork(S, NO_I_WORKS);
    ssSetNumPWork(S, 0);

    ssSetNumModes(S, 0);
    ssSetNumNonsampledZCs(S, 0);

    ssSetSFcnParamNotTunable(S, 0);
    ssSetSFcnParamNotTunable(S, 1);
    ssSetSFcnParamNotTunable(S, 2);
    ssSetSFcnParamNotTunable(S, 3);
    ssSetSFcnParamNotTunable(S, 4);
    ssSetSFcnParamNotTunable(S, 5);

    ssSetOptions(S, SS_OPTION_DISALLOW_CONSTANT_SAMPLE_TIME | SS_OPTION_EXCEPTION_FREE_CODE );
}

static void mdlInitializeSampleTimes(SimStruct *S)
{
    ssSetSampleTime(S, 0, mxGetPr(SAMP_TIME_ARG)[SAMP_TIME_IND]);
    ssSetOffsetTime(S, 0, 0.0);
}

#define MDL_START
static void mdlStart(SimStruct *S)
{
#ifndef MATLAB_MEX_FILE

    size_t nChannels;
    size_t i;
    int_T channel, range, dacRange, coupling, out;
    int_T convDelay;

    PCIDeviceInfo pciinfo;
    void *Physical1, *Physical0;
    void *Virtual1, *Virtual0;
    volatile unsigned int *ioaddress0;
    volatile unsigned short *ioaddress;
    volatile unsigned char *ioaddress8;
    uint8_T *devName;
    int  devId;
    double resFloat;

    nChannels = mxGetN(CHANNEL_ARG);

    switch ((int_T)mxGetPr(DEV_ARG)[0]) {
        case 1:
            devName = "NI PCI-6023E";
            devId=0x2a60;
            resFloat=4096.0;
            convDelay=0x0040;
            break;
        case 2:
            devName = "NI PCI-6024E";
            devId=0x2a70;
            resFloat=4096.0;
            convDelay=0x0040;
            break;
        case 3:
            devName = "NI PCI-6025E";
            devId=0x2a80;
            resFloat=4096.0;
            convDelay=0x0040;
            break;
        case 4:
            devName = "NI PCI-MIO-16E-1";
            devId=0x1180;
            resFloat=4096.0;
            convDelay=0x000f;
            break;
        case 5:
            devName = "NI PCI-MIO-16E-4";
            devId=0x1190;
            resFloat=4096.0;
            convDelay=0x0027;
            break;
        case 6:
            devName = "NI PXI-6070E";
            devId=0x11b0;
            resFloat=4096.0;
            convDelay=0x000f;
            break;
        case 7:
            devName = "NI PXI-6040E";
            devId=0x11c0;
            resFloat=4096.0;
            convDelay=0x0027;
            break;
        case 8:
            devName = "NI PCI-6071E";
            devId=0x1350;
            resFloat=4096.0;
            convDelay=0x000f;
            break;
        case 9:
            devName = "NI PCI-6052E";
            devId=0x18b0;
            resFloat=65536.0;
            convDelay=0x003a;
            break;
        case 10:
            devName = "NI PCI-MIO-16XE-10";
            devId=0x1170;
            resFloat=65536.0;
            convDelay=0x00bc;
            break;
        case 11:
            devName = "NI PCI-6031E";
            devId=0x1330;
            resFloat=65536.0;
            convDelay=0x00bc;
            break;
        case 12:
            devName = "NI PXI-6071E";
            devId=0x15B0;
            resFloat=4096.0;
            convDelay=0x000f;
            break;
        case 13: // PCI-6713
        case 14: // PXI-6713
        case 15: // PCI-6711
            break;
        case 16:
            devName = "NI PXI-6052E";
            devId=0x18c0;
            resFloat=65536.0;
            convDelay=0x03a;
            break;
        case 17: // PXI-6711
        case 18: // PCI-6731
        case 19: // PXI-6731
        case 20: // PCI-6733
        case 21: // PXI-6733
            break;  // No A/D section on these boards
        case 22:
            devName = "NI PCI-6011E (PCI-MIO-16XE-50)";
            devId=0x0162;
            resFloat=65536.0;
            convDelay = 0x03ac;  // 20 Khz board, needs almost 50 microseconds
            break;
    }

    if ((int_T)mxGetPr(SLOT_ARG)[0]<0) {
        /* look for the PCI-Device */
        if (rl32eGetPCIInfo((unsigned short)0x1093, (unsigned short)devId, &pciinfo)) {
            sprintf(msg, "%s: board not present", devName);
            ssSetErrorStatus(S, msg);
            return;
        }
    } else {
        int_T bus, slot;
        if (mxGetN(SLOT_ARG) == 1) {
            bus = 0;
            slot = (int_T)mxGetPr(SLOT_ARG)[0];
        } else {
            bus = (int_T)mxGetPr(SLOT_ARG)[0];
            slot = (int_T)mxGetPr(SLOT_ARG)[1];
        }
        // look for the PCI-Device
        if (rl32eGetPCIInfoAtSlot((unsigned short)0x1093, (unsigned short)devId, (slot & 0xff) | ((bus & 0xff)<< 8), &pciinfo)) {
            sprintf(msg, "%s (bus %d, slot %d): board not present", devName, bus, slot );
            ssSetErrorStatus(S, msg);
            return;
        }
    }
    
    // show Device Information
    //rl32eShowPCIInfo(pciinfo);
    
    Physical1 = (void *)pciinfo.BaseAddress[1];
    Virtual1 = rl32eGetDevicePtr(Physical1, 4096, RT_PG_USERREADWRITE);
    ioaddress = (unsigned short *)Virtual1;
    ioaddress8 = (unsigned char *)Virtual1;
    
    Physical0 = (void *)pciinfo.BaseAddress[0];
    Virtual0 = rl32eGetDevicePtr(Physical0, 4096, RT_PG_USERREADWRITE);
    ioaddress0 = (unsigned int *) Virtual0;
    ioaddress0[48] = ((unsigned int)Physical1 & 0xffffff00) | 0x00000080;
    
    ssSetIWorkValue(S, BASE_ADDR_I_IND, (uint_T)ioaddress);
    ssSetIWorkValue(S, CHANNELS_I_IND, nChannels);
    
    // calibration
    switch ((int_T)mxGetPr(DEV_ARG)[0]) {
        case 1:
            nie_writeCALDAC(ioaddress8, 4, nie_readEEPROM(ioaddress8, 442));
            nie_writeCALDAC(ioaddress8, 11, nie_readEEPROM(ioaddress8, 441));
            nie_writeCALDAC(ioaddress8, 1, nie_readEEPROM(ioaddress8, 440));
            nie_writeCALDAC(ioaddress8, 2, nie_readEEPROM(ioaddress8, 439));
            break;
        case 2:
            nie_writeCALDAC(ioaddress8, 4, nie_readEEPROM(ioaddress8, 430));
            nie_writeCALDAC(ioaddress8, 11, nie_readEEPROM(ioaddress8, 429));
            nie_writeCALDAC(ioaddress8, 1, nie_readEEPROM(ioaddress8, 428));
            nie_writeCALDAC(ioaddress8, 2, nie_readEEPROM(ioaddress8, 427));
            break;
        case 3:
            nie_writeCALDAC(ioaddress8, 4, nie_readEEPROM(ioaddress8, 430));
            nie_writeCALDAC(ioaddress8, 11, nie_readEEPROM(ioaddress8, 429));
            nie_writeCALDAC(ioaddress8, 1, nie_readEEPROM(ioaddress8, 428));
            nie_writeCALDAC(ioaddress8, 2, nie_readEEPROM(ioaddress8, 427));
            break;
        case 4:
            nie_writeCALDAC(ioaddress8, 4, nie_readEEPROM(ioaddress8, 424));
            nie_writeCALDAC(ioaddress8, 1, nie_readEEPROM(ioaddress8, 423));
            nie_writeCALDAC(ioaddress8, 3, nie_readEEPROM(ioaddress8, 422));
            nie_writeCALDAC(ioaddress8, 14, nie_readEEPROM(ioaddress8, 422));
            nie_writeCALDAC(ioaddress8, 2, nie_readEEPROM(ioaddress8, 421));
            break;
        case 5:
            nie_writeCALDAC(ioaddress8, 4, nie_readEEPROM(ioaddress8, 424));
            nie_writeCALDAC(ioaddress8, 1, nie_readEEPROM(ioaddress8, 423));
            nie_writeCALDAC(ioaddress8, 3, nie_readEEPROM(ioaddress8, 422));
            nie_writeCALDAC(ioaddress8, 14, nie_readEEPROM(ioaddress8, 422));
            nie_writeCALDAC(ioaddress8, 2, nie_readEEPROM(ioaddress8, 421));
            break;
        case 6:
            nie_writeCALDAC(ioaddress8, 4, nie_readEEPROM(ioaddress8, 424));
            nie_writeCALDAC(ioaddress8, 1, nie_readEEPROM(ioaddress8, 423));
            nie_writeCALDAC(ioaddress8, 3, nie_readEEPROM(ioaddress8, 422));
            nie_writeCALDAC(ioaddress8, 14, nie_readEEPROM(ioaddress8, 422));
            nie_writeCALDAC(ioaddress8, 2, nie_readEEPROM(ioaddress8, 421));
            break;
        case 7:
            nie_writeCALDAC(ioaddress8, 4, nie_readEEPROM(ioaddress8, 424));
            nie_writeCALDAC(ioaddress8, 1, nie_readEEPROM(ioaddress8, 423));
            nie_writeCALDAC(ioaddress8, 3, nie_readEEPROM(ioaddress8, 422));
            nie_writeCALDAC(ioaddress8, 14, nie_readEEPROM(ioaddress8, 422));
            nie_writeCALDAC(ioaddress8, 2, nie_readEEPROM(ioaddress8, 421));
            break;
        case 8:
        case 12:
            nie_writeCALDAC(ioaddress8, 4, nie_readEEPROM(ioaddress8, 424));
            nie_writeCALDAC(ioaddress8, 1, nie_readEEPROM(ioaddress8, 423));
            nie_writeCALDAC(ioaddress8, 3, nie_readEEPROM(ioaddress8, 422));
            nie_writeCALDAC(ioaddress8, 14, nie_readEEPROM(ioaddress8, 422));
            nie_writeCALDAC(ioaddress8, 2, nie_readEEPROM(ioaddress8, 421));
            break;
        case 9:
        case 16:
            nie_writeCALDAC(ioaddress8, 0, nie_readEEPROM(ioaddress8, 414));
            nie_writeCALDAC(ioaddress8, 8, nie_readEEPROM(ioaddress8, 413));
            nie_writeCALDAC(ioaddress8, 4, nie_readEEPROM(ioaddress8, 412));
            nie_writeCALDAC(ioaddress8, 12, nie_readEEPROM(ioaddress8, 411));
            nie_writeCALDAC(ioaddress8, 2, nie_readEEPROM(ioaddress8, 410));
            nie_writeCALDAC(ioaddress8, 10, nie_readEEPROM(ioaddress8, 409));
            nie_writeCALDAC(ioaddress8, 14, nie_readEEPROM(ioaddress8, 408));
            nie_writeCALDAC(ioaddress8, 7, nie_readEEPROM(ioaddress8, 407));
            break;
        case 10:
        case 11:
            dacRange=(int_T)(mxGetPr(RANGE_ARG)[0]);
            if (dacRange > 0) {
                //Bipolar
                nie_writeCALDAC3(ioaddress8, (short)(nie_readEEPROM(ioaddress8, 428)|(nie_readEEPROM(ioaddress8, 429) << 8)));
                nie_writeCALDAC2(ioaddress8, 2, nie_readEEPROM(ioaddress8, 427));
                nie_writeCALDAC2(ioaddress8, 3, nie_readEEPROM(ioaddress8, 426));
                nie_writeCALDAC2(ioaddress8, 0, nie_readEEPROM(ioaddress8, 425));
                nie_writeCALDAC2(ioaddress8, 1, nie_readEEPROM(ioaddress8, 424));
            } else {
                //Unipolar
                nie_writeCALDAC3(ioaddress8, (short)(nie_readEEPROM(ioaddress8, 422)|(nie_readEEPROM(ioaddress8, 423) << 8)));
                nie_writeCALDAC2(ioaddress8, 2, nie_readEEPROM(ioaddress8, 421));
                nie_writeCALDAC2(ioaddress8, 3, nie_readEEPROM(ioaddress8, 420));
                nie_writeCALDAC2(ioaddress8, 0, nie_readEEPROM(ioaddress8, 419));
                nie_writeCALDAC2(ioaddress8, 1, nie_readEEPROM(ioaddress8, 418));
            }
            break;
            
        case 22:  // 6011 (MIO-16XE-50)
            dacRange=(int_T)(mxGetPr(RANGE_ARG)[0]);
            if (dacRange > 0) {
                //Bipolar
                nie_writeCALDAC3(ioaddress8, (short)(nie_readEEPROM(ioaddress8, 435)|(nie_readEEPROM(ioaddress8, 436) << 8)));
                nie_writeCALDAC2(ioaddress8, 2, nie_readEEPROM(ioaddress8, 434));
                nie_writeCALDAC2(ioaddress8, 0, nie_readEEPROM(ioaddress8, 433));
                nie_writeCALDAC2(ioaddress8, 1, nie_readEEPROM(ioaddress8, 432));
            } else {
                //Unipolar
                nie_writeCALDAC3(ioaddress8, (short)(nie_readEEPROM(ioaddress8, 430)|(nie_readEEPROM(ioaddress8, 431) << 8)));
                nie_writeCALDAC2(ioaddress8, 2, nie_readEEPROM(ioaddress8, 429));
                nie_writeCALDAC2(ioaddress8, 0, nie_readEEPROM(ioaddress8, 428));
                nie_writeCALDAC2(ioaddress8, 1, nie_readEEPROM(ioaddress8, 427));
            }
            break;
    }
    
    nie_DAQ_STC_Windowed_Mode_Write(ioaddress, Write_Strobe_0_Register, 0x0001);
    nie_DAQ_STC_Windowed_Mode_Write(ioaddress, Write_Strobe_1_Register, 0x0001);
    
    for (i=0;i<nChannels;i++) {
        
        channel = (int_T)mxGetPr(CHANNEL_ARG)[i];
        range=(int_T)(1000*mxGetPr(RANGE_ARG)[i]);
        coupling = (int_T)mxGetPr(COUPLING_ARG)[i];
        
        out=0x0;
        out=out | (channel -1);
        if (coupling==0) out= out | (0x3<<12);
        else if (coupling==1) out= out | (0x2<<12);
        else if (coupling==2) out= out | (0x1<<12);
        
        // Writing to register Config_Memory_High_Register with address 18.
        nie_Board_Write(ioaddress, Configuration_Memory_High, (unsigned short)out);
        
        switch ((int_T)mxGetPr(DEV_ARG)[0]) {
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            case 12:
            case 16:
                if (range== -10000) {
                    out=0x0;
                    ssSetRWorkValue(S, GAIN_R_IND+i, 20.0/resFloat);
                    ssSetIWorkValue(S, POL_I_IND+i, 0);
                } else if (range==-5000) {
                    out=0x1;
                    ssSetRWorkValue(S, GAIN_R_IND+i, 10.0/resFloat);
                    ssSetIWorkValue(S, POL_I_IND+i, 0);
                } else if (range==-2500) {
                    out=0x2;
                    ssSetRWorkValue(S, GAIN_R_IND+i, 5.0/resFloat);
                    ssSetIWorkValue(S, POL_I_IND+i, 0);
                } else if (range==-1000) {
                    out=0x3;
                    ssSetRWorkValue(S, GAIN_R_IND+i, 2.0/resFloat);
                    ssSetIWorkValue(S, POL_I_IND+i, 0);
                } else if (range==-500) {
                    out=0x4;
                    ssSetRWorkValue(S, GAIN_R_IND+i, 1.0/resFloat);
                    ssSetIWorkValue(S, POL_I_IND+i, 0);
                } else if (range==-250) {
                    out=0x5;
                    ssSetRWorkValue(S, GAIN_R_IND+i, 0.5/resFloat);
                    ssSetIWorkValue(S, POL_I_IND+i, 0);
                } else if (range==-100) {
                    out=0x6;
                    ssSetRWorkValue(S, GAIN_R_IND+i, 0.2/resFloat);
                    ssSetIWorkValue(S, POL_I_IND+i, 0);
                } else if (range==-50) {
                    out=0x7;
                    ssSetRWorkValue(S, GAIN_R_IND+i, 0.1/resFloat);
                    ssSetIWorkValue(S, POL_I_IND+i, 0);
                } else if (range==10000) {
                    out=0x1 | (1<<8);
                    ssSetRWorkValue(S, GAIN_R_IND+i, 10.0/resFloat);
                    ssSetIWorkValue(S, POL_I_IND+i, 1);
                } else if (range==5000) {
                    out=0x2 | (1<<8);
                    ssSetRWorkValue(S, GAIN_R_IND+i, 5.0/resFloat);
                    ssSetIWorkValue(S, POL_I_IND+i, 1);
                } else if (range==2000) {
                    out=0x3 | (1<<8);
                    ssSetRWorkValue(S, GAIN_R_IND+i, 2.0/resFloat);
                    ssSetIWorkValue(S, POL_I_IND+i, 1);
                } else if (range==1000) {
                    out=0x4 | (1<<8);
                    ssSetRWorkValue(S, GAIN_R_IND+i, 1.0/resFloat);
                    ssSetIWorkValue(S, POL_I_IND+i, 1);
                } else if (range==500) {
                    out=0x5 | (1<<8);
                    ssSetRWorkValue(S, GAIN_R_IND+i, 0.5/resFloat);
                    ssSetIWorkValue(S, POL_I_IND+i, 1);
                } else if (range==200) {
                    out=0x6 | (1<<8);
                    ssSetRWorkValue(S, GAIN_R_IND+i, 0.2/resFloat);
                    ssSetIWorkValue(S, POL_I_IND+i, 1);
                } else if (range==100) {
                    out=0x7 | (1<<8);
                    ssSetRWorkValue(S, GAIN_R_IND+i, 0.1/resFloat);
                    ssSetIWorkValue(S, POL_I_IND+i, 1);
                }
                break;
            case 10:
            case 11:
                if (range== -10000) {
                    out=0x1;
                    ssSetRWorkValue(S, GAIN_R_IND+i, 20.0/resFloat);
                    ssSetIWorkValue(S, POL_I_IND+i, 0);
                } else if (range==-5000) {
                    out=0x2;
                    ssSetRWorkValue(S, GAIN_R_IND+i, 10.0/resFloat);
                    ssSetIWorkValue(S, POL_I_IND+i, 0);
                } else if (range==-2000) {
                    out=0x3;
                    ssSetRWorkValue(S, GAIN_R_IND+i, 4.0/resFloat);
                    ssSetIWorkValue(S, POL_I_IND+i, 0);
                } else if (range==-1000) {
                    out=0x4;
                    ssSetRWorkValue(S, GAIN_R_IND+i, 2.0/resFloat);
                    ssSetIWorkValue(S, POL_I_IND+i, 0);
                } else if (range==-500) {
                    out=0x5;
                    ssSetRWorkValue(S, GAIN_R_IND+i, 1.0/resFloat);
                    ssSetIWorkValue(S, POL_I_IND+i, 0);
                } else if (range==-200) {
                    out=0x6;
                    ssSetRWorkValue(S, GAIN_R_IND+i, 0.4/resFloat);
                    ssSetIWorkValue(S, POL_I_IND+i, 0);
                } else if (range==-100) {
                    out=0x7;
                    ssSetRWorkValue(S, GAIN_R_IND+i, 0.2/resFloat);
                    ssSetIWorkValue(S, POL_I_IND+i, 0);
                } else if (range==10000) {
                    out=0x1 | (1<<8);
                    ssSetRWorkValue(S, GAIN_R_IND+i, 10.0/resFloat);
                    ssSetIWorkValue(S, POL_I_IND+i, 1);
                } else if (range==5000) {
                    out=0x2 | (1<<8);
                    ssSetRWorkValue(S, GAIN_R_IND+i, 5.0/resFloat);
                    ssSetIWorkValue(S, POL_I_IND+i, 1);
                } else if (range==2000) {
                    out=0x3 | (1<<8);
                    ssSetRWorkValue(S, GAIN_R_IND+i, 2.0/resFloat);
                    ssSetIWorkValue(S, POL_I_IND+i, 1);
                } else if (range==1000) {
                    out=0x4 | (1<<8);
                    ssSetRWorkValue(S, GAIN_R_IND+i, 1.0/resFloat);
                    ssSetIWorkValue(S, POL_I_IND+i, 1);
                } else if (range==500) {
                    out=0x5 | (1<<8);
                    ssSetRWorkValue(S, GAIN_R_IND+i, 0.5/resFloat);
                    ssSetIWorkValue(S, POL_I_IND+i, 1);
                } else if (range==200) {
                    out=0x6 | (1<<8);
                    ssSetRWorkValue(S, GAIN_R_IND+i, 0.2/resFloat);
                    ssSetIWorkValue(S, POL_I_IND+i, 1);
                } else if (range==100) {
                    out=0x7 | (1<<8);
                    ssSetRWorkValue(S, GAIN_R_IND+i, 0.1/resFloat);
                    ssSetIWorkValue(S, POL_I_IND+i, 1);
                }
                break;
                
            case 22: // PCI-MIO-16XE-50 == PCI-6011
                switch( range ) {
                    case -10000:  // +-10
                        out=0x1;
                        ssSetRWorkValue(S, GAIN_R_IND+i, 20.0/resFloat);
                        ssSetIWorkValue(S, POL_I_IND+i, 0);
                        break;
                    case -5000:   // +-5
                        out=0x2;
                        ssSetRWorkValue(S, GAIN_R_IND+i, 10.0/resFloat);
                        ssSetIWorkValue(S, POL_I_IND+i, 0);
                        break;
                    case -1000:   // +-1
                        out=0x4;
                        ssSetRWorkValue(S, GAIN_R_IND+i, 2.0/resFloat);
                        ssSetIWorkValue(S, POL_I_IND+i, 0);
                        break;
                    case -100:    // +-.1
                        out=0x7;
                        ssSetRWorkValue(S, GAIN_R_IND+i, .2/resFloat);
                        ssSetIWorkValue(S, POL_I_IND+i, 0);
                        break;
                    case 10000:   // 0-10
                        out=0x1 | 0x100;
                        ssSetRWorkValue(S, GAIN_R_IND+i, 10.0/resFloat);
                        ssSetIWorkValue(S, POL_I_IND+i, 1);
                        break;
                    case 5000:    // 0-5
                        out=0x2 | 0x100;
                        ssSetRWorkValue(S, GAIN_R_IND+i, 5.0/resFloat);
                        ssSetIWorkValue(S, POL_I_IND+i, 1);
                        break;
                    case 1000:    // 0-1
                        out=0x4 | 0x100;
                        ssSetRWorkValue(S, GAIN_R_IND+i, 1.0/resFloat);
                        ssSetIWorkValue(S, POL_I_IND+i, 1);
                        break;
                    case 100:     // 0-.1
                        out=0x7 | 0x100;
                        ssSetRWorkValue(S, GAIN_R_IND+i, 0.1/resFloat);
                        ssSetIWorkValue(S, POL_I_IND+i, 1);
                        break;
                }
        }
        if (i==(nChannels-1)) out = out | 0x8000;  // Last channel in scan bit.
        
        // Writing to register Config_Memory_Low_Register with address 16.
        nie_Board_Write(ioaddress, Configuration_Memory_Low, (unsigned short)out);
    }
    
    nie_AI_MSC_Clock_Configure(ioaddress);
    nie_AI_Clear_FIFO(ioaddress);
    nie_AI_Reset_All(ioaddress);
    nie_AI_Board_Personalize(ioaddress);
    nie_AI_Initialize_Configuration_Memory_Output(ioaddress);
    nie_AI_Board_Environmentalize(ioaddress);
    //  nie_AI_FIFO_Request(ioaddress);
    //  nie_AI_Hardware_Gating(ioaddress);
    nie_AI_Trigger_Signals(ioaddress);
    //  nie_AI_Number_of_Scans(ioaddress);
    nie_AI_Scan_Start(ioaddress);
    nie_AI_End_of_Scan(ioaddress);
    nie_AI_Convert_Signal(ioaddress, convDelay);
    nie_AI_Clear_FIFO(ioaddress);
    //  nie_AI_Interrupt_Enable(ioaddress);
    nie_AI_Arming(ioaddress);

#endif

}

static void mdlOutputs(SimStruct *S, int_T tid)
{
#ifndef MATLAB_MEX_FILE

    uint_T base = ssGetIWorkValue(S, BASE_ADDR_I_IND);
    size_t nChannels = ssGetIWorkValue(S, CHANNELS_I_IND);
    size_t i;
    //int_T res;
    //double gain;
    volatile unsigned short *ioaddress;
    volatile unsigned short stat;
    volatile unsigned short data;
    real_T  *y;
    int count = 0;

    ioaddress=(void *) base;

// The following use of the 'volatile short data', is required by what
// appears to be broken optimizer action by Open Watcom 1.3.
// If the use of 'data' is removed and you just write the constants
// to ioaddress[0] an ioaddress[1], the A/D acquisition doesn't start
// and the timeout loop runs to completion and no data is present
// to be read.  This also causes the TET to go up to 10-15 milliseconds.
// This version works with Open Watcom 1.3 and with MSDEV 6.0, 7.0 and 7.1

// start single scan
//nie_AI_Start_The_Acquisition(ioaddress);
//nie_DAQ_STC_Windowed_Mode_Write(ioaddress, AI_Command_2_Register,0x0001);
    data = AI_Command_2_Register;
    ioaddress[0]= data;
    data = 0x0001;
    ioaddress[1]= data;
//nie_DAQ_STC_Windowed_Mode_Write(ioaddress, AI_START_STOP_Select_Register,0xa9FF);
    data = AI_START_STOP_Select_Register;
    ioaddress[0]= data;
    data = 0xa9FF;
    ioaddress[1]= data;
//nie_DAQ_STC_Windowed_Mode_Write(ioaddress, AI_START_STOP_Select_Register,0x29FF);
    data = AI_START_STOP_Select_Register;
    ioaddress[0]= data;
    data = 0x29FF;
    ioaddress[1]= data;

    for (i = 0; i < nChannels; i++) {
        y=ssGetOutputPortSignal(S, i);
    
    //nie_DAQ_STC_Windowed_Mode_Read(ioaddress, AI_Status_1_Register)
        data = AI_Status_1_Register;
        do {
            ioaddress[0]= data;
            stat = ioaddress[1];
        } while (((stat & 0x1000) == 0x1000) && (count++ < 10000));
        if(count >= 9990) {
            printf("AD: no response, 0=>%d, 1=>%x\n", ioaddress[0], ioaddress[1]);
        }
        if (ssGetIWorkValue(S, POL_I_IND+i)) {
            y[0] = (uint16_T) ioaddress[ADC_FIFO_Data_Register/2] * ssGetRWorkValue(S, GAIN_R_IND+i);
        } else {
            y[0] = (int16_T) ioaddress[ADC_FIFO_Data_Register/2] * ssGetRWorkValue(S, GAIN_R_IND+i);
        }
    }
#endif
}

static void mdlTerminate(SimStruct *S)
{
}

#ifdef MATLAB_MEX_FILE  /* Is this file being compiled as a MEX-file? */
#include "simulink.c"   /* Mex glue */
#else
#include "cg_sfun.h"    /* Code generation glue */
#endif
