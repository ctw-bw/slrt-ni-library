// encnipcim.c - xPC Target, non-inlined S-function driver for the 
// encoder functionality of National Instruments M series boards

// This version supports quad modes X1, X2, X4 for channels 1-4.
// Spurious reloads are sometimes observed when reload is selected.

// Copyright 2003-2014 The MathWorks, Inc.

#define S_FUNCTION_LEVEL  2
#undef  S_FUNCTION_NAME
#define S_FUNCTION_NAME   encnipcim

#include <stddef.h> 
#include <stdlib.h> 

#include "simstruc.h" 

#ifdef  MATLAB_MEX_FILE
#include "mex.h"
#endif

#ifndef MATLAB_MEX_FILE
#include <windows.h>
#include "xpctarget.h"
#include "xpcionim.h"

#endif

#define NUM_ARGS        10
#define DEV_ARG         ssGetSFcnParam(S, 0) // 
#define CHANNEL_ARG     ssGetSFcnParam(S, 1) // 0 or 1
#define COUNT_MODE_ARG  ssGetSFcnParam(S, 2) // 1:X1 2:X2 3:X4
#define INIT_COUNT_ARG  ssGetSFcnParam(S, 3)
#define RELOAD_ARG      ssGetSFcnParam(S, 4) // boolean
#define INDEX_PHASE_ARG ssGetSFcnParam(S, 5) // 1-4
#define FILTER_ARG      ssGetSFcnParam(S, 6) // 1-4
#define PFIDIR_ARG      ssGetSFcnParam(S, 7)
#define SAMP_TIME_ARG   ssGetSFcnParam(S, 8) // seconds
#define SLOT_ARG        ssGetSFcnParam(S, 9) // integer

#define NUM_I_WORKS     (0)
#define NUM_R_WORKS     (0)
#define NUM_P_WORKS     (1)

/* Index into PWork */
#define BASE_ADDR_P_IND (0)

static char_T msg[256];

static void mdlInitializeSizes(SimStruct *S)
{
    int_T i, numOutputPorts;

    ssSetNumSFcnParams(S, NUM_ARGS);
    if (ssGetNumSFcnParams(S) != ssGetSFcnParamsCount(S))
    {
        sprintf(msg, "%d input args expected, %d passed", 
            NUM_ARGS, ssGetSFcnParamsCount(S));
        ssSetErrorStatus(S, msg);
        return;
    }

    ssSetNumContStates(S, 0);
    ssSetNumDiscStates(S, 0);
    if( !ssSetNumInputPorts(S, 0) )return;

    numOutputPorts = (int_T)mxGetN(CHANNEL_ARG);

    if( !ssSetNumOutputPorts(S, numOutputPorts) )return;
    for (i = 0; i < numOutputPorts; i++)
    {
        ssSetOutputPortWidth(S, i, 1);
    }

    ssSetSimStateCompliance( S, HAS_NO_SIM_STATE );

    ssSetNumSampleTimes(S, 1);
    ssSetNumRWork(S, NUM_R_WORKS);
    ssSetNumIWork(S, NUM_I_WORKS);
    ssSetNumPWork(S, NUM_P_WORKS);
    ssSetNumModes(S, 0);
    ssSetNumNonsampledZCs(S, 0);

    for (i = 0; i < NUM_ARGS; i++)
        ssSetSFcnParamTunable(S, i, 0);

    ssSetOptions(S, SS_OPTION_DISALLOW_CONSTANT_SAMPLE_TIME | SS_OPTION_EXCEPTION_FREE_CODE );
}
 
static void mdlInitializeSampleTimes(SimStruct *S)
{
    ssSetModelReferenceSampleTimeInheritanceRule(S, USE_DEFAULT_FOR_DISCRETE_INHERITANCE);
    if (mxGetPr(SAMP_TIME_ARG)[0] == -1.0)
    {
        ssSetSampleTime(S, 0, INHERITED_SAMPLE_TIME);
        ssSetOffsetTime(S, 0, FIXED_IN_MINOR_STEP_OFFSET);
    } else {
        ssSetSampleTime(S, 0, mxGetPr(SAMP_TIME_ARG)[0]);
        ssSetOffsetTime(S, 0, 0.0);
    }
}

#define MDL_START 
static void mdlStart(SimStruct *S)
{
#ifndef MATLAB_MEX_FILE
    //int_T   *IWork   = ssGetIWork(S);
    int_T    devtype  = (int_T) mxGetPr(DEV_ARG)[0];
    int_T    bus, slot;
    xpcPCIDevice pciinfo;
    volatile int32_T *lreg;
    volatile uint16_T *sreg;
    const char *devName;
    MseriesBoard *bd;
    uint32_T pfidir = (uint32_T)mxGetPr(PFIDIR_ARG)[0];
    int32_T bnum;
    int32_T success;
    uint32_T achan, bchan, zchan;
    //int_T    i;
    int_T    chan, reload, phase, filter;

    if (mxGetN(SLOT_ARG) == 1)
    {
        bus = 0;
        slot = (int_T)mxGetPr(SLOT_ARG)[0];
    }
    else
    {
        bus = (int_T)mxGetPr(SLOT_ARG)[0];
        slot = (int_T)mxGetPr(SLOT_ARG)[1];
    }

    devtype = (int_T)mxGetPr(DEV_ARG)[0];

    bd = niMIdentify( S, devtype, bus, slot, &pciinfo );
    if( bd == 0 )
        return;  // We didn't find the board as described.
    devName = bd->name;
    
    lreg  = (volatile int32_T *)pciinfo.VirtAddress[1];
    sreg  = (volatile uint16_T *)pciinfo.VirtAddress[1];

    bnum = niM_Private_Data( (void *)sreg );
    if( bnum < 0 )
    {
        sprintf(msg, "%s: Too many M series boards", devName );
        ssSetErrorStatus(S, msg);
        return;
    }
    ssSetPWorkValue(S, BASE_ADDR_P_IND, (void *)lreg);

    chan = (int_T)mxGetPr(CHANNEL_ARG)[0];

    niM_configureTimebase( (void *)sreg );
    niM_Counter_Reset_All( (void *)sreg, chan );

    sreg[ S_IO_Bidirection_Pin ] = pfidir;

    filter = (int_T)mxGetPr(FILTER_ARG)[0] - 1;
    // Set both encoder channels and the index pulse.
    if( chan == 0 )
    {
        if( devtype == 27 )  // Special 6221/37
        {
            achan = 0;
            bchan = 2;
            zchan = 1;
        }
        else  // All the rest
        {
            achan = 8;
            bchan = 10;
            zchan = 9;
        }
    }
    else
    {
        if( devtype == 27 )  // Special 6221/37
        {
            achan = 3;
            bchan = 5;
            zchan = 4;
        }
        else  // All the rest
        {
            achan = 3;
            bchan = 11;
            zchan = 4;
        }
    }
    
    success = niM_SetPFIFilter( bnum, achan, filter );
    success = niM_SetPFIFilter( bnum, bchan, filter );
    success = niM_SetPFIFilter( bnum, zchan, filter );

    // Encoder setup
    {
        uint16_T mode;
        uint16_T countmode;
        uint16_T cmode;
        int32_T initCount;
        
        sreg[ S_G0_Mode + chan ] = 0; // Point to LOAD Source A
        // load initial value to both load registers
        initCount = (int32_T)mxGetPr(INIT_COUNT_ARG)[0];

        sreg[ S_G0_Command + chan ] = 0x0E40;  // Select bank X

        // Duplicate write A and B
        lreg[ L_G0_Load_A + 2*chan ] = initCount;

        // sync gate, load counter
        sreg[ S_G0_Command + chan ] = 0x0144;

        lreg[ L_G0_Load_B + 2*chan ] = initCount;
        lreg[ L_G0_Load_A + 2*chan ] = initCount;

        // index phase
        phase = (int_T)mxGetPr(INDEX_PHASE_ARG)[0] - 1;
        // Gi_Alternate_Sync = 0x4000, only needed if 80 MHz source
        // but it doesn't hurt for slower speeds.
        countmode = 0x4000;
        // Gi_Index_Phase = 0x0060 bits,
        //    0 = a low, b low
        //    1 = a low, b high
        //    2 = a high, b low
        //    3 = a high, b high
        // Depends on when the encoder asserts index.
        countmode |= (phase & 3) << 5;
        cmode = (uint16_T)mxGetPr(COUNT_MODE_ARG)[0];
        // Gi_Counting_Mode = 0x0007, bottom 3 bits
        //    0 not an incremental encoder, normal mode
        //    1 = x1 incremental
        //    2 = x2 incremental
        //    3 = x4 incremental
        //    4 and 5 are not incremental encoder modes.
        // Only 1, 2 or 3 can come from the block.
        countmode |= cmode & 0x7;
        reload = (int_T)mxGetPr(RELOAD_ARG)[0];  // 0 noreload, 1 reset on index
        if( reload == 1 )
            countmode |= 0x10;
        sreg[ S_G0_MSeries_Counting_Mode + chan ] = countmode;

        switch( chan )
        {
            // Use the input pins that are documented in the NI user
            // manual for the M series boards.
            case 0:
                if( devtype == 27 )  // Special 6221/37
                {
                    // A = SRC = PFI0 = 1 << 10 = 0x01 << 10 = 0x0400
                    // B = AUX = PFI2 = 3 << 5 = 0x03 << 5 = 0x0060
                    // Z = Gate = PFI1 = 2 << 0 = 0x0002
                    sreg[ S_G0_MSeries_ABZ ] = 0x0462;
                }
                else
                {
                    // A = SRC = PFI8 = 9 << 10 = 0x09 << 10 = 0x2400
                    // B = AUX = PFI10 = 21 << 5 = 0x15 << 5 = 0x02a0
                    // Z = Gate = PFI9 = 0xA << 0 = 0x000a
                    sreg[ S_G0_MSeries_ABZ ] = 0x26aa;
                }
                break;
            case 1:
                if( devtype == 27 )  // Special 6221/37
                {
                    // A = SRC = PFI3 = 4 << 10 = 0x04 << 10 = 0x1000
                    // B = AUX = PFI5 = 6 << 5 = 0x06 << 5 = 0x00c0
                    // Z = Gate = PFI4 = 5 << 0 = 0x0005
                    sreg[ S_G1_MSeries_ABZ ] = 0x10c5;
                }
                else
                {
                    // A = SRC = PFI3 = 4 << 10 = 0x1000
                    // B = AUX = PFI11 = 22 << 5 = 0x16 << 5 = 0x02c0
                    // Z = Gate = PFI4 = 5 << 0 = 0x0005
                    sreg[ S_G1_MSeries_ABZ ] = 0x12c5;
                }
                break;
        }
        
        // no gated start/stop, rising edge gate
        // Load source select(A) = 0 << 7
        // 
        mode = 0x119;
        if( reload == 1 )
            mode |= 0x4000;  // Reload on gate, index pulse to gate.
        sreg[ S_G0_Mode + chan ] = mode;

        // Synchronous gate, aux (B) direction, armed, following value
        sreg[ S_G0_Command + chan ] = 0x0141;
    }
#endif
}

static void mdlOutputs(SimStruct *S, int_T tid)
{
#ifndef MATLAB_MEX_FILE
    int_T   chan  = (int_T)mxGetPr(CHANNEL_ARG)[0];
    real_T *y     = ssGetOutputPortRealSignal(S, 0);
    volatile int32_T *lreg = (volatile int32_T *)ssGetPWorkValue(S, BASE_ADDR_P_IND);
    volatile uint16_T *sreg = (volatile uint16_T *)lreg;
    int32_T value;
    
    // synch gate, counting direction = aux pin 
    sreg[ S_G0_Command + chan ] = 0x0140;
    
    // synch gate, counting direction = aux pin, save trace
    sreg[ S_G0_Command + chan ] = 0x0142;

    // read count
    value = lreg[L_G0_Save + chan];
    y[0] = (real_T)value;
#endif
}

static void mdlTerminate(SimStruct *S)
{
#ifndef MATLAB_MEX_FILE
    int_T   chan  = (int_T)mxGetPr(CHANNEL_ARG)[0];
    volatile uint16_T *sreg = (volatile uint16_T *)ssGetPWorkValue(S, BASE_ADDR_P_IND);

    sreg[ S_G0_Command + chan ] = 0x0010;  // Disarm on stop
#endif
}

#ifdef MATLAB_MEX_FILE
#include "simulink.c"
#else
#include "cg_sfun.h"
#endif
