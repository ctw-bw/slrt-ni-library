/* pfidionipcim.c - Simulink Real-Time, non-inlined S-function driver for
 *                  digital IO on the PFI lines
 *                  for the National Instruments M series boards.
 */
/* Copyright 1996-2014 The MathWorks, Inc.
 */

#define         S_FUNCTION_LEVEL        2
#undef          S_FUNCTION_NAME
#define         S_FUNCTION_NAME         pfidonipcim

#include        <stddef.h>
#include        <stdlib.h>

#include        "simstruc.h"

#ifdef          MATLAB_MEX_FILE
#include        "mex.h"
#endif

#ifndef         MATLAB_MEX_FILE
#include        <windows.h>
#include        "xpctarget.h"
#include        "xpcionim.h"
#endif

/* Input Arguments */
#define NUMBER_OF_ARGS          (7)
#define DEV_ARG                 ssGetSFcnParam(S, 0)
#define CHANNEL_ARG             ssGetSFcnParam(S, 1)
#define PFIDIR_ARG              ssGetSFcnParam(S, 2)
#define RESET_ARG               ssGetSFcnParam(S, 3)
#define INITIAL_ARG             ssGetSFcnParam(S, 4)
#define SAMP_TIME_ARG           ssGetSFcnParam(S, 5)
#define SLOT_ARG                ssGetSFcnParam(S, 6)

#define SAMP_TIME_IND           (0)

#define NO_R_WORKS              (0)
#define NO_I_WORKS              (1)
#define NO_P_WORKS              (1)

/* Index into IWork */
#define BNUM_I_IND              (0)

/* Index into PWork */
#define BASE_ADDR_P_IND         (0)

static char_T msg[256];

static void mdlInitializeSizes(SimStruct *S)
{
    size_t i, j, numInputs, numOutputs;

    ssSetNumSFcnParams(S, NUMBER_OF_ARGS);
    if (ssGetNumSFcnParams(S) != ssGetSFcnParamsCount(S))
    {
        sprintf(msg,"Wrong number of input arguments passed.\n"
                "%d arguments are expected\n", NUMBER_OF_ARGS);
        ssSetErrorStatus(S,msg);
        return;
    }

    ssSetNumContStates(S, 0);
    ssSetNumDiscStates(S, 0);

    numInputs = mxGetN( CHANNEL_ARG );
    if( !ssSetNumInputPorts(S, (int32_T)numInputs ) )return;
    for( i = 0 ; i < numInputs ; i++ )
    {
        ssSetInputPortWidth(             S, i, 1);
        ssSetInputPortDirectFeedThrough( S, i, 1);
        ssSetInputPortRequiredContiguous(S, i, 1);
    }

    numOutputs = 0;
    if( !ssSetNumOutputPorts(S, (int32_T)numOutputs ) )return;
    for( i = 0 ; i < numOutputs ; i++ )
    {
        ssSetOutputPortWidth( S, i, 1 );
    }

    ssSetNumSampleTimes(S, 1);

    ssSetSimStateCompliance( S, HAS_NO_SIM_STATE );

    ssSetNumRWork(S, NO_R_WORKS);
    ssSetNumIWork(S, NO_I_WORKS);
    ssSetNumPWork(S, NO_P_WORKS);

    ssSetNumModes(S, 0);
    ssSetNumNonsampledZCs(S, 0);

    for (j = 0; j < NUMBER_OF_ARGS; j++)
        ssSetSFcnParamNotTunable(S, j);

    ssSetOptions(S, SS_OPTION_DISALLOW_CONSTANT_SAMPLE_TIME | SS_OPTION_EXCEPTION_FREE_CODE );
    return;
}

static void mdlInitializeSampleTimes(SimStruct *S)
{
    if (mxGetPr(SAMP_TIME_ARG)[0] == -1.0)
    {
        ssSetSampleTime(S, 0, INHERITED_SAMPLE_TIME);
        ssSetOffsetTime(S, 0, FIXED_IN_MINOR_STEP_OFFSET);
        ssSetModelReferenceSampleTimeInheritanceRule(S, USE_DEFAULT_FOR_DISCRETE_INHERITANCE);
    }
    else
    {
        ssSetSampleTime(S, 0, mxGetPr(SAMP_TIME_ARG)[0]);
        ssSetOffsetTime(S, 0, 0.0);
    }
}

#define MDL_START
static void mdlStart(SimStruct *S)
{
#ifndef MATLAB_MEX_FILE
    int_T bus, slot;
    xpcPCIDevice pciinfo;
    volatile uint32_T *lreg;
    volatile uint16_T *sreg;
    const char *devName;
    uint32_T *iwork = (uint32_T *)ssGetIWork(S);
    MseriesBoard *bd;
    int  devtype;
    int32_T nchan = (int32_T)mxGetN( CHANNEL_ARG );
    int32_T chan;
    uint32_T pfidir = (uint32_T)mxGetPr(PFIDIR_ARG)[0];
    int32_T bnum;
    int32_T idx;

    if (mxGetN(SLOT_ARG) == 1)
    {
        bus = 0;
        slot = (int_T)mxGetPr(SLOT_ARG)[0];
    }
    else
    {
        bus = (int_T)mxGetPr(SLOT_ARG)[0];
        slot = (int_T)mxGetPr(SLOT_ARG)[1];
    }

    devtype = (int_T)mxGetPr(DEV_ARG)[0];

    bd = niMIdentify( S, devtype, bus, slot, &pciinfo );
    if( bd == 0 )
        return;  // We didn't find the board as described.
    devName = bd->name;

    lreg  = (volatile uint32_T *)pciinfo.VirtAddress[1];
    sreg  = (volatile uint16_T *)pciinfo.VirtAddress[1];

    bnum = niM_Private_Data( (void *)sreg );
    if( bnum < 0 )
    {
        sprintf(msg, "%s: Too many M series boards", devName );
        ssSetErrorStatus(S, msg);
        return;
    }

    ssSetPWorkValue(S, BASE_ADDR_P_IND, (void *)lreg);
    iwork[BNUM_I_IND] = bnum;

    // Set PFI direction register
    // The mask init file looked at all blocks that use PFI bits.
    sreg[ S_IO_Bidirection_Pin ] = pfidir;

    // Make sure this blocks channels are set up correctly.
    for( idx = 0 ; idx < nchan ; idx++ )
    {
        chan = (int32_T)mxGetPr( CHANNEL_ARG )[idx];
        niM_SetPFI_Select( bnum, chan, PFI_SELECT_DO ); //PFI_SELECT_DEFAULT );
        // Only the PWM outputs don't default to DO, PFI12 and PFI13.
    }

#endif
}

#define THRESHOLD               0.5

static void mdlOutputs(SimStruct *S, int_T tid)
{
#ifndef MATLAB_MEX_FILE
    uint32_T output;
    uint32_T channel;
    volatile uint16_T *sreg = ssGetPWorkValue(S, BASE_ADDR_P_IND );
    uint32_T bnum = ssGetIWork(S)[BNUM_I_IND];
    size_t i;
    real_T *uPtr;

    for(i = 0 ; i < mxGetN(CHANNEL_ARG) ; i++ )
    {
        channel = (uchar_T)mxGetPr(CHANNEL_ARG)[i];
        uPtr = (real_T *)ssGetInputPortRealSignal(S,i);
        if (*uPtr >= THRESHOLD)
        {
            output = niM_SetPFI_DO( bnum, channel, 1 );
        }
        else
        {
            output = niM_SetPFI_DO( bnum, channel, 0 );
        }
    }
    sreg[ S_PFI_DO ] = output;

    return;
#endif
}

static void mdlTerminate(SimStruct *S)
{
#ifndef MATLAB_MEX_FILE
    volatile uint16_T *sreg = ssGetPWorkValue(S, BASE_ADDR_P_IND );
    size_t i;
    volatile uint16_T output;
    uint32_T channel;
    uint32_T bnum = ssGetIWork(S)[BNUM_I_IND];

    output = sreg[ S_PFI_DI ];  // Only used if no channels reset
    for(i = 0 ; i < mxGetN(CHANNEL_ARG) ; i++ )
    {
        if (xpcIsModelInit() || (uint_T)mxGetPr(RESET_ARG)[i])
        {
            channel = (uchar_T)mxGetPr(CHANNEL_ARG)[i];
            if (mxGetPr(INITIAL_ARG)[i] > THRESHOLD)
                output = niM_SetPFI_DO( bnum, channel, 1 );
            else
                output = niM_SetPFI_DO( bnum, channel, 0 );
        }
    }
    sreg[ S_PFI_DO ] = output;

#endif
    return;
}

#ifdef MATLAB_MEX_FILE  /* Is this file being compiled as a MEX-file? */
#include "simulink.c"   /* Mex glue */
#else
#include "cg_sfun.h"    /* Code generation glue */
#endif
