/* adnipcim.c - xPC Target, non-inlined S-function driver for A/D
 * section of NI PCI-M series boards
 */
/* Copyright 1996-2014 The MathWorks, Inc.
 */

#define         S_FUNCTION_LEVEL        2
#undef          S_FUNCTION_NAME
#define         S_FUNCTION_NAME         adnipcim

#include        <stddef.h>
#include        <stdlib.h>

#include        "simstruc.h"

#ifdef          MATLAB_MEX_FILE
#include        "mex.h"
#endif

#ifndef         MATLAB_MEX_FILE
#include        <windows.h>
#include        "xpctarget.h"
#include        "xpcionim.h"
#endif

/* Input Arguments */
#define NUMBER_OF_ARGS          (7)
#define CHANNEL_ARG             ssGetSFcnParam(S,0)
#define RANGE_ARG               ssGetSFcnParam(S,1)
#define COUPLING_ARG            ssGetSFcnParam(S,2)
#define SCAN_INTERVAL_ARG       ssGetSFcnParam(S,3)
#define SAMP_TIME_ARG           ssGetSFcnParam(S,4)
#define SLOT_ARG                ssGetSFcnParam(S,5)
#define DEV_ARG                 ssGetSFcnParam(S,6)

#define NO_I_WORKS              (41)
#define BASE_ADDR_I_IND         (0)
#define SCALE_I_IND             (1)  /* array of tScalingCoefficients*8 */

#define NO_R_WORKS              (0)

#define NO_P_WORKS              (1)
#define BOARD_P_IND             (0)

static char_T msg[256];

// List xpcionim.c as additional file to
// include with the block.

static void mdlInitializeSizes(SimStruct *S)
{
    size_t i;

    ssSetNumSFcnParams(S, NUMBER_OF_ARGS);
    if (ssGetNumSFcnParams(S) != ssGetSFcnParamsCount(S))
    {
        sprintf(msg,"Wrong number of input arguments passed.\n%d arguments are expected\n",NUMBER_OF_ARGS);
        ssSetErrorStatus(S,msg);
        return;
    }

    ssSetNumContStates(S, 0);
    ssSetNumDiscStates(S, 0);

    if( !ssSetNumOutputPorts(S, (int_T)mxGetN(CHANNEL_ARG))) return;
    for (i = 0; i < mxGetN(CHANNEL_ARG); i++)
    {
        ssSetOutputPortWidth(S, i, 1);
    }

    if( !ssSetNumInputPorts(S, 0)) return;

    ssSetNumSampleTimes(S, 1);

    ssSetSimStateCompliance( S, HAS_NO_SIM_STATE );

    ssSetNumRWork(S, NO_R_WORKS);
    ssSetNumIWork(S, NO_I_WORKS);
    ssSetNumPWork(S, NO_P_WORKS);

    ssSetNumModes(S, 0);
    ssSetNumNonsampledZCs(S, 0);

    for( i = 0 ; i < NUMBER_OF_ARGS ; i++ )
        ssSetSFcnParamNotTunable(S,i);

    ssSetOptions(S, SS_OPTION_DISALLOW_CONSTANT_SAMPLE_TIME | SS_OPTION_EXCEPTION_FREE_CODE );
}

static void mdlInitializeSampleTimes(SimStruct *S)
{
    ssSetModelReferenceSampleTimeInheritanceRule(S, USE_DEFAULT_FOR_DISCRETE_INHERITANCE);
    if (mxGetPr(SAMP_TIME_ARG)[0] == -1.0)
    {
    	ssSetSampleTime(S, 0, INHERITED_SAMPLE_TIME);
    	ssSetOffsetTime(S, 0, FIXED_IN_MINOR_STEP_OFFSET);
    }
    else
    {
        ssSetSampleTime(S, 0, mxGetPr(SAMP_TIME_ARG)[0]);
    	ssSetOffsetTime(S, 0, 0.0);
    }
}

#define MDL_START
static void mdlStart(SimStruct *S)
{
#ifndef MATLAB_MEX_FILE

    int_T nChannels;
    int_T i;
    uint32_T *channel, *range, *coupling, *polarity;
    int_T bus, slot;

    xpcPCIDevice pciinfo;
    volatile uint32_T *lreg;
    volatile uint16_T *sreg;
    volatile uint8_T  *breg;
    volatile uint8_T  *breg0;  // bar0
    MseriesBoard *bd;
    uint32_T devtype;
    uint32_T maxchan;
    uint32_T minconvert;
    real_T scaninterval = mxGetPr( SCAN_INTERVAL_ARG )[0];
    uint32_T scancount;
    bool adreset;
    uint8_T eeprom[1024];
    tScalingCoefficients *iscale;

    nChannels = mxGetN(CHANNEL_ARG);
    devtype = (int_T)mxGetPr(DEV_ARG)[0];


    if (mxGetN(SLOT_ARG) == 1)
    {
        bus = 0;
        slot = (int_T)mxGetPr(SLOT_ARG)[0];
    }
    else
    {
        bus = (int_T)mxGetPr(SLOT_ARG)[0];
        slot = (int_T)mxGetPr(SLOT_ARG)[1];
    }

    bd = niMIdentify( S, devtype, bus, slot, &pciinfo );
    if( bd == 0 )
        return;  // We didn't find the board as described.

    // Since we allow repeating channel numbers, the channel vector can
    // be of arbitrary length.  Allow any of these arrays to be of
    // arbitrary length.
    if( (channel = malloc( nChannels * sizeof(uint32_T) )) == 0 )
    {
        sprintf( msg, "NI %s: Can't allocate temporary buffer.", bd->name );
        ssSetErrorStatus(S,msg);
        return;

    }
    if( (range = malloc( nChannels * sizeof(uint32_T) )) == 0 )
    {
        free( channel );
        sprintf( msg, "NI %s: Can't allocate temporary buffer.", bd->name );
        ssSetErrorStatus(S,msg);
        return;
    }
    if( (coupling = malloc( nChannels * sizeof(uint32_T) )) == 0 )
    {
        free( channel );
        free( range );
        sprintf( msg, "NI %s: Can't allocate temporary buffer.", bd->name );
        ssSetErrorStatus(S,msg);
        return;
    }
    if( (polarity = malloc( nChannels * sizeof(uint32_T) )) == 0 )
    {
        free( channel );
        free( range );
        free( coupling );
        sprintf( msg, "NI %s: Can't allocate temporary buffer.", bd->name );
        ssSetErrorStatus(S,msg);
        return;
    }

    adreset = bd->AdReset;
    maxchan = bd->maxADChan;
    if( nChannels > 1 )
        minconvert = bd->minConvert[1];
    else
        minconvert = bd->minConvert[0];

    lreg  = (volatile uint32_T *)pciinfo.VirtAddress[1];
    sreg  = (volatile uint16_T *)lreg;
    breg  = (volatile uint8_T *)lreg;
    breg0 = (volatile uint8_T *)pciinfo.VirtAddress[0];

    ssSetIWorkValue(S, BASE_ADDR_I_IND, (uint_T)lreg);

    niM_eepromReadMSeries( (void *)breg0, (void *)breg, eeprom, 1024 );

    iscale = (tScalingCoefficients *)(((uint8_T *)ssGetIWork( S )) + sizeof(uint32_T)*SCALE_I_IND);

    for( i = 0 ; i < 7 ; i++ )
    {
        tScalingCoefficients lscale;
        
        // Get scaling coefficients from EEProm data
        niM_aiGetScalingCoefficients( eeprom, i, 0, 0, &lscale );
        iscale[i] = lscale;
        //printf("%d %d, %f %f %f %f\n", i, iscale[i].order, iscale[i].c[0],
        //  iscale[i].c[1],iscale[i].c[2],iscale[i].c[3] );
    }

    // Check the range of channel numbers, should have been caught in mask init, but recheck here.
    for( i = 0 ; i < nChannels ; i++ )
    {
        channel[i]  = (uint32_T)mxGetPr( CHANNEL_ARG )[i];
        range[i]    = (uint32_T)mxGetPr( RANGE_ARG )[i];
        coupling[i] = (uint32_T)mxGetPr( COUPLING_ARG )[i];

        // NOTE: the low level register programming subtracts 1 from channel
        // to get the hardware 0 based channel numbers.
        if( channel[i] < 1 || channel[i] > maxchan )
        {
            sprintf( msg, "NI M series: Channel %d out of range, %s.", channel[i], bd->name );
            ssSetErrorStatus(S,msg);
            return;
        }

        polarity[i] = 0;  // All are BIPOLAR only, this bit used on 6255 for upper 16 chans.

        // if devtype is 25 or 55, this is an 80 channel board.  For the 6225,
        // the channels from 65 to 80 need special handling.  The method for
        // the 6255 is different.
        if( devtype == 25 )
        {
            if( channel[i] > 64 )
            {
                channel[i] &= 0x3f;
                range[i] |= 2;  // access the 5th group of 16 channels, bit number 1
            }
        }
        if( devtype == 55 )
        {
            if( channel[i] > 64 )
            {
                // Need to set the polarity bit in the fifo config register for
                // this channel.
                channel[i] &= 0x3f;
                polarity[i] = 1;  // access the 5th group of 16 channels
            }
        }
    }

    scancount = (uint32_T)(scaninterval * 20e6);
    if( scancount < minconvert )
        scancount = minconvert;

    // Configure the hardware for demand mode A/D
    niM_AISetup( (void *)breg, adreset, channel, range, coupling, polarity, nChannels, scancount );

    // Clean up.
    free( channel );
    free( range );
    free( coupling );
    free( polarity );
#endif
}

static uint32_T gainidx[2][8] = { { 0, 1, 0, 0, 3, 5, 0, 0},
                                  { 0, 0, 1, 2, 3, 4, 5, 6} };

static void mdlOutputs(SimStruct *S, int_T tid)
{
#ifndef MATLAB_MEX_FILE

    uint_T base = ssGetIWorkValue(S, BASE_ADDR_I_IND);
    int_T nChannels = mxGetN( CHANNEL_ARG );
    const real_T *range;
    tScalingCoefficients *iscale;
    int_T i;
    volatile uint8_T  *breg;
    volatile uint16_T *sreg;
    volatile uint32_T *lreg;
    volatile uint16_T stat;
    real_T  *y;
    int32_T value;
    float scaled;
    int_T devtype = (int_T)mxGetPr(DEV_ARG)[0];
    int32_T gaintype;
    int32_T gaini;
    uint32_T counter;
    real_T starttime;
    xpcTime timestr;

    breg = (volatile uint8_T  *)base;
    sreg = (volatile uint16_T *)base;
    lreg = (volatile uint32_T *)base;  // Data is signed

    // The mask init file converted range and coupling from the mask values
    // to the hardware values.
    range   = mxGetPr( RANGE_ARG );

    // clear the FIFO each time, else we get out of sync sometimes!
    sreg[S_AI_FIFO_Clear] = 1;

    // start single scan
    // niM_aiStartOnDemand (char *base);
    sreg[ S_AI_Command_2 ] = 0x0004;

    // Do some stuff while waiting for completion
    iscale = (tScalingCoefficients *)(((uint8_T *)ssGetIWork( S )) + sizeof(uint32_T)*SCALE_I_IND);
    if( (int)(devtype / 10) == 2 )  // 622x boards
        gaintype = 0;
    else
        gaintype = 1;

    counter = 0;
    starttime = xpcGetElapsedTime( 0 ); //&timestr );
    // With MSDEV 6.0, the optimizer sees that we're only looking at
    // a bit in the lower byte and it incorrectly decides to only read
    // the lower byte.  This doesn't work.  Save the value to a short
    // variable then mask it and the problem goes away.  Not a problem
    // in MSDEV 8.0 or later or with OpenWatcom 1.7 or later.
    while( (stat = sreg[ S_Joint_Status_2 ]) & 0x0080 ) // while AI in progress
    {
        if( ((counter++) & 0x3ff) == 0x3ff )
        {
            time_T ts = ssGetSampleTime(S, 0);  // actually a real_T 
            if( xpcGetElapsedTime( &timestr ) - starttime > ts )
            {
                sprintf( msg, "NI M series: Timeout waiting for A/D" );
                ssSetErrorStatus(S,msg);
                return;
            }
        }
    }

    for (i = 0 ; i < nChannels ; i++ )
    {
        y = ssGetOutputPortSignal(S,i);
        value = lreg[ L_AI_FIFO_Data ];
        // Scale the value
        gaini = gainidx[gaintype][(int32_T)range[i]];
        niM_aiPolynomialScaler( &value, &scaled, iscale+gaini );
        y[0] = (real_T)scaled;
    }
#endif
}

static void mdlTerminate(SimStruct *S)
{
#ifndef MATLAB_MEX_FILE
    uint_T base = ssGetIWorkValue(S, BASE_ADDR_I_IND);
    volatile unsigned short *sreg = (volatile unsigned short *)base;

    // aiDisarm()
    sreg[S_Joint_Reset] = 0x0001;
#endif
}

#ifdef MATLAB_MEX_FILE  /* Is this file being compiled as a MEX-file? */
#include "simulink.c"   /* Mex glue */
#else
#include "cg_sfun.h"    /* Code generation glue */
#endif
